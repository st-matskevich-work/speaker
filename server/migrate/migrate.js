var pg = require('pg');
var fs = require('fs'),
    path = require('path'),
    Client = pg.Client

const dotenv = require('dotenv');
dotenv.config();

let connect = {
    user: process.env.DB_USER_NAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    connectionString: process.env.DATABASE_URL
};

var client = new Client(connect);
client.connect()
    .then(() => { return client.query('create table if not exists migrations (id int NOT NULL)') })
    .then(() => { return client.query('select max(id) from migrations') })
    .then(result => {
        var version = result.rows[0].max ? result.rows[0].max : 0;
        console.log('Current db version: ', version);
        var dir = path.join(__dirname, './migrations'),
            files = fs.readdirSync(dir).sort();

        return { version, dir, files };
    })
    .then(result => {
        var p = Promise.resolve();
        var start = result.version;
        result.files.forEach(file => {
            p = p.then(() => {
                var n = +file.substr(0, 5)
                if (n <= result.version) return;
                console.log('Migration ', n)

                var q = fs.readFileSync(path.join(result.dir, file), 'utf8');
                return client.query(q)
                    .then(() => { result.version = n; console.log('OK') });
            });
        });
        p = p.then(() => {
            if (result.version != start)
                return client.query('insert into migrations (id) values ($1)', [result.version])
                    .then(() => { console.log('Updated db. New version: ', result.version); });
            else 
                console.log('DB is up to date!');
        });
        return p;
    })
    .then(() => { process.exit() })
    .catch(e => { console.log(e.stack); process.exit(-1) });