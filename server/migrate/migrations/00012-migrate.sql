DROP TABLE messages;
CREATE TABLE messages(_id SERIAL PRIMARY KEY, sender_id int, receiver_id int, message TEXT, created_date TEXT);