drop table users;
CREATE TABLE users (id SERIAL PRIMARY KEY,  name VARCHAR(60), credentials VARCHAR(100) UNIQUE, password VARCHAR(65), status BOOLEAN, setup int, registrationDate DATE);
