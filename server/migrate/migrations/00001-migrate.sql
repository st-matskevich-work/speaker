CREATE TABLE users (id SERIAL PRIMARY KEY,  name VARCHAR(60), email VARCHAR(100) UNIQUE, password VARCHAR(65), status BOOLEAN, setup int, registrationDate DATE);

CREATE TABLE tokens (id int, token VARCHAR(64) UNIQUE, valid BOOLEAN);

CREATE TABLE activators (id int, code VARCHAR(32) UNIQUE);

CREATE TABLE accounts (id int UNIQUE, image varchar(100), tag varchar(32) UNIQUE, headerimage varchar(100), description varchar(255), city varchar(30));

CREATE TABLE speaks (id SERIAL PRIMARY KEY, userid int, text TEXT, voice TEXT, likes int);

CREATE TABLE subscriptions (id SERIAL PRIMARY KEY, userid int, subscriber_id int);

CREATE TABLE likes (id SERIAL PRIMARY KEY, speakid int, liker_id int);