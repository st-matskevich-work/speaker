DROP TABLE speaks;

CREATE TABLE speaks (id SERIAL PRIMARY KEY, userid int, text TEXT, voice TEXT, likes int, speaksDate TEXT);