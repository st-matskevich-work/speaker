CREATE TABLE attachments(id SERIAL PRIMARY KEY, attach_id int, data_type TEXT, attach_to TEXT, path TEXT UNIQUE);

ALTER TABLE speaks DROP COLUMN voice;
ALTER TABLE speaks DROP COLUMN likes;