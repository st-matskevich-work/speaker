create table chats (chat_id int, user_id int);
create table chats_state (id SERIAL PRIMARY KEY, chat_id int, user_id int, last_read_message_id int);