ALTER TABLE likes ADD COLUMN likes_date text;
ALTER TABLE comments ADD COLUMN comment_date text;
ALTER TABLE subscriptions ADD COLUMN sub_date text;