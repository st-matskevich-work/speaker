DROP TABLE message;
CREATE TABLE messages(id SERIAL PRIMARY KEY, sender_id int, receiver_id int, message TEXT, created_date TEXT);