CREATE TABLE respeaks (id SERIAL PRIMARY KEY, original_id int, speakid int, respeaker_id int);
ALTER TABLE speaks ADD COLUMN respeaked boolean;