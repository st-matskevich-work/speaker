const hostname = "localhost";
const port = normalizePort(process.env.PORT || 3030);


module.exports = { hostname, port };

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}