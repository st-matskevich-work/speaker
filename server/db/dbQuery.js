const pool = require('./db_config');
const stringGenerator = require('crypto-random-string');
const sha256 = require('sha256');
const moment = require('moment');
const uniqueFilename = require('unique-filename');
const os = require('os');
const fs = require('fs');
let io; 
let userSockets; 


let init = (IO,  users) => {
  io = IO;
  userSockets = users;
}

let getUser = async (id) => {
  let result = await pool.query('select * from users inner join accounts on users.id = accounts.id where users.id = $1', [id]).catch(err => { throw err });
  if(result.rowCount !== 0){
    return result.rows[0];
  }
  else return [];
}
let setUser = async (name, credentials, password) => {
  console.log(2);
  await pool.query('insert into users(name, credentials, password, status, setup, registrationDate) values ($1::text, $2::text, $3::text, $4, $5, $6)',
    [name, credentials, sha256(password), false, 0, moment().format('LL').toString()]).catch(err => { throw err; }); // 
  console.log("after insert");  
}
let getUserIdByTag = async (tag) => {
  let id = await pool.query('select * from accounts where tag = $1', [tag]);
  if (id.rowCount != 0)
    return id.rows[0].id;
  else
    return null;
}
let getId = async (credentials) => {
  let result = await pool.query('select * from users where credentials = $1::text', [credentials]).catch(err => { throw err });
  return result.rows[0].id;
}
let setAccount = async (id, image = "/images/defaultAvatar.jpg", headerImage = "/images/defaultHeader.jpg") => {
  await pool.query('insert into accounts(id, image, headerimage) values ($1, $2, $3)',
    [id, image, headerImage]).catch(err => { throw err });
  return true;
}
let getAccount = async (id) => {
  let accounts = await pool.query("select * from accounts where id = $1", [id]).catch(err => { throw err });
  return accounts.rows[0];
}
let setActivator = async (id, activationCode = stringGenerator({length: 7, type: 'numeric'})) => {
  await pool.query('insert into activators(id, code) values ($1, $2::text)',
    [id, activationCode]).catch(err => { throw err });
  return activationCode;
}
let setToken = async (id) => {
  let _token = stringGenerator({ length: 64, type: 'hex' });
  await pool.query('insert into tokens(id, token, valid) values ($1, $2::text, $3)',
    [id, _token, true]).catch(err => { throw err });
  return _token;
}
let checkUser = async (credentials) => {
  let response = false;
  await pool.query('select * from users where credentials = $1', [credentials])
    .then(result => {
      if (result.rowCount)
        response = true;
    })
    .catch(err => {
      throw err;
    });
  return response;
}
let checkUserId = async (id, email) => {
  let response = false;
  await pool.query('select * from users where credentials = $1 and id = $2', [email, id])
    .then(result => {
      if (result.rowCount)
        response = true;
    })
    .catch(err => {
      throw err;
    });
  return response;
}
let checkToken = async (token, id) => {
  let response = false;
  await pool.query('select * from tokens where token = $1 and id = $2 and valid = $3', [token, id, true])
    .then(result => {
      if (result.rowCount)
        response = true;
    })
    .catch(err => {
      response = false;
    })
  return response;
}
let checkActivator = async (id, activatorCode) => {
  let response = false;
  await pool.query('select * from activators where code = $1 and id = $2', [activatorCode, id])
    .then(result => {
      if (result.rowCount)
        response = true;
    })
    .catch(err => {
      response = false;
    })
  return response;
}
let updateUserStatus = async (id, status) => {
  await pool.query('update users set status = $1 where id = $2',
    [status, id]).catch(err => { throw err });
  return true;
}
let updateUserSetup = async (id, setup) => {
  await pool.query('update users set setup = $1 where id = $2',
    [setup, id]).catch(err => { throw err });
  return true;
}
let updateUserPassword = async (id, password) => {
  await pool.query('update users set password = $1 where id = $2',
    [sha256(password), id]).catch(err => { throw err });
  return true;
}
let updateTokenValid = async (id, valid) => {
  await pool.query('update tokens set valid = $1 where id = $2',
    [valid, id]).catch(err => { throw err });
  return true;
}
// let searchUserByName = async (name)=>{
//   let result = await pool.query('select * from users where name like $1', [name]).catch(err =>{ throw err });
//   return result.rows;
// }
// let searchUserByTag = async (tag)=>{
//   let result = await pool.query('select * from users where tag = $1', [tag]).catch(err =>{ throw err });
//   return result.rows;
// }

let notifyAll = async (userId, message = 'notified') => {
  let subs = await getSubscribers(userId).catch(err => { throw err });
  for await (subOne of subs) {
    let socket_id = userSockets[userId];
    socket_id ? io.to(socket_id).emit('notification', message) : false;
  }
}

let notify = async (user_for_notify_id, message = 'notified') => {
  console.log(userSockets);
  let socket_id = userSockets[user_for_notify_id];
  return socket_id ? io.to(socket_id).emit('notification', message) : false;
}

let setSpeak = async (userId, speakObj) => {
  if (speakObj.respeaked == 0) {
    let result = await pool.query('insert into speaks(userid, text, speaksDate, respeaked, respeaked_link) values ($1, $2::text, $3::text, $4, $5) returning id', [userId, speakObj.text, Date.now(), false, false]).catch(err => { throw err });
    var mention = speakObj.text.match(/@[A-Za-z0-9_-]{3,32}/g);
    if (mention != undefined) {
      for await (mentionOne of mention) {
        let ment_id = await getUserIdByTag(mentionOne.substring(1)).catch(err => { throw err });
        if (ment_id) await setMentions(result.rows[0].id, ment_id, 'speak').catch(err => { throw err })
      }
    }
    return result.rows[0].id;
  }
  else {
    let result = null;
    // // console.log(speakObj.text)
    // console.log(speakObj)
    // console.log(!speakObj.text);
    // console.log(speakObj.attachments.length);
    if(!speakObj.text && speakObj.attachments.length == 0) {
      // console.log('yeap');
      result = await pool.query('insert into speaks(userid, text, speaksDate, respeaked, respeaked_link) values ($1, $2::text, $3::text, $4, $5) returning id', [userId, null, Date.now(), true, true]).catch(err => { throw err }); //moment().format()
    }
    else {
      result = await pool.query('insert into speaks(userid, text, speaksDate, respeaked, respeaked_link) values ($1, $2::text, $3::text, $4, $5) returning id', [userId, speakObj.text, Date.now(), true, false]).catch(err => { throw err });
    }
    await setRespeaks(speakObj.respeaked, result.rows[0].id, userId).catch(err => { throw err });
    var mention = speakObj.text.match(/@[A-Za-z0-9_-]{3,32}/g);
    if (mention != undefined) {
      for await (mentionOne of mention) {
        let ment_id = await getUserIdByTag(mentionOne.substring(1)).catch(err => { throw err });
        if (ment_id) await setMentions(result.rows[0].id, ment_id, 'speak').catch(err => { throw err })
      }
    }
    let content_creator_id = await getContentCreator(speakObj.respeaked, "speak").catch(err => { throw err });
    if(content_creator_id == userId) return result.rows[0].id;
    console.log(result.rows[0].id, content_creator_id);
    if (!await notify(content_creator_id, { 
      type: "respeak",
      data: {
        original_speak: speakObj.respeaked,
        respeak: await getSpeakById(result.rows[0].id).catch(err =>{ throw err }),
        user: await getUserById(userId).catch(err =>{ throw err })
      },
      date: await getSpeakDate(result.rows[0].id).catch(err =>{ throw err }),
    })){
      console.log("ERRRRRRRR");
    }
    console.log(2)
    
    return result.rows[0].id;
  }
}

let getOriginalId = async (speak_id) =>{ 
  let result = await pool.query('select * from respeaks where speakid = $1', [speak_id]).catch(err => { throw err });
  if(result.rowsCount == 0) return null;
  return result.rows[0].original_id;
}

let getOriginalSpeak = async (speak_id)=>{ 
  let result = await pool.query('select * from respeaks inner join speaks on speaks.id = respeaks.original_id where respeaks.speakid = $1', [speak_id]).catch(err => { throw err });
  if(result.rowCount == 0) return null;
  return result.rows[0]
}

let getSpeaks = async (MyId, userId, isFeed = false) => {
  let resp = [];
  let speaks = await pool.query('select * from speaks where userid = $1', [userId]).catch(err => { throw err });
  //// console.log(speaks);
  for await (speakOne of speaks.rows) {
    //// console.log(speakOne.id);
    let account = await getAccount(userId).catch(err => { throw err; });
    let user = await getUser(userId).catch(err => { throw err; });
    let result = {
      id: speakOne.id,
      speak: speakOne.text,
      user: {
        id: userId,
        name: user.name,
        tag: account.tag,
        avatar: account.image
      },
      date: speakOne.speaksdate,
      likes: await getNumberOfLikes(speakOne.id, "speak").catch(err => { throw err }),
      liked: await checkLike(speakOne.id, MyId, "speak").catch(err => { throw err }),
      respeaks: await getNumberOfRespeaks(speakOne.id).catch(err => { throw err }),
      respeaked: null,
      // respeaksArray: await getAllRespeaks(speakOne.id, MyId).catch(err =>{throw err}),
      comments: await getNumberOfComments(speakOne.id).catch(err => { throw err }),
      attachments: await getAttachments(speakOne.id, 'speak').catch(err => { throw err })
    }
    if (speakOne.respeaked == true) {
      if(speakOne.respeaked_link == true){
        //if(isFeed == true){ continue; }
        //console.log(12734091935791379913);
        let id = await getOriginalId(speakOne.id).catch(err => { throw err });
        //console.log("id", id);
        if(!id) console.log("No Please God no");
        let speak = await getSpeak(id, MyId).catch(err => { throw err });
        //console.log(2)
        speak.uniqueId = speakOne.id;
        speak.date = speakOne.speaksdate;
        speak.respeak = true;
        console.log(speak);
        resp.push(speak);
        continue;
      }
      //else{
      let id = await getRespeakOriginalIdBySpeakId(speakOne.id).catch(err => { throw err });
      // console.log(id);
      let user = await pool.query('select * from users inner join accounts on users.id = accounts.id where users.id = $1',[id]).catch(err => { throw err });
      // console.log(user);
      let speak = await getOriginalSpeak(speakOne.id).catch(err =>{ throw err });
      // console.log(speak);
      //let speak_text = ""
      //if(speak.text == null || speak.text == "") speak_text = "Вложения";
      let respeakedObj = {
        id: await getUserRespeaks(speakOne.id, MyId).catch(err => { throw err }),
        name: user.rows[0].name,
        tag: user.rows[0].tag,
        image: user.rows[0].image,
        original_text: speak.text == "" ? "Вложения" : speak.text
      }
      //}
      if (respeakedObj.id != 0) {
        //// console.log(respeakedObj);
        result.respeaked = respeakedObj;
      }
    }
    //console.log(result);
    resp.push(result);
  }
  resp.sort((a, b) => b.date - a.date);
   //console.log("Speaks",resp);
  return resp;
}
let getLikedPosts = async (MyId, userId) => {
  let response = [];
  let speaks = await pool.query('select * from likes where liker_id = $1 and content_type = $2', [userId, "speak"]).catch(err => { throw err });
  if (speaks.rowCount != 0) {
    for await (speak of speaks.rows) response.push(await getSpeak(speak.content_id, MyId, speak.like_date).catch(err => { throw err }));
    response.sort((a, b) => b.like_date - a.like_date);
    return response;
  }
  else
    return response;
}
let getSpeak = async (speakId, MyId, like_date = null) => {
  // console.log(speakId);
  let speakROW = await pool.query('select * from users inner join accounts on users.id = accounts.id inner join speaks on users.id = speaks.userid where speaks.id = $1', [speakId]).catch(err => { throw err });
  let speak = speakROW.rows[0];
  // console.log(speak);
  let result = {
    id: speakId,
    speak: speak.text,
    user: {
      id: speak.userid,
      name: speak.name,
      tag: speak.tag,
      avatar: speak.image
    },
    date: speak.speaksdate,
    like_date: like_date,
    likes: await getNumberOfLikes(speakId, "speak").catch(err => { throw err }),
    liked: await checkLike(speakId, MyId, "speak").catch(err => { throw err }),
    respeaks: await getNumberOfRespeaks(speakId).catch(err => { throw err }),
    respeaked: null,
    comments: await getNumberOfComments(speakId).catch(err => { throw err }),
    commentsArray: await getComments(speakId, MyId).catch(err => { throw err }),
    attachments: await getAttachments(speakId, 'speak').catch(err => { throw err })
  }
  if (speak.respeaked == true) {
    if(speak.respeaked_link == true){
      console.log(12734091908502347238492342);
      let speakNew = await getSpeak(await getOriginalId(speakId).catch(err => { throw err }), MyId).catch(err => { throw err });
      speakNew.uniqueId = speakId;
      speakNew.date = speak.speaksdate;
      speak.respeak = true;
      return speakNew;
      //continue;
    }
    let id = await getRespeakOriginalIdBySpeakId(speakId).catch(err => { throw err });
    // console.log(id);
    let user = await pool.query('select * from users inner join accounts on users.id = accounts.id where users.id = $1',[id]).catch(err => { throw err });
    // console.log(user);
    let respSpeak = await getOriginalSpeak(speakId).catch(err =>{ throw err });
    // console.log(speak);
    //let speak_text = ""
    //if(speak.text == null || speak.text == "") speak_text = "Вложения";
    let respeakedObj = {
      id: await getUserRespeaks(speakId, MyId).catch(err => { throw err }),
      name: user.rows[0].name,
      tag: user.rows[0].tag,
      image: user.rows[0].image,
      original_text: respSpeak.text == "" ? "Вложения" : respSpeak.text
    }
    if (respeakedObj.id != 0) {
      //// console.log(respeakedObj);
      result.respeaked = respeakedObj;
    }
  }
  //// console.log('Speak', result);
  return result;
}
// let getAvatarImage = async (id)=>{
//   let result = await pool.query('select * from accounts where id = id').catch(err => { throw err });
//   return result.rows[0].image;
// }
let checkSubscription = async (userId, id) => {
  let result = await pool.query('select * from subscriptions where userid = $1 and subscriber_id = $2', [userId, id]).catch(err => { throw err });
  if (result.rowCount == 0) {
    return false;
  }
  else {
    return true;
  }
}
let setSubscription = async (userId, subscriber_id) => {
  let sub_date = Date.now();
  await pool.query('insert into subscriptions(userid, subscriber_id, sub_date) values ($1, $2, $3)', [userId, subscriber_id, sub_date]).catch(err => { throw err });
  let result = await pool.query('select * from users inner join accounts on users.id = accounts.id where users.id = $1', [subscriber_id]).catch(err => { throw err });
  await notify(userId, {
    type: "subscription",
    data: {
      user: {
        id: subscriber_id,
        name: result.rows[0].name,
        tag: result.rows[0].tag,
        avatar: result.rows[0].image
      }
    },
    date: sub_date
  })
}
let removeSubscription = async (userId, subscriber_id) => {
  return await pool.query('delete from subscriptions where userid = $1 and subscriber_id = $2', [userId, subscriber_id]).catch(err => { throw err });
}
let getNumberOfSubscriptions = async (userId) => {
  let result = await pool.query('select * from subscriptions where userid = $1', [userId]).catch(err => { throw err });
  return result.rowCount;
}
let getSubscribers = async (userId) => {
  let response = [];
  let result = await pool.query('select * from subscriptions where userid = $1', [userId]).catch(err => { throw err });
  if (result.rowCount != 0) for await (resultOne of result.rows) response.push(resultOne.subscriber_id);
  else return null;
}
let searchUserByName = async (string) => {
  let response = [];
  let resultUsers = await pool.query("select * from users where setup = $1 and upper(name) like $2", [5,`%${string.toUpperCase()}%`]).catch(err => { throw err });
  for await (user of resultUsers.rows) {
    //// console.log(user);
    let responseOne = {
      id: user.id,
      image: null,
      name: user.name,
      tag: null
    }
    let accounts = await pool.query("select * from accounts where id = $1", [user.id]).catch(err => { throw err });
    responseOne.image = accounts.rows[0].image;
    responseOne.tag = accounts.rows[0].tag;
    // // console.log('responseOne' + responseOne);
    response.push(responseOne);
  };
  return response;
}
let searchUserByTag = async (string) => {
  let response = [];
  let accounts = await pool.query("select * from accounts inner join users on accounts.id = users.id where users.setup = $1 and upper(accounts.tag) like $2", [5, `%${string.toUpperCase()}%`]).catch(err => { throw err });
  for await (account of accounts.rows) {
    // // console.log(account);
    let responseOne = {
      id: account.id,
      image: account.image,
      name: null,
      tag: account.tag
    }
    let users = await pool.query('select * from users where id = $1', [account.id]).catch(err => { throw err });
    responseOne.name = users.rows[0].name;
    response.push(responseOne);
  };
  return response;
}
let getInfoByIdAndTag = async (Myid, tag) => {
  let info = await pool.query('SELECT * FROM users INNER JOIN accounts ON users.id = accounts.id where accounts.tag = $1', [tag]).catch(err => { throw err })
  // console.log(info.rows[0]);
  let response = {
    status: "Success",
    email: info.rows[0].credentials,
    headerImage: info.rows[0].headerimage,
    description: info.rows[0].description,
    city: info.rows[0].city,
    date: info.rows[0].registrationdate,
    setup: info.rows[0].setup,
    id: info.rows[0].id,
    image: info.rows[0].image,
    tag: info.rows[0].tag,
    name: info.rows[0].name,
    following: await checkSubscription(info.rows[0].id, Myid).catch(err => { throw err }),
    subscribersNumber: await getNumberOfSubscriptions(info.rows[0].id).catch(err => { throw err }),
    subscribesNumber: await getNumberOfSubscribes(info.rows[0].id).catch(err => { throw err }),
    listeners: await getListeners(info.rows[0].id).catch(err => { throw err })
  };
  // console.log(response);
  return response;
}
let getInfoById = async (Myid) => {
  let info = await pool.query('SELECT * FROM users INNER JOIN accounts ON users.id = accounts.id where users.id = $1', [Myid]).catch(err => { throw err })
  let response = {
    status: "Success",
    email: info.rows[0].credentials,
    headerImage: info.rows[0].headerimage,
    description: info.rows[0].description,
    city: info.rows[0].city,
    date: info.rows[0].registrationdate,
    setup: info.rows[0].setup,
    id: Myid,
    image: info.rows[0].image,
    tag: info.rows[0].tag,
    name: info.rows[0].name,
    //following: await checkSubscription(info.rows[0].id, Myid),
    subscribersNumber: await getNumberOfSubscriptions(Myid).catch(err => { throw err }),
    subscribesNumber: await getNumberOfSubscribes(Myid).catch(err => { throw err }),
    listeners: await getListeners(Myid).catch(err => { throw err })
  };
  // console.log(response);
  return response;
}

let getRespeakOriginalIdBySpeakId = async (speak_id) => {
  let res = await pool.query('select * from respeaks where speakid = $1', [speak_id]).catch(err => { throw err });
  if (res.rows[0].original_id == undefined) {
    console.log('ARE you kidding me');
    //let info = await pool.query('SELECT * FROM users INNER JOIN speaks ON users.id = speaks.userid where speaks.id = $1', [speak_id]).catch(err => { throw err });
    return null;
  }
  else {
    let info = await pool.query('SELECT * FROM users INNER JOIN speaks ON users.id = speaks.userid where speaks.id = $1', [res.rows[0].original_id]).catch(err => { throw err });
    return info.rows[0].userid;
  }
}


let getNameBySpeakId = async (speak_id) => {
  let res = await pool.query('select * from respeaks where speakid = $1', [speak_id]).catch(err => { throw err });
  if (res.rows[0].original_id == undefined) {
    let info = await pool.query('SELECT * FROM users INNER JOIN speaks ON users.id = speaks.userid where speaks.id = $1', [speak_id]).catch(err => { throw err });
    return info.rows[0].name;
  }
  else {
    let info = await pool.query('SELECT * FROM users INNER JOIN speaks ON users.id = speaks.userid where speaks.id = $1', [res.rows[0].original_id]).catch(err => { throw err });
    return info.rows[0].name;
  }
}
let checkLike = async (content_id, liker_id, content_type) => {
  let result = await pool.query('select * from likes where content_id = $1 and liker_id = $2 and content_type = $3', [content_id, liker_id, content_type]).catch(err => { throw err });
  if (result.rowCount == 0) {
    return false;
  }
  else {
    return true;
  }
}


let getContentCreator = async (content_id, content_type)=>{ 
  if(content_type == "speak"){
    let result = await pool.query('select * from speaks where id = $1',[content_id]).catch(err => { throw err });
    return result.rowsCount == 0 ? null : result.rows[0].userid;
  }
  if(content_type == "comment"){
    let result = await pool.query('select * from comments where comment_id = $1',[content_id]).catch(err =>{ throw err });
    return result.rowsCount == 0 ? null : result.rows[0].user_id;
  }
  return null;
}


let setLike = async (content_id, liker_id, content_type) => {
  await pool.query('insert into likes(content_id, content_type, liker_id, like_date) values ($1, $2, $3, $4)', [content_id, content_type, liker_id, Date.now()]).catch(err => { throw err });
  let content_creator_id = await getContentCreator(content_id, content_type).catch(err => { throw err });
  console.log("Content creator id ===> ", content_creator_id);
  if(content_creator_id == liker_id) return;
  if(content_type == "speak"){
    let result = await pool.query('select * from speaks inner join likes on speaks.id = likes.content_id inner join users on likes.liker_id = users.id inner join accounts on accounts.id = users.id where speaks.id = $1 and likes.content_type = $2 order by likes.like_date desc',[content_id, "speak"]).catch(err => { throw err });
    if(result.rowCount == 0) console.log('Nothing found1');
    await notify(content_creator_id, { 
      type: "like",
      data: {
        content: {
          content_type: content_type,
          id: result.rows[0].content_id,
          text: result.rows[0].text == "" ? 'Вложения' : result.rows[0].text,
        },
        user: { // LIKER
          id: result.rows[0].liker_id,
          name: result.rows[0].name,
          tag: result.rows[0].tag,
          avatar: result.rows[0].image
        },
      },
      date: result.rows[0].like_date
    });
  }
  else if(content_type == "comment"){
    let result = await pool.query('select * from comments inner join likes on comments.comment_id = likes.content_id inner join users on likes.liker_id = users.id inner join accounts on accounts.id = users.id where comments.comment_id = $1 and likes.content_type = $2 order by likes.like_date desc',[content_id, "comment"]).catch(err => { throw err });
    if(result.rowCount == 0) console.log('Nothing found2');
    
    await notify(content_creator_id, { 
      type: "like",
      data: {
        content: {
          content_type: content_type,
          id: result.rows[0].content_id,
          text: result.rows[0].comment_text == "" ? 'Вложения' : result.rows[0].comment_text,
        },
        user: { // LIKER
          id: result.rows[0].liker_id,
          name: result.rows[0].name,
          tag: result.rows[0].tag,
          avatar: result.rows[0].image
        },
      },
      date: result.rows[0].like_date
    });
  }
}
let removeLike = async (content_id, liker_id, content_type) => {
  return await pool.query('delete from likes where content_id = $1 and liker_id = $2 and content_type = $3', [content_id, liker_id, content_type]).catch(err => { throw err });
}
let getNumberOfLikes = async (content_id, content_type) => {
  let result = await pool.query('select * from likes where content_id = $1 and content_type = $2', [content_id, content_type]).catch(err => { throw err });
  return result.rowCount;
}
let getNumberOfSubscribes = async (id) => {
  let result = await pool.query('select * from subscriptions where subscriber_id = $1', [id]).catch(err => { throw err });
  return result.rowCount;
}
let getAllPosts = async (userId, LastPostId = 0) => {
  let post = [];
  let subscribers = await pool.query('select * from subscriptions where subscriber_id = $1', [userId]).catch(err => { throw err });
  for await (subscriberOne of subscribers.rows) {
    post.push(... await getSpeaks(userId, subscriberOne.userid).catch(err => { throw err }));
  }
  post.push(... await getSpeaks(userId, userId, true).catch(err => { throw err}));
  post.sort((a, b)=> b.date - a.date);
  return post;
}
let setComment = async (userId, speakId, commentText) => {
  let commentDate = Date.now();
  let return_id = await pool.query('insert into comments(user_id, speak_id, comment_text, comment_date) values ($1, $2, $3, $4) returning comment_id', [userId, speakId, commentText, commentDate]).catch(err => { throw err });
  var mention = commentText.match(/@[A-Za-z0-9_-]{3,32}/g);
  if (mention != undefined) {
    for await (mentionOne of mention) {
      let ment_id = await getUserIdByTag(mentionOne.substring(1)).catch(err => { throw err });
      if (ment_id) await setMentions(return_id.rows[0].comment_id, ment_id, 'comment').catch(err => { throw err })
    }
  }

  let content_creator_id = await getContentCreator(speakId, "speak").catch(err => { throw err });
  if(content_creator_id == userId) return;
  let result = await pool.query('select * from speaks where id = $1', [speakId]).catch(err => { throw err });
  await notify(content_creator_id, {
    type: "comment",
    data: {
      speak: {
        id: speakId,
        text: result.rows[0].text == "" ? 'Вложения' : result.rows[0].text,
      },
      comment: {
        text: commentText,
      },
      user: await getUserById(userId).catch(err => { throw err })
    },
    date: commentDate
  });
  
  return return_id.rows[0].comment_id;
}
let getComments = async (speakId, MyId) => {
  let result = [];
  let info = await pool.query('select * from users inner join accounts on users.id = accounts.id inner join comments on users.id = comments.user_id inner join speaks on comments.speak_id = speaks.id where speaks.id = $1', [speakId]).catch(err => { throw err });
  for await (infoOne of info.rows) {
    result.push({
      name: infoOne.name,
      date: infoOne.comment_date,
      text: infoOne.comment_text,
      userid: infoOne.userid,
      commentid: infoOne.comment_id,
      likes: await getNumberOfLikes(infoOne.comment_id, "comment").catch(err => { throw err }),
      liked: await checkLike(infoOne.comment_id, MyId, "comment").catch(err => { throw err }),
      tag: infoOne.tag,
      image: infoOne.image
    });
  }
  result.sort((a, b) => b.commentid - a.commentid);
  // console.log(result);
  return result;
}
let getNumberOfComments = async (speakId) => {
  let result = await pool.query('select * from comments where speak_id = $1', [speakId]).catch(err => { throw err });
  return result.rowCount;
}
let getListeners = async (Myid) => {
  let listeners = [], counter = 0;
  let result = await pool.query('select * from users inner join accounts on users.id = accounts.id inner join subscriptions on users.id = subscriptions.subscriber_id where subscriptions.userid = $1', [Myid]).catch(err => { throw err });
  for await (resultOne of result.rows) {
    if (counter > 3) return listeners;
    if (resultOne != 0) {
      listeners.push({
        name: resultOne.name,
        image: resultOne.image
      })
    }
    ++counter;
  }
  return listeners;
}

let setAttachment = async (data, type, id) => {
  let data_type = '';
  if (type == 'wav')
    data_type = 'voice';
  else
    data_type = 'image';
  const filePath = uniqueFilename('/attachments', os.tmpdir(), id + Math.random() + '_') + '.' + type;
  fs.writeFile("./public" + filePath, data, { encoding: 'base64' }, async (err) => { throw err; });
  let return_id = await pool.query('insert into attachments(attach_id, data_type, attach_to, path) values ($1, $2, $3, $4) returning id', [null, data_type, null, filePath]).catch(err => { throw err });
  return return_id.rows[0].id;
}

let updateAttachment = async (attach_id, attach_to, id) => {
  await pool.query('update attachments set attach_id = $1, attach_to = $2 where id = $3', [attach_id, attach_to, id]).catch(err => { throw err });
}

let getAttachments = async (attach_id, attach_to) => {
  let attachments = []
  let result = await pool.query('select * from attachments where attach_to = $1 and attach_id = $2', [attach_to, attach_id]).catch(err => { throw err });
  for await (resultOne of result.rows) {
    attachments.push({
      path: resultOne.path,
      type: resultOne.data_type
    })
  }
  return attachments;
}

let getAttachmentDT = async (attach_id, attach_to, data_type) => {
  let result = await pool.query('select * from attachments where attach_to = $1 and attach_id = $2 and data_type = $3', [attach_to, attach_id, data_type]).catch(err => { throw err });
  if (result.rowCount != 0)
    return result.rows[0].path;
  else
    return null;
}

let getMediaPosts = async (userId) => {
  let speaks = await pool.query('select * from attachments where attach_id != $1', [0]).catch(err => { throw err });
  let speak_ids = [];
  for await (speak of speaks.rows) speak_ids.push(speak.attach_id);
  let uniqueSpeakIds = [...new Set(speak_ids)];
  let result = [];
  for await (speakU of uniqueSpeakIds) result.push(... await getSpeak(speakU, userId).catch(err => { throw err }));
  return result;
}

let setRespeaks = async (original_id, speakId, respeaker_id) => {
  return await pool.query('insert into respeaks(original_id, speakid, respeaker_id) values ($1, $2, $3)', [original_id, speakId, respeaker_id]).catch(err => { throw err });
}
let removeRespeaks = async (original_id, respeaker_id) => {
  return await pool.query('delete from respeaks where original_id = $1 and respeaker_id = $2', [speakId, respeaker_id]).catch(err => { throw err });
}
let getNumberOfRespeaks = async (original_id) => {
  let result = await pool.query('select * from respeaks where original_id = $1', [original_id]).catch(err => { throw err });
  return result.rowCount;
}
let checkRespeak = async (original_id, MyId) => {
  let result = await pool.query('select * from respeaks where original_id = $1 and respeaker_id = $2', [original_id, MyId]).catch(err => { throw err });
  if (result.rowCount == 0)
    return false;
  else
    return true;
}
let getUserRespeaks = async (speak_id) => {
  let result = await pool.query('select * from respeaks where speakid = $1', [speak_id]).catch(err => { throw err });
  if (result.rows[0].original_id == undefined) {
    return null;
  }
  else {
    return result.rows[0].original_id;
  }
}
let setMessage = async (sender_id, receiver_id, message, date) => {
  let result = await pool.query('insert into messages (sender_id, receiver_id, message, created_date) values ($1, $2, $3, $4) returning _id', [sender_id, receiver_id, message, date]).catch(err => { throw err });
  return result.rows[0]._id;
}
let getNameByUserId = async (user_id) => {
  let res = await pool.query('select name from users where id = $1', [user_id]).catch(err => { throw err });
  return res.rows[0].name;
}
let getImageByUserId = async (user_id) => {
  let res = await getAccount(user_id).catch(err => { throw err });
  return res.image;
}
let getTagByUserId = async (user_id) => {
  let res = await getAccount(user_id).catch(err => { throw err });
  return res.tag;
}

let setChat = async (sender_id, receiver_id)=>{
  let chatId = (await pool.query('select * from chats').catch(err =>{throw err})).rowCount;
  if(chatId == 0) ++chatId;
  await pool.query('insert into chats(chat_id, user_id) values ($1, $2)',[chatId, sender_id]).catch(err=>{throw err});
  await pool.query('insert into chats(chat_id, user_id) values ($1, $2)',[chatId, receiver_id]).catch(err=>{throw err});
  return chatId;
}

let checkIfChatSituated = async (sender_id, receiver_id)=>{ 
  let chat_ = await pool.query('select * from chats where user_id = $1 order by chat_id desc', [sender_id]).catch(err=>{throw err});
  if(chat_.rowCount != 0){
    for await (chatOne of chat_.rows){
      let _chat = await pool.query('select * from chats where user_id = $1 and chat_id = $2 order by chat_id desc', [receiver_id, chatOne.chat_id]).catch(err=>{throw err});
      if(_chat.rowCount != 0){
        console.log(chatOne.chat_id);
        return chatOne.chat_id;
      }
      else
        continue; 
    }
  }
  else 
    return false;
}




let getMessages = async (sender_id) => {
  var response = [];
  console.log("Sender ",sender_id);
  let resultSender = await pool.query('select * from messages inner join users on users.id = messages.receiver_id inner join accounts on users.id = accounts.id where messages.sender_id = $1 or messages.receiver_id = $1 order by messages._id desc', [sender_id]).catch(err => { throw err });
  //let resultReceiver = await pool.query('select * from messages inner join users on users.id = messages.receiver_id inner join accounts on users.id = accounts.id where messages.receiver_id = $1 order by messages._id desc',[sender_id]).catch(err =>{throw err});
  
  for await (resultSenderOne of resultSender.rows) {
    if (response[resultSenderOne.sender_id] == undefined && resultSenderOne.sender_id == resultSenderOne.receiver_id && resultSenderOne.receiver_id == sender_id) { // && validator[resultSenderOne.sender_id] != resultSenderOne.receiver_id
      console.log('0001');
      message_id = await getLastReadMessage(sender_id, resultSenderOne.sender_id, resultSenderOne._id).catch(err =>{throw err});
      console.log("here " + message_id);
      // console.log("here it is " + resultSenderOne._id);
      response[sender_id] = {
        user: { name: resultSenderOne.name, image: resultSenderOne.image, id: resultSenderOne.sender_id, tag: resultSenderOne.tag },
        messages: [{
          _id: resultSenderOne._id,
          text: resultSenderOne.message,
          createdAt: resultSenderOne.created_date,
          user: {
            _id: resultSenderOne.sender_id,
            name: resultSenderOne.name,
            avatar: resultSenderOne.image
          },
          voice: await getAttachmentDT(resultSenderOne._id, 'message', 'voice').catch(err => { throw err }),
          image: await getAttachmentDT(resultSenderOne._id, 'message', 'image').catch(err => { throw err }),
          read: message_id >= resultSenderOne._id ? true : false 
        }]
      }
      
      //validator[resultSenderOne.sender_id] = resultSenderOne.sender_id;
      continue;
    }
    if ((response[resultSenderOne.receiver_id] == undefined) && resultSenderOne.sender_id == sender_id) {
      console.log('0002');
      //// console.log(resultSenderOne.sender_id == sender_id && validator[resultSenderOne.sender_id] != resultSenderOne.receiver_id);
      message_id = await getLastReadMessage(resultSenderOne.receiver_id, resultSenderOne.sender_id, resultSenderOne._id).catch(err =>{throw err});
      console.log("here " + message_id);
      // console.log("here it is " + resultSenderOne._id);
      response[resultSenderOne.receiver_id] = {
        user: { name: await getNameByUserId(resultSenderOne.receiver_id), image: await getImageByUserId(resultSenderOne.receiver_id), id: resultSenderOne.receiver_id, tag: await getTagByUserId(resultSenderOne.receiver_id) },
        messages: [{
          _id: resultSenderOne._id,
          text: resultSenderOne.message,
          createdAt: resultSenderOne.created_date,
          user: {
            _id: resultSenderOne.sender_id,
            name: await getNameByUserId(resultSenderOne.sender_id),
            avatar: await getImageByUserId(resultSenderOne.sender_id)
          },
          voice: await getAttachmentDT(resultSenderOne._id, 'message', 'voice').catch(err => { throw err }),
          image: await getAttachmentDT(resultSenderOne._id, 'message', 'image').catch(err => { throw err }),
          read: message_id >= resultSenderOne._id ? true : false 
        }]
      }
      // validator[resultSenderOne.sender_id] = resultSenderOne.receiver_id;
      // validator[resultSenderOne.receiver_id] = resultSenderOne.sender_id;
      continue;
    }
    if ((response[resultSenderOne.sender_id] == undefined) && resultSenderOne.receiver_id == sender_id) {
      console.log('0003');
      message_id = await getLastReadMessage(sender_id, resultSenderOne.sender_id, resultSenderOne._id).catch(err =>{throw err})
      console.log("here " + message_id);
      // console.log("here it is " + resultSenderOne._id);
      response[resultSenderOne.sender_id] = {
        user: { name: await getNameByUserId(resultSenderOne.sender_id), image: await getImageByUserId(resultSenderOne.sender_id), id: resultSenderOne.sender_id, tag: await getTagByUserId(resultSenderOne.sender_id) },
        messages: [{
          _id: resultSenderOne._id,
          text: resultSenderOne.message,
          createdAt: resultSenderOne.created_date,
          user: {
            _id: resultSenderOne.sender_id,
            name: await getNameByUserId(resultSenderOne.sender_id),
            avatar: await getImageByUserId(resultSenderOne.sender_id)
          },
          voice: await getAttachmentDT(resultSenderOne._id, 'message', 'voice').catch(err => { throw err }),
          image: await getAttachmentDT(resultSenderOne._id, 'message', 'image').catch(err => { throw err }),
          read: message_id >= resultSenderOne._id ? true : false 
        }]
      }
    }
    continue;
  }
  for await (resOne of response){
    if(resOne !== undefined && resOne.messages !== undefined && resOne.messages[0].user._id == sender_id){
      resOne.messages[0].read = true;
    }
  }
  return response;
}


//       response[resultSenderOne.sender_id] = {
//         user: { name:  await getNameByUserId(resultSenderOne.sender_id).catch(err =>{throw err}), image: await getImageByUserId(resultSenderOne.sender_id).catch(err =>{throw err}), id: resultSenderOne.sender_id, tag: await getTagByUserId(resultSenderOne.sender_id).catch(err =>{throw err}) },
//         messages: new Array([... await getMessage(resultSenderOne.sender_id, resultSenderOne.receiver_id).catch(err =>{throw err}), ... await getMessage(resultSenderOne.receiver_id, resultSenderOne.sender_id).catch(err =>{throw err})].sort((a,b)=> b._id - a._id)[0])
//           // {
//           //   _id: resultSenderOne._id,
//           //   text: resultSenderOne.message,
//           //   createdAt: resultSenderOne.created_date,
//           //   user:{
//           //     _id: resultSenderOne.sender_id,
//           //     name: await getNameByUserId(resultSenderOne.sender_id).catch(err =>{throw err}),
//           //     avatar: await getImageByUserId(resultSenderOne.sender_id).catch(err =>{throw err})
//           //   },
//           //   image: null,
//           //   voice: null
//           // }
//       }
//     }
//     // else{
//     //   //response[resultSenderOne.id].user = { name: resultSenderOne.name, image: await getImageByUserId(resultSenderOne.id).catch(err =>{throw err}), id: resultSenderOne.id, tag: resultSenderOne.tag};
//     //   response[resultSenderOne.id].messages.push({
//     //     _id: resultSenderOne._id,
//     //     text: resultSenderOne.message,
//     //     createdAt: resultSenderOne.created_date, 
//     //     user:{
//     //       _id: resultSenderOne.sender_id,
//     //       name: await getNameByUserId(resultSenderOne.sender_id).catch(err =>{throw err}),
//     //       avatar: await getImageByUserId(resultSenderOne.sender_id).catch(err =>{throw err})
//     //     },
//     //     image: null,
//     //     voice: null
//     //   });
//     // }
//   }
//   return response;
// }

let getMessageOne = async (sender_id, receiver_id, admin = false) => {
  var response = [];
  let result = (await pool.query('select * from messages where sender_id = $1 and receiver_id = $2 order by _id desc', [sender_id, receiver_id]).catch(err => { throw err })).rows[0];
  response = {
    user: { name: await getNameByUserId(result.sender_id).catch(err => { throw err }), image: await getImageByUserId(result.sender_id).catch(err => { throw err }), id: result.sender_id, tag: await getTagByUserId(result.sender_id).catch(err => { throw err }), admin: admin},
    messages: [{
      _id: result._id,
      text: result.message,
      createdAt: result.created_date,
      user: {
        _id: result.sender_id,
        name: await getNameByUserId(result.sender_id).catch(err => { throw err }),
        avatar: "https://speaker-app.herokuapp.com" + await getImageByUserId(result.sender_id).catch(err => { throw err })
      },
      voice: await getAttachmentDT(result._id, 'message', 'voice').catch(err => { throw err }),
      image: await getAttachmentDT(result._id, 'message', 'image').catch(err => { throw err })
    }]
  }
  return response;
}



let getMessage = async (sender_id, receiver_id) => {
  var response = [];
  let result = await pool.query('select * from messages where (sender_id = $1 and receiver_id = $2) or (sender_id = $2 and receiver_id = $1) order by _id desc', [sender_id, receiver_id]).catch(err => { throw err });
  for await (resultOne of result.rows) {
    response.push({
      _id: resultOne._id,
      text: resultOne.message,
      createdAt: resultOne.created_date,
      user: {
        _id: resultOne.sender_id,
        name: await getNameByUserId(resultOne.sender_id).catch(err => { throw err }),
        avatar: "https://speaker-app.herokuapp.com" + await getImageByUserId(resultOne.sender_id).catch(err => { throw err })
      },
      voice: await getAttachmentDT(resultOne._id, 'message', 'voice').catch(err => { throw err }),
      image: await getAttachmentDT(resultOne._id, 'message', 'image').catch(err => { throw err })
    })
  }
  return response;
}

let setMentions = async (content_id, mention_id, content_type) => {
  // console.log(content_id, mention_id);
  let res = await pool.query('insert into mentions(content_id, mention_id, content_type) values ($1, $2, $3) returning id', [content_id, mention_id, content_type]).catch(err => { throw err });
  return res.rows[0].id;
}

let getMentionsComment = async (userId) => {
  let response = []
  let res = await pool.query("select * from mentions inner join comments on comments.comment_id = mentions.content_id inner join accounts on comments.user_id = accounts.id inner join users on users.id = comments.user_id where mention_id = $1 and content_type = $2", [userId, 'comment']).catch(err => { throw err });
  if (res.rowCount != 0) {
    res.rows.sort((a, b) => b.id - a.id);
    for await (resOne of res.rows)
      response.push({
        comment: {
          id: resOne.content_id,
          speak_id: resOne.speak_id,
          text: resOne.comment_text,
        },
        user: {
          id: resOne.user_id,
          name: resOne.name,
          tag: resOne.tag,
          avatar: resOne.image
        },
        date: resOne.comment_date
      })
    return response;
  }
  else {
    return []
  }
}

let getMentionsSpeak = async (userId) => {
  let response = []
  let res = await pool.query("select * from mentions inner join speaks on speaks.id = mentions.content_id inner join accounts on speaks.userid = accounts.id inner join users on users.id = speaks.userid where mention_id = $1 and content_type = $2", [userId, 'speak']).catch(err => { throw err });
  if (res.rowCount != 0) {
    res.rows.sort((a, b) => b.id - a.id);
    for await (resOne of res.rows)
      response.push({
        speak: {
          id: resOne.content_id,
          text: resOne.text,
        },
        user: {
          id: resOne.userid,
          name: resOne.name,
          tag: resOne.tag,
          avatar: resOne.image
        },
        date: resOne.speaksdate
      })
    return response;
  }
  else {
    return []
  }
}

let getLikeNotifications = async (user_id) => {
  let response = [];
  let result = await pool.query('select * from speaks inner join likes on speaks.id = likes.content_id inner join users on users.id = likes.liker_id inner join accounts on users.id = accounts.id where speaks.userid = $1 and likes.content_type = $2 order by speaks.id desc', [user_id, "speak"]).catch(err => { throw err });
  if (result.rowCount != 0) {
    for await (resultOne of result.rows) {
      if(resultOne.liker_id != user_id){
        if(resultOne.content_type == "speak"){
          response.push({
            type: "like",
            data: {
              content: {
                content_type: resultOne.content_type,
                id: resultOne.content_id,
                text: resultOne.text == "" ? 'Вложения' : resultOne.text,
              },
              user: { // LIKER
                id: resultOne.liker_id,
                name: resultOne.name,
                tag: resultOne.tag,
                avatar: resultOne.image
              },
            },
            date: resultOne.like_date
          })
        }
      }
    }
  }
  let comment_result = await pool.query('select * from comments inner join likes on comments.comment_id = likes.content_id inner join users on users.id = likes.liker_id inner join accounts on users.id = accounts.id where comments.user_id = $1 and likes.content_type = $2 order by comments.comment_id desc', [user_id, "comment"]).catch(err => { throw err });
  if (comment_result.rowCount != 0) {
    for await (resultOne of comment_result.rows) {
      if(resultOne.liker_id != user_id){
        if(resultOne.content_type == "comment"){
          response.push({
            type: "like",
            data: {
              content: {
                id: resultOne.speak_id,
                content_type: resultOne.content_type,
                comment_id: resultOne.content_id,
                text: resultOne.comment_text == "" ? 'Вложения' : resultOne.comment_text,
              },
              user: { // LIKER
                id: resultOne.liker_id,
                name: resultOne.name,
                tag: resultOne.tag,
                avatar: resultOne.image
              },
            },
            date: resultOne.like_date
          })
        }
      }
    }
  }
  return response;
}

let getSpeakById = async (speak_id) => {
  let result = await pool.query('select * from speaks where id = $1', [speak_id]).catch(err => { throw err });
  if (result.rowCount != 0) {
    return {
      id: result.rows[0].id,
      text: result.rows[0].text == "" ? 'Вложения' : result.rows[0].text,
    }
  }
  else {
    return null;
  }
}

let getSpeakDate = async(speak_id)=>{
  let result = await pool.query('select * from speaks where id = $1', [speak_id]).catch(err => { throw err });
  if (result.rowCount != 0) {
    return result.rows[0].speaksdate;
  }
  else {
    return null;
  }
}

let getRespeakNotifications = async (user_id) => {
  let response = [];
  let result = await pool.query('select * from users inner join speaks on users.id = speaks.userid inner join respeaks on respeaks.original_id = speaks.id where users.id = $1', [user_id]).catch(err => { throw err }); //select * from respeaks inner join speaks on speaks.id = respeaks.speakid inner join users on respeaks.respeaker_id = users.id inner join accounts on accounts.id = users.id where original_id = $1 order by users.id desc
  if (result.rowCount != 0) {
    for await (resultOne of result.rows) {
      if(resultOne.respeaker_id != user_id)
        response.push({
          type: "respeak",
          data: {
            original_speak: await getSpeakById(resultOne.original_id).catch(err => { throw err }),
            respeak: await getSpeakById(resultOne.speakid).catch(err =>{ throw err }),
            user: await getUserById(resultOne.respeaker_id).catch(err =>{ throw err })
          },
          date: await getSpeakDate(resultOne.speakid).catch(err =>{ throw err }),
        })
    }
    return response;
  }
  else {
    return [];
  }
}

let getUserById = async (user_id) => {
  let res = await pool.query('select * from users inner join accounts on accounts.id = users.id where users.id = $1', [user_id]).catch(err => { throw err });
  if (res.rowCount != 0) {
    return {
      id: res.rows[0].id,
      name: res.rows[0].name,
      tag: res.rows[0].tag,
      avatar: res.rows[0].image
    }
  }
  else {
    return null;
  }
}

let getCommentNotifications = async (user_id) => {
  let response = [];
  let result = await pool.query('select * from users inner join accounts on accounts.id = users.id inner join speaks on speaks.userid = users.id inner join comments on speaks.id = comments.speak_id where users.id = $1', [user_id]).catch(err => { throw err });
  if (result.rowCount != 0) {
    for await (resultOne of result.rows) {
      if(resultOne.user_id != user_id)
        response.push({
          type: "comment",
          data: {
            speak: {
              id: resultOne.speak_id,
              text: resultOne.text == "" ? 'Вложения' : resultOne.text,
            },
            comment: {
              text: resultOne.comment_text,
            },
            user: await getUserById(resultOne.user_id).catch(err => { throw err })
          },
          date: resultOne.comment_date
        })
    }
    return response;
  }
  else {
    return [];
  }
}

let getSubsNotifications = async (user_id) => {
  let response = [];
  let result = await pool.query('select * from subscriptions inner join users on users.id = subscriptions.subscriber_id inner join accounts on subscriptions.subscriber_id = accounts.id where subscriptions.userid = $1', [user_id]).catch(err => { throw err });
  if (result.rowCount != 0) {
    for await (resultOne of result.rows) {
      if(resultOne.subscriber_id != user_id)
        response.push({
          type: "subscription",
          data: {
            user: {
              id: resultOne.subscriber_id,
              name: resultOne.name,
              tag: resultOne.tag,
              avatar: resultOne.image
            }
          },
          date: resultOne.sub_date
        })
    }
    return response;
  }
  else {
    return [];
  }
}

let getNotifications = async (user_id) => {
  let likes = await getLikeNotifications(user_id).catch(err =>{ throw err });
  let respeaks = await getRespeakNotifications(user_id).catch(err =>{ throw err });
  let comments = await getCommentNotifications(user_id).catch(err =>{ throw err });
  let subs = await getSubsNotifications(user_id).catch(err =>{ throw err });

  let result = [... likes, ... respeaks, ... comments, ... subs];
  // console.log(result);
  result.sort((a, b)=> b.date - a.date); // sort result 
  // console.log("\n\n AfterSort: \n\n");
  // console.log(result);
  return result;
}

let setAdmin = async (user_id)=>{
  await pool.query('insert into admins (user_id) values ($1)', [user_id]).catch(err =>{ throw err });
}

let getAdminList = async ()=>{ 
  let result = [];
  let list = await pool.query('select * from admins').catch(err =>{ throw err });
  if(list.rowCount !== 0){
    for await(admin of list.rows){
      result.push(admin.user_id);
    }  
  }
  return result
}

let checkAdmin = async (user_id)=>{ 
  let result = await pool.query('select * from admins where user_id = $1', [user_id]).catch(err =>{ throw err });
  if (result.rowCount == 0) {
    return false;
  }
  else {
    return true;
  }
}

let setLastReadMessage = async (chatId, userId, lastReadMessageId)=>{
  await pool.query('insert into chats_state(chat_id, user_id, last_read_message_id) values($1, $2, $3)', [chatId, userId, lastReadMessageId]).catch(err =>{ throw err });
}

let getLastReadMessage = async (senderId, receiverId, return_id) => {
  console.log("senderId", senderId, "receiverId", receiverId);
  let chatId = await checkIfChatSituated(senderId, receiverId).catch(err =>{ throw err });
  if(!chatId){
    throw new Error("Chat not situated");
  }
  else{
    let lastMessage = await pool.query('select * from chats_state where chat_id = $1 and user_id = $2 order by id desc', [chatId, senderId]).catch(err =>{ throw err });
    if(lastMessage.rowCount == 0) return 0; 
    return lastMessage.rows[0].last_read_message_id;
  }
}

let setAvatar = async (user_id, new_avatar_image, extension) =>{
  const filePath = uniqueFilename('/images/avatars',os.tmpdir(), user_id * 10000 * Math.random() + '_') + '.' + extension;
  console.log(filePath);
  
  fs.writeFile("./public" + filePath, new_avatar_image, {encoding: 'base64'}, async (err) => {
    if(err) throw err;
    await pool.query('update accounts set image = $2 where id = $1',[user_id, filePath]).catch(err=>{ throw err });
  });
}

// let getMessages = async (sender_id) =>{
//   var response = [];
//   let resultSender = await pool.query('select * from messages inner join users on users.id = messages.receiver_id inner join accounts on users.id = accounts.id where messages.sender_id = $1 or messages.receiver_id = $1',[sender_id]).catch(err =>{throw err});
//   if(resultSender.rowCount != 0){
//     for await(resultSenderOne of resultSender.rows){
//       if(response[resultSenderOne.id] == undefined){
//         response[resultSenderOne.id] = {
//           user: { name: resultSenderOne.name, image: await getImageByUserId(resultSenderOne.id).catch(err =>{throw err}), id: resultSenderOne.id, tag: resultSenderOne.tag},
//           messages: [{
//             _id: resultSenderOne._id,
//             text: resultSenderOne.message,
//             createdAt: resultSenderOne.created_date,
//             user:{
//               _id: resultSenderOne.sender_id,
//               name: await getNameByUserId(resultSenderOne.sender_id).catch(err =>{throw err}),
//               avatar: await getImageByUserId(resultSenderOne.sender_id).catch(err =>{throw err})
//             },
//             image: null,
//             voice: null
//           }]
//         }
//       }
//       else{
//         //response[resultSenderOne.id].user = { name: resultSenderOne.name, image: await getImageByUserId(resultSenderOne.id).catch(err =>{throw err}), id: resultSenderOne.id, tag: resultSenderOne.tag};
//         response[resultSenderOne.id].messages.push({
//           _id: resultSenderOne._id,
//           text: resultSenderOne.message,
//           createdAt: resultSenderOne.created_date, 
//           user:{
//             _id: resultSenderOne.sender_id,
//             name: await getNameByUserId(resultSenderOne.sender_id).catch(err =>{throw err}),
//             avatar: await getImageByUserId(resultSenderOne.sender_id).catch(err =>{throw err})
//           },
//           image: null,
//           voice: null
//         });
//       }
//     }
//   }
//   // let resultSenderSender = await pool.query('select * from messages inner join users on users.id = messages.sender_id inner join accounts on users.id = accounts.id where messages.receiver_id = $1',[sender_id]).catch(err =>{throw err});
//   // if(resultSender.rowCount != 0){
//   //   for await(resultSenderOne of resultSender.rows){
//   //     if(response[resultSenderOne.id] == undefined){
//   //       response[resultSenderOne.id] = {
//   //         user: { name: resultSenderOne.name, image: await getImageByUserId(resultSenderOne.id).catch(err =>{throw err}), id: resultSenderOne.id, tag: resultSenderOne.tag},
//   //         messages: [{
//   //           _id: resultSenderOne._id,
//   //           text: resultSenderOne.message,
//   //           createdAt: resultSenderOne.created_date,
//   //           user:{
//   //             _id: resultSenderOne.sender_id,
//   //             name: await getNameByUserId(resultSenderOne.sender_id).catch(err =>{throw err}),
//   //             avatar: await getImageByUserId(resultSenderOne.sender_id).catch(err =>{throw err})
//   //           },
//   //           image: null,
//   //           voice: null
//   //         }]
//   //       }
//   //     }
//   //     else{
//   //       //response[resultSenderOne.id].user = { name: resultSenderOne.name, image: await getImageByUserId(resultSenderOne.id).catch(err =>{throw err}), id: resultSenderOne.id, tag: resultSenderOne.tag};
//   //       response[resultSenderOne.id].messages.push({
//   //         _id: resultSenderOne._id,
//   //         text: resultSenderOne.message,
//   //         createdAt: resultSenderOne.created_date, 
//   //         user:{
//   //           _id: resultSenderOne.sender_id,
//   //           name: await getNameByUserId(resultSenderOne.sender_id).catch(err =>{throw err}),
//   //           avatar: await getImageByUserId(resultSenderOne.sender_id).catch(err =>{throw err})
//   //         },
//   //         image: null,
//   //         voice: null
//   //       });
//   //     }
//   //   }
//   // }
//   for await(respOne of response) if(respOne != undefined) respOne.messages.reverse();
//   return response;
// }

module.exports = {
  init,
  userSockets,
  getUser,
  setUser,
  getId,
  setAccount,
  setActivator,
  setToken,
  checkUser,
  checkToken,
  checkActivator,
  updateUserPassword,
  updateUserSetup,
  updateUserStatus,
  updateTokenValid,
  searchUserByTag,
  searchUserByName,
  setSpeak,
  checkUserId,
  getSpeaks,
  checkSubscription,
  setSubscription,
  getNumberOfSubscriptions,
  getNumberOfSubscribes,
  removeSubscription,
  getInfoByIdAndTag,
  setLike,
  checkLike,
  removeLike,
  getNumberOfLikes,
  getInfoById,
  getAllPosts,
  setComment,
  getSpeak,
  getListeners,
  setAttachment,
  updateAttachment,
  getAttachmentDT,
  getNumberOfRespeaks,
  getUserRespeaks,
  getLikedPosts,
  getMediaPosts,
  setMessage,
  getMessages,
  getMessage,
  getMessageOne,
  setMentions,
  getMentionsSpeak,
  getMentionsComment,
  getNotifications,
  getUserIdByTag,
  setAdmin,
  checkAdmin,
  setChat,
  checkIfChatSituated,
  setLastReadMessage,
  getLastReadMessage,
  getAdminList,
  setAvatar
}