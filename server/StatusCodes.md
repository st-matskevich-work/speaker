## Status Code decryption
---
**200 - Success**
**400 - Invalid data**
**401 - Tag is already exists**
**414 - Wrong id**
**402 - Phone number is already exists**
**410 - Phone number is not exists**
**403 - Email is already exists**
**409 - Email doesn't exist**
**405 - User not found**
**406 - Wrong activation code**
**440 - Wrong password**
**408 - Account is not activated**
**418 - SQL error**

---