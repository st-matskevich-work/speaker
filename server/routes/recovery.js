const express = require('express');
const router = express.Router();
const dbQuery = require('../db/dbQuery');
const bodyparser = require('body-parser');
const stringGenerator = require('crypto-random-string');
const nodemailer = require("nodemailer");
const fetch = require('node-fetch');

const sendpulse = require('sendpulse-api');
const API_USER_ID = "9f2646648d7bf444a85ad5675bb32cae";
const API_SECRET = "8313752e99494cd4609caa1aaa2d9af1";
const TOKEN_STORAGE = "/tmp/";
const https = require('https');

router.use(bodyparser.json());

router
.all('/', (req, res, next)=>{
  next();
})
.get('/', (req, res, next)=>{
  res.render('res', { statuscode: '400', message: 'Bad request' });
})
.post('/', async (req, res, next)=>{
  if(req.body.email == undefined && req.body.phone == undefined){
    next();
  }
  else{
    if(req.body.email != undefined && req.body.email != ""){
      await dbQuery.checkUser(req.body.email)
      .then(async result =>{
        if(!result){
          res.statusCode = 409;
          res.end(JSON.stringify({ status:"Пользователь с такой почтой не найден" }));
        }
        else{
          let activeCode = stringGenerator({length: 7, type: 'numeric'});
          sendMail(req.body.email, activeCode);
        
          await dbQuery.setActivator(await dbQuery.getId(req.body.email), activeCode)
          .then(async result =>{
            res.statusCode = 200;
            res.end(JSON.stringify({ status:"Success", id: (await dbQuery.getId(req.body.email)) }));
          })
          .catch(err => { throw err });        
        }
      })
      .catch(err => { throw err });
    }
    else if (req.body.phone != undefined && req.body.phone != "") {
      await dbQuery.checkUser(req.body.phone)
      .then(async result =>{
        if(!result){
          res.statusCode = 410;
          res.end(JSON.stringify({ status:"Пользователь с таким номером не найден" }));
        }
        else{
          let activeCode = stringGenerator({length: 7, type: 'numeric'});
          sendSMS(req.body.phone, activeCode);
        
          await dbQuery.setActivator(await dbQuery.getId(req.body.phone), activeCode)
          .then(async result =>{
            res.statusCode = 200;
            res.end(JSON.stringify({ status:"Success", id: (await dbQuery.getId(req.body.phone)) }));
          })
          .catch(err => { throw err });        
        }
      })
      .catch(err => { throw err });
    }
  }
})
.post('/', async (req, res, next)=>{
  if(!req.body.id || !req.body.recoveryCode){
    next();
  }
  else {
    await dbQuery.checkActivator(req.body.id, req.body.recoveryCode)
    .then(async result =>{
      if(!result){
        res.statusCode = 406;
        res.end(JSON.stringify({ status:"Неправильный код активации" }));
      }
      else{
        let _token = await dbQuery.setToken(req.body.id).catch(err =>{ throw err });
        res.statusCode = 200;
        res.end(JSON.stringify({ status:"Success", token: _token }));
      }
    })
    .catch(err =>{ throw err});
  }
})
.post('/', async (req, res, next)=>{
  if(!req.body.id || !req.body.token || !req.body.password){
    res.statusCode = 400;
    res.end(JSON.stringify({ status: "Неверные данные" }));
  }
  else {
    await dbQuery.checkToken(req.body.token, req.body.id)
    .then(async result =>{
      if(!result){
        res.statusCode = 400;
        res.end(JSON.stringify({ status:"Wrong token" }));
      }
      else{
        await dbQuery.updateUserPassword(req.body.id, req.body.password).catch(err =>{ throw err });
        // await dbQuery.updateTokenValid(req.body.id, true);
        res.statusCode = 200;
        res.end(JSON.stringify({ status:"Password update successfully" }));
      }
    })
    .catch(err =>{ throw err});
  }
})

module.exports = router;

async function sendSMS(phone, activationCode){
  await fetch('https://events.sendpulse.com/events/id/5e48c5280a6a45dcd9461805ec234e77/7347718', { 
    method: 'POST',
    body: JSON.stringify({
      email: 'email',
      phone: phone,
      code: activationCode
    }),
    headers: { 'Content-Type': 'application/json' } 
  })
  .then(res => res.json())
  .then(json => console.log(json));
  //.catch(err => { throw err });
}

async function sendMail(email, activeCode){  
  var transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      type: 'OAuth2',
      user: 'speakertest60@gmail.com',
      clientId: '1073326472229-pq4fpb6sblqls91ttlrb3lq43gar3ohg.apps.googleusercontent.com',
      clientSecret: '6BHE_CCEu1ji3OKy5_vl1oKd',
      refreshToken: '1//04lJHo2c4yqopCgYIARAAGAQSNwF-L9Ir7fyMTbBFQgh-mTiJ9C165XbZyGC5L18eEqccHKYerqwARGG-gXJqJ23cLpSfeCfa4wI',
    }
  });
  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"Speaker Company" <speakertest60@gmail.com>', // sender address
    to: email.toString(), // list of receivers
    subject: "Hello! That's your code for password recovery. Don't tell it anybody.", // Subject line
    html: `
    <html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=0.7">
    <title>Спикер - Восстановление пароля</title>
    <style type="text/css">
        body {
            margin: 0;
        }
        #header {
            height: 100px;
            width: 100%;
            display: flex;
            background: #0AB4BE;
        }
        #icon {
            width: 100px;
            height: 100px;
            background: url('https://speaker-app.herokuapp.com/images/icon.png');
            background-size: 100px 100px;
        }
        #label {
            flex: 1;
            background: url('https://speaker-app.herokuapp.com/images/label.png');
            background-size: contain;
            background-repeat: no-repeat;
            margin-top: 20px;
            margin-bottom: 10px;
        }
        #recover_title {
            font-family: 'Roboto', sans-serif;
            margin-top: 10px;
            margin-bottom: 10px;
            font-weight: 500;
            font-size: 45px;
            font-style: normal;
            color: rgba(0, 0, 0, 0.87);
            margin-left: 5px;
            margin-right: 5px;
        }
        #recover_text {
            font-family: 'Roboto', sans-serif;
            font-weight: 100;
            font-size: 20px;
            font-style: normal;
            color: rgba(0, 0, 0, 0.87);
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 0;
            margin-bottom: 0;
        }
    </style>
</head>
<body>
    <div id="header">
        <div id="icon"></div>
        <div id="label"></div>
    </div>
    <h1 id="recover_title">Код восстановления пароля</h1>
    <h4 id="recover_text">Привет! Вот ваш код для восстановления пароля: ${activeCode}. С уважением, команда Speaker</h4>
</body>
</html>
    `, // plain text body
  });
  console.log("Message sent: %s", info.messageId);
}