const express = require('express');
const router = express.Router();
const dbQuery = require('../db/dbQuery');
const bodyParser = require('body-parser');
// const stringGenerator = require('crypto-random-string');
// const nodemailer = require("nodemailer");

router.use(bodyParser.json());

router
.all('/', (req, res, next)=>{
  next();
})
.get('/', (req, res, next)=>{
  res.render('res', { statuscode: '400', message: 'Bad request' });
})
.post('/add', async (req, res, next)=>{
  if(!req.body.id || !req.body.token || !req.body.speak){
    res.statusCode = 400;
    res.end(JSON.stringify({status: "Wrong arguments"}));
    // next();
  }
  else {
    await dbQuery.checkToken(req.body.token, req.body.id)
    .then(async result=>{
      if(!result){
        res.statusCode = 400;
        res.end(JSON.stringify({status: "Wrong token"}));
      }
      else {
        let speak_id = await dbQuery.setSpeak(req.body.id, req.body.speak).catch(err => {throw err});
        if(req.body.speak.attachments != undefined){
          console.log(req.body.speak.attachments);
          for await(attachment of req.body.speak.attachments) await dbQuery.updateAttachment(speak_id, 'speak', attachment).catch(err =>{throw err});
        }
        res.statusCode = 200;
        res.end(JSON.stringify({status:"Success"}));
      }
    })
    .catch(err =>{ throw err });
  }
})
.post('/get', async (req,res,next)=>{
  if(!req.body.id || !req.body.userid){
    res.statusCode = 400;
    res.end(JSON.stringify({status: "Wrong arguments"}));
  }
  else {
    let speaks = [], respeaks = [];
    let liked = await dbQuery.getLikedPosts(req.body.id, req.body.userid).catch(err =>{throw err});
    //console.log(liked);
    let media = [];
    let result = await dbQuery.getSpeaks(req.body.id, req.body.userid)
    .catch(err =>{
      res.statusCode = 500
      res.end(JSON.stringify({status: "SQL Error", err: err.message}));
    });
    console.log("\n\n\n\nSpeaks" , result);
    for await(resultOne of result){
      if(resultOne.respeaked == null && resultOne.respeak == undefined)
        speaks.push(resultOne);
      if(resultOne.attachments.length != 0)
        media.push(resultOne);
      respeaks.push(resultOne);
    }
    res.statusCode = 200;
    res.end(JSON.stringify({status:"Success", speaks: speaks, respeaks: respeaks, liked: liked, media: media})); //media: media
  }
})
.post('/getOne', async (req,res,next)=>{
  if(!req.body.id || !req.body.speakId){
    res.statusCode = 400;
    res.end(JSON.stringify({status: "Wrong arguments"}));
  }
  else {
    dbQuery.getSpeak(req.body.speakId, req.body.id)
    .then(result =>{
      res.statusCode = 200;
      res.end(JSON.stringify({status:"Success", speaks: result}));
    })
    .catch(err =>{throw err})
  }
})
.post('/comments/add', (req, res, next)=>{
  if(!req.body.userId || !req.body.speakId || !req.body.commentText){
    res.statusCode = 400;
    res.end(JSON.stringify({status: "Wrong arguments"}));  
  }
  else{
    dbQuery.setComment(req.body.userId, req.body.speakId, req.body.commentText)
    .then(async comment_id =>{
      if(req.body.attachments){
        for await(attachment of req.body.attachments) await dbQuery.updateAttachment(comment_id, 'comment', attachment).catch(err =>{throw err});
      }
      res.statusCode = 200;
      res.end(JSON.stringify({status:"Success", comment_id: comment_id}));
    })
    .catch(err =>{
      res.statusCode = 418
      res.end(JSON.stringify({status: "SQL Error", err: err.message}));
    });
  }
})
// .post('/respeaks/add', (req, res, next)=>{
//   if(!req.body.userId || !req.body.speakId || !req.body.commentText){
//     res.statusCode = 400;
//     res.end(JSON.stringify({status: "Wrong arguments"}));  
//   }
//   else{
//   }
// })

module.exports = router;