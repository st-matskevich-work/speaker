const express = require('express');
const router = express.Router();
const bodyparser = require('body-parser');
const stringGenerator = require('crypto-random-string');
const fetch = require('node-fetch');

const sendpulse = require('sendpulse-api');
const API_USER_ID = "9f2646648d7bf444a85ad5675bb32cae";
const API_SECRET = "8313752e99494cd4609caa1aaa2d9af1";
const TOKEN_STORAGE = "/tmp/";

// const pool = require('../db/db_config');
const nodemailer = require("nodemailer");
// const sha256 = require('sha256');
// const credentials = require('../bin/config');
const dbQuery = require('../db/dbQuery');
//const moment = require('moment');

router.use(bodyparser.json());

router
.all('/', (req, res, next)=>{
  next();
})
.get('/', (req, res, next)=>{
  res.render('res', { statuscode: '400', message: 'Bad request' });
})
.post('/', async (req, res, next)=>{
  if(!req.body.name || !req.body.password || (req.body.email == undefined && req.body.phone == undefined)){
    res.statusCode = 400;
    res.end(JSON.stringify({ status: "Неверные данные" }));
  }
  else {
    console.log(req.body);
    if(req.body.email){
      await dbQuery.setUser(req.body.name, req.body.email, req.body.password)
      .then(async ()=>{
        let id = await dbQuery.getId(req.body.email).catch(err=>{throw err});
        let activationCode = stringGenerator({length: 7, type: 'numeric'});
        sendRegistrationMail(req.body.email, activationCode).catch(err => { throw err });
        await dbQuery.setActivator(id, activationCode).catch(err=>{ throw err });
        await dbQuery.setAccount(id).catch(err =>{ throw err });
        res.statusCode = 200;
        res.end(JSON.stringify({ status: 'Success', id: id }));
      })
      .catch(err=>{ 
        if(err.code == 23505){
          res.statusCode = 403;
          res.end(JSON.stringify({status:"Email уже используется"}))
        } 
      });
    }
    else if(req.body.phone){
      //console.log(111);
      await dbQuery.setUser(req.body.name, req.body.phone, req.body.password)
      .then(async ()=>{
        //console.log(111);
        
        let id = await dbQuery.getId(req.body.phone).catch(err=>{throw err});
        console.log("id", id);
        let activationCode = stringGenerator({length: 7, type: 'numeric'});
        console.log("activationCode", activationCode);
        //sendRegistrationMail(req.body.email, activationCode).catch(err => { throw err });
        sendSMS(req.body.phone, activationCode);
        await dbQuery.setActivator(id, activationCode).catch(err=>{ throw err });
        await dbQuery.setAccount(id).catch(err =>{ throw err });
        res.statusCode = 200;
        res.end(JSON.stringify({ status: 'Success', id: id }));
      })
      .catch(err=>{ 
        if(err.code == 23505){
          res.statusCode = 402;
          res.end(JSON.stringify({status:"Этот номер телефона уже используется"}))
        } 
      });
    }

  }
})
.post('/checkAvailability', async (req, res, next)=>{
  if(req.body.email != undefined && req.body.email != ""){
    if(! await dbQuery.checkUser(req.body.email).catch(err =>{ throw err })){
      res.statusCode = 200;
      res.end(JSON.stringify({ status: "User available" }));
    }
    else {
      res.statusCode = 403;
      res.end(JSON.stringify({ status: "Email занят" }));
    }
  }
  else if(req.body.tag != undefined && req.body.tag != ""){
    if(! await dbQuery.getUserIdByTag(req.body.tag).catch(err =>{ throw err })){
      res.statusCode = 200;
      res.end(JSON.stringify({ status: "User available" }));
    }
    else {
      res.statusCode = 401;
      res.end(JSON.stringify({ status: "Tag занят" }));
    }
  }
  else {
    res.statusCode = 400;
    res.end(JSON.stringify({ status: "Неверные данные" }));
  }
})
.post('/verify', async (req,res,next)=>{
  if(req.body.email == undefined && req.body.phone == undefined){
    next();
  }
  else {
    console.log(req.body);
    if(req.body.email != undefined){
      console.log(1);
      await dbQuery.checkUser(req.body.email)
      .then(async result =>{
        if(!result){
          res.statusCode = 405;
          res.end(JSON.stringify({ status:"Пользователь не найден" }));
        }
        else {
          let id = await dbQuery.getId(req.body.email).catch(err =>{ throw err });
          let activationCode = await dbQuery.setActivator(id);
          sendRegistrationMail(req.body.email, activationCode);
          //let _token = await dbQuery.setToken(id);
          res.statusCode = 200;
          res.end(JSON.stringify({status: "Success"}));
        }
      })
      .catch(err =>{ throw err });
    }
    else if(req.body.phone != undefined){
      console.log(2);
      await dbQuery.checkUser(req.body.phone)
      .then(async result =>{
        if(!result){
          res.statusCode = 405;
          res.end(JSON.stringify({ status:"Пользователь не найден" }));
        }
        else {
          let id = await dbQuery.getId(req.body.phone).catch(err=>{ throw err });
          let activationCode = await dbQuery.setActivator(id);
          sendSMS(req.body.phone, activationCode);
          //let _token = await dbQuery.setToken(id);
          res.statusCode = 200;
          res.end(JSON.stringify({status: "Success"}));
        }
      })
      .catch(err =>{ throw err });
    }
  }
})
.post('/checkActivation', (req, res, next)=>{
  if(!req.body.id || !req.body.activationCode){
    res.statusCode = 400;
    res.end(JSON.stringify({ status: "Invalid Data" }));
  }
  else {
    dbQuery.checkActivator(req.body.id, req.body.activationCode)
    .then(async result=>{
      if(!result){
        res.statusCode = 406;
        res.end(JSON.stringify({ status:"Неверный код активации" }));
      }
      else{
        await dbQuery.updateUserStatus(req.body.id, true).catch(err=>{ throw err });
        let _token = await dbQuery.setToken(req.body.id).catch(err=>{ throw err });
        res.statusCode = 200;
        res.end(JSON.stringify({ status:"Success", token: _token}));
      }
    })
    .catch(err=>{ throw err });
  }
})

module.exports = router;


async function sendSMS(phone, activationCode){
  await fetch('https://events.sendpulse.com/events/id/9b9c10df2d8145c2a3bf0b8d649b17ee/7347718', { 
    method: 'POST',
    body: JSON.stringify({
      email: 'email',
      phone: phone,
      code: activationCode
    }),
    headers: { 'Content-Type': 'application/json' } 
  })
  .then(res => res.json())
  .then(json => console.log(json));
  //.catch(err => { throw err });
  // sendpulse.init(API_USER_ID, API_SECRET, TOKEN_STORAGE, function(token) {
  //   sendpulse.smsSend((data) => console.log(data), "speaker", [phone], `Спасибо за регистрацию на нашем сервисе. Это ваш код активации ${activationCode}`);
  // });
}


async function sendRegistrationMail(email, activeCode){  
  var transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      type: 'OAuth2',
      user: 'speakertest60@gmail.com',
      clientId: '1073326472229-pq4fpb6sblqls91ttlrb3lq43gar3ohg.apps.googleusercontent.com',
      clientSecret: '6BHE_CCEu1ji3OKy5_vl1oKd',
      refreshToken: '1//04lJHo2c4yqopCgYIARAAGAQSNwF-L9Ir7fyMTbBFQgh-mTiJ9C165XbZyGC5L18eEqccHKYerqwARGG-gXJqJ23cLpSfeCfa4wI',
    }
  });
  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"Speaker Company" <speakertest60@gmail.com>', // sender address
    to: email.toString(), // list of receivers
    subject: "Hello! Thank you for registration on our platform.", // Subject line
    html: `
    <html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=0.7">
    <title>Спикер - Подтверждение регистрации</title>
    <style type="text/css">
        body {
            margin: 0;
        }
        #header {
            height: 100px;
            width: 100%;
            display: flex;
            background: #0AB4BE;
        }
        #icon {
            width: 100px;
            height: 100px;
            background: url('https://speaker-app.herokuapp.com/images/icon.png');
            background-size: 100px 100px;
        }
        #label {
            flex: 1;
            background: url('https://speaker-app.herokuapp.com/images/label.png');
            background-size: contain;
            background-repeat: no-repeat;
            margin-top: 20px;
            margin-bottom: 10px;
        }
        #confirm_title {
            font-family: 'Roboto', sans-serif;
            margin-top: 10px;
            margin-bottom: 10px;
            font-weight: 500;
            font-size: 45px;
            font-style: normal;
            color: rgba(0, 0, 0, 0.87);
            margin-left: 5px;
            margin-right: 5px;
        }
        #confirm_text {
            font-family: 'Roboto', sans-serif;
            font-weight: 100;
            font-size: 20px;
            font-style: normal;
            color: rgba(0, 0, 0, 0.87);
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 0;
            margin-bottom: 0;
        }
    </style>
</head>
<body>
    <div id="header">
        <div id="icon"></div>
        <div id="label"></div>
    </div>
    <h1 id="confirm_title">Код подтверждения регистрации</h1>
    <h4 id="confirm_text">Привет! Вот ваш код для активации аккаунта: ${activeCode}. С уважением, команда Speaker!</h4>
</body>
</html>
    `, // plain text body
  });
  console.log("Message sent: %s", info.messageId);
}


// async function getId(email){
//   let result = await pool.query('select * from users where email = $1::text', [email])
//   return result.rows[0].id;
// }



// pool.query('insert into users(name, email, password, status, setup, registrationDate) values ($1::text, $2::text, $3::text, $4, $5, $6)', 
//       [req.body.name, req.body.email, sha256(req.body.password), false, 0, moment().format('LL').toString()])
//     .then(async (response)=>{
//       let activationCode = stringGenerator({length: 7, type: 'hex'});
//       sendRegistrationMail(req.body.email, activationCode).catch(err => { throw err });
//       await pool.query('insert into activators(id, code) values ($1, $2::text)', 
//         [await getId(req.body.email), activationCode])
//       .catch(err => {
//         console.log(err)
//         res.statusCode = 400;
//         res.end(JSON.stringify({ status:'Sql error', err:err}));
//       });
//       await pool.query('insert into accounts(id, image, headerimage) values ($1, $2, $3)', 
//         [await getId(req.body.email), "/images/defaultAvatar.jpg", "/images/defaultHeader.jpg"])
//       .catch(err => { 
//         console.log(err);
//         res.statusCode = 400;
//         res.end(JSON.stringify({status: "Sql error", err: err}));
//       });
//       res.statusCode = 200;
//       res.end(JSON.stringify({ status: 'Success', id: await getId(req.body.email)}));
//     })
//     .catch(err => {
//       console.log(err); 
//       res.statusCode = 403;
//       res.end(JSON.stringify({ status: "User already exists but inactive" })); 
//     });

// pool.query('select * from activators where id = $1', [req.body.id])
// .then(async (result)=>{
//   if(result.rows[0].code === req.body.activationCode){
//     await pool.query("update users set status = $1 where id = $2", [true, req.body.id]);
//     let _token = stringGenerator({length: 64, type: 'hex'});
//     pool.query('insert into tokens(id, token, valid) values ($1, $2::text, $3)', [result.rows[0].id, _token, true])
//     .then((response)=>{
//       res.statusCode = 200;
//       res.end(JSON.stringify({ status:"Success", token: _token}));
//     })
//     .catch(err => {
//       console.log(err)
//       res.statusCode = 400;
//       res.end(JSON.stringify({ status:'Sql error', err:err }));
//     });
//   }
//   else {
//     res.statusCode = 400;
//     res.end(JSON.stringify({ status: "Wrong activation code." }));
//   }
// })
// .catch((err) => {
//   res.statusCode = 400;
//   res.end(JSON.stringify({ status:'Wrong id' }));
// });
