const express = require('express');
const router = express.Router();
const dbQuery = require('../db/dbQuery');
const bodyParser = require('body-parser');
const moment = require('moment');
// const stringGenerator = require('crypto-random-string');
// const nodemailer = require("nodemailer");

router.use(bodyParser.json());

router
.all('/', (req, res, next) => {
  next();
})
.get('/', (req, res, next) => {
  res.render('res', { statuscode: '400', message: 'Bad request' });
})
.post('/mentions',async (req, res, next) => {
  if (!req.body.userId || !req.body.token) {
    res.statusCode = 400;
    res.end(JSON.stringify({ status: "Wrong arguments" }));
  }
  else {
    if (!(await dbQuery.checkToken(req.body.token, req.body.userId))) {
      res.statusCode = 400;
      res.end(JSON.stringify({ status: "Wrong token" }));
    }
    else {
      let responseComments = await dbQuery.getMentionsComment(req.body.userId).catch(err => { throw err });
      let responseSpeaks = await dbQuery.getMentionsSpeak(req.body.userId).catch(err => { throw err });
      let response = [...responseComments, ...responseSpeaks];
      response.sort((a, b)=> b.date - a.date);
      res.statusCode = 200;
      res.end(JSON.stringify({ status: "Success", mentions: response }));
    }
  }
})
.post('/all', async(req, res, next)=>{
  if (!req.body.userId || !req.body.token) {
    res.statusCode = 400;
    res.end(JSON.stringify({ status: "Wrong arguments" }));
  }
  else{
    if (!(await dbQuery.checkToken(req.body.token, req.body.userId))) {
      res.statusCode = 400;
      res.end(JSON.stringify({ status: "Wrong token" }));
    }
    else {
      let response = await dbQuery.getNotifications(req.body.userId).catch(err =>{ throw err });
      res.statusCode = 200;
      res.end(JSON.stringify({ status: "Success", notifications: response }));
    }
  }
})

module.exports = router;