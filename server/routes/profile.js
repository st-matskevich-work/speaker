const express = require('express');
const router = express.Router();
const dbQuery = require('../db/dbQuery');
const bodyparser = require('body-parser');

router.use(bodyparser.json());

router
.all('/', (req, res, next)=>{
  next();
})
.get('/', (req, res, next)=>{
  res.render('res', { statuscode: '400', message: 'Bad request' });
})
.post('/changeAvatar', async (req, res, next)=>{ 
  if(req.body.id == undefined || req.body.token == undefined || req.body.avatar == undefined || req.body.extension == undefined){
    res.statusCode = 400;
    res.end(JSON.stringify({ status: "Wrong arguments"}));
  }
  else {
    if(! await dbQuery.checkToken(req.body.token, req.body.id).catch(err => { throw err })){
      res.statusCode = 400;
      res.end(JSON.stringify({ status: "Wrong token"}));
    }
    else {
      await dbQuery.setAvatar(req.body.id, req.body.avatar, req.body.extension).catch(err => { throw err });
      res.statusCode = 200;
      res.end(JSON.stringify({ status: "Success"}));
    }
  }
})

module.exports = router;