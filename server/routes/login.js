const express = require('express');
const router = express.Router();
const bodyparser = require('body-parser');
const stringGenerator = require('crypto-random-string');
const pool = require('../db/db_config');
const sha256 = require('sha256');
const path = require('path');
const dbQuery = require('../db/dbQuery');

router.use(bodyparser.json());

router
.all('/', (req, res, next)=>{
  next();
})
.get('/', (req, res, next)=>{
  res.statusCode = 400;
  res.render('res', { statuscode: '400', message: 'Bad request' });
})
.post('/', async (req, res, next)=>{
  if(!req.body.credentials || !req.body.password){ 
    next();
  }
  else {
    pool.query('SELECT * FROM users WHERE credentials = $1::text', [req.body.credentials])
    .then(async (results) => {
      if(sha256(req.body.password) === results.rows[0].password){
        if(results.rows[0].status == true || results.rows[0].status == null) {
          let _token = stringGenerator({length: 64, type: 'hex'});
          pool.query('insert into tokens(id, token, valid) values ($1, $2::text, $3)', [results.rows[0].id, _token, true])
          .then((response)=>{
            res.statusCode = 200;
            res.end(JSON.stringify({ status:"Success", token: _token, id:results.rows[0].id, setup:results.rows[0].setup}));
          })
          .catch(err =>{
            console.log(err);
            res.statusCode = 440;
            res.end(JSON.stringify({ status:'Неверный пароль' }));
          });
        }
        else{
          res.statusCode = 408;
          res.end(JSON.stringify({ status:'Аккаунт не активирован', id: await dbQuery.getId(req.body.credentials)}));
        }
      }
      else{
        res.statusCode = 440;
        res.end(JSON.stringify({ status:'Неверный пароль' }));
      }
    })
    .catch((err) => {
      console.log(err)
      res.statusCode = 400;
      res.end(JSON.stringify({ status:"Неверные данные" }));
    });
  }
})
.post('/', (req, res, next)=>{
  if(!req.body.id || !req.body.token){
    res.statusCode = 400;
    res.end(JSON.stringify({ status: "Неверные данные" }));
  }
  pool.query('select * from tokens where token = $1::text', [req.body.token])
  .then(async result =>{
    if(!result.rows[0].valid || (result.rows[0].id != req.body.id)){
      res.statusCode = 400;
      res.end(JSON.stringify({ status:"Wrong id or token not valid" }));
    }
    else {
      let userInfo = await getInfo(result.rows[0].id);
      res.statusCode = 200;
      res.end(JSON.stringify({ 
        status:"Success", 
        id: result.rows[0].id, 
        setup: userInfo.setup,
        name: userInfo.name,
        email: userInfo.credentials,
        image: userInfo.image,
        tag: userInfo.tag,
        headerImage: userInfo.headerImage,
        description: userInfo.description,
        city: userInfo.city,
        date: userInfo.date,
        subscribersNumber: await dbQuery.getNumberOfSubscriptions(result.rows[0].id).catch(err =>{throw err}),
        subscribesNumber: await dbQuery.getNumberOfSubscribes(result.rows[0].id).catch(err =>{throw err}),
        listeners: await dbQuery.getListeners(result.rows[0].id).catch(err =>{throw err})
      }));
    }
  })
  .catch(err =>{
    res.statusCode = 400;
    res.end(JSON.stringify({ status:"Неверный токен" }));
  })
})
.get('/activation/:activationId', (req, res, next)=>{
  if(!req.params.activationId){
    res.statusCode = 400;
    res.render('res', { statuscode: '400', message: 'Bad request' });
  }
  else {
    pool.query('SELECT * FROM activators WHERE code = $1::text', [req.params.activationId])
    .then(async (result)=>{
      await pool.query("update users set status = $1 where id = $2", [true, result.rows[0].id]).catch(err => { throw err });
      res.statusCode = 200;
      res.sendFile(path.join(__dirname, '../public/confirmation/success.html'));
    })
    .catch((err)=>{
      console.log(err);
      res.statusCode = 400;
      res.sendFile(path.join(__dirname, '../public/confirmation/error.html'));
    });
  }
});

module.exports = router;


async function getInfo(id){
  let response = { 
    "status":"Success" ,
    "name": null,
    "email": null, 
    "image": null, 
    "tag": null, 
    "headerImage": null, 
    "description": null,
    "city": null,
    "date": null,
    "setup": null 
  };
  await pool.query('select * from accounts where id = $1', [id])
  .then(result =>{
    response.image = result.rows[0].image;
    response.tag = result.rows[0].tag;
    response.description = result.rows[0].description;
    response.headerImage = result.rows[0].headerimage;
    response.city = result.rows[0].city;
  }).catch(err =>{ 
    console.log(err); 
  });
  await pool.query('select * from users where id = $1', [id])
  .then(result =>{
    response.email = result.rows[0].credentials;
    response.name = result.rows[0].name;
    response.date = result.rows[0].registrationdate;
    response.setup = result.rows[0].setup;
  })
  .catch(err =>{
    console.log(err);
  });
  return response;
}