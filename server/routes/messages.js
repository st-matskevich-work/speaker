const express = require('express');
const router = express.Router();
const dbQuery = require('../db/dbQuery');
const bodyParser = require('body-parser');
// const stringGenerator = require('crypto-random-string');
// const nodemailer = require("nodemailer");

router.use(bodyParser.json());

router
.all('/', (req, res, next)=>{
  next();
})
.get('/', (req, res, next)=>{
  res.render('res', { statuscode: '400', message: 'Bad request' });
})
.post('/get', async(req, res, next)=>{
  if(!req.body.sender_id || !req.body.token){
    res.statusCode = 400;
    res.end(JSON.stringify({ status:"Wrong arguments" }));
  }
  else{
    if(!await dbQuery.checkToken(req.body.token, req.body.sender_id).catch(err =>{throw err})){
      res.statusCode = 400;
      res.end(JSON.stringify({ status:"Wrong token" }));
    }
    else {
      console.log(req.body.sender_id);
      dbQuery.getMessages(req.body.sender_id)
      .then(result =>{
        if(result != undefined){
          console.log(result);
          res.statusCode = 200;
          res.end(JSON.stringify({ status: "Success", messages: result}));
        }
        else{
          res.statusCode = 503;
          res.end(JSON.stringify({ status:"SQL Error" }));          
        }
      })
    }
  }
})
.post('/getOne', async(req, res, next)=>{
  if(!req.body.sender_id || !req.body.receiver_id || !req.body.token){
    res.statusCode = 400;
    res.end(JSON.stringify({ status:"Wrong arguments" }));
  }
  else{
    if(!await dbQuery.checkToken(req.body.token, req.body.sender_id).catch(err =>{throw err})){
      res.statusCode = 400;
      res.end(JSON.stringify({ status:"Wrong token" }));
    }
    else {
      await dbQuery.getMessage(req.body.sender_id, req.body.receiver_id)
      .then(result =>{
        if(result != undefined){
          res.statusCode = 200;
          
          res.end(JSON.stringify({ status: "Success", messages: result}));
        }
        else{
          res.statusCode = 418;
          res.end(JSON.stringify({ status:"SQL Error" }));          
        }
      })
    }
  }
})
// .post('/getLastReadMessage', async(req, res, next)=>{
//   if(req.body.user_id == undefined || req.body.chat_id == undefined || req.body.token == undefined){
//     res.statusCode = 400;
//     res.end(JSON.stringify({ status:"Wrong arguments" }));
//   }
//   else {
//     if(await dbQuery.checkToken(req.body.token, req.body.user_id).catch(err => { throw err })){
//       res.statusCode = 418;
//       res.end(JSON.stringify({ status: "Wrong token" }));
//     }
//     else{
//       let lastMessageId = await dbQuery.getLastReadMessage(req.body.user_id, req.body.chat_id).catch(err => { throw err });
//       res.statusCode = 200;
//       res.end(JSON.stringify({ status: "Success", message_id: lastMessageId}));
//     }
//   }
// })
.post('/setLastReadMessage', async(req, res, next)=>{ 
  if(req.body.sender_id == undefined || req.body.receiver_id == undefined || req.body.token == undefined || req.body.last_message == undefined){
    res.statusCode = 400;
    res.end(JSON.stringify({ status:"Wrong arguments" }));
  }
  else {
    if(!await dbQuery.checkToken(req.body.token, req.body.sender_id).catch(err => { throw err })){
      res.statusCode = 418;
      res.end(JSON.stringify({ status: "Wrong token" }));
    }
    else{
      let chat_id = await dbQuery.checkIfChatSituated(req.body.sender_id, req.body.receiver_id).catch(err => { throw err });
      if(chat_id == undefined || chat_id == null){
        res.statusCode = 418;
        res.end(JSON.stringify({ status: "Chat is not created" }));
      }
      else {
        console.log('Chat id');
        console.log(chat_id);
        await dbQuery.setLastReadMessage(chat_id, req.body.sender_id, req.body.last_message)
        .then(() => { 
          res.statusCode = 200;
          res.end(JSON.stringify({ status: "Success"}));
        })
        .catch(err => { 
          res.statusCode = 503;
          console.log(err);
          res.end(JSON.stringify({ status: "SQL Error", error: err.message }));
        });
      }
    }
  }
})

module.exports = router;