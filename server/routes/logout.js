const express = require('express');
const router = express.Router();
const bodyparser = require('body-parser');
// const stringGenerator = require('crypto-random-string');
const pool = require('../db/db_config');

router.use(bodyparser.json());

router
.all('/', (req, res, next)=>{
  next();
})
.get('/', (req, res, next)=>{
  res.render('res', { statuscode: '400', message: 'Bad request' });
})
.post('/', (req, res, next)=>{
  if(!req.body.id || !req.body.token){
    res.statusCode = 400;
    res.end(JSON.stringify({ status:"Wrong arguments" }));
    // next();
  }
  else {
    pool.query('select * from tokens where token = $1::text',[req.body.token])
    .then(async (result)=>{
      if(result.rows[0].id == req.body.id){
        await pool.query('update tokens set valid = $1 where token = $2', [false, req.body.token]).catch((err)=> { throw err });
        res.statusCode = 200;
        res.end(JSON.stringify({status:'Success'}));
      }
      else{
        res.statusCode = 400;
        res.end(JSON.stringify({status:'Wrong id'}));
      }
    })
    .catch(err => {
      console.log(err);
      res.statusCode = 400;
      res.end(JSON.stringify({status:'Wrong token'}));
    });
  }
})

module.exports = router;