const express = require('express');
const router = express.Router();
const dbQuery = require('../db/dbQuery');
const bodyparser = require('body-parser');
// const stringGenerator = require('crypto-random-string');
// const nodemailer = require("nodemailer");
router.use(bodyparser.json());

router
.all('/', (req, res, next)=>{
  next();
})
.get('/', (req, res, next)=>{
  res.render('res', { statuscode: '400', message: 'Bad request' });
})
.post('/', (req, res, next)=>{
  if(!req.body.id){
    res.statusCode = 400;
    res.end(JSON.stringify({ status:"Wrong arguments" }))
  }
  else{
    dbQuery.getAllPosts(req.body.id)
    .then(feed =>{
      res.statusCode = 200;
      res.end(JSON.stringify({status: "Success", feed: feed}));
    })
    .catch(err=>{
      res.statusCode = 500;
      res.end(JSON.stringify({status: "SQL Error", err: err}));
    })
  }
})

module.exports = router;