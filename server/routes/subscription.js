const express = require('express');
const router = express.Router();
const dbQuery = require('../db/dbQuery');
const bodyParser = require('body-parser');

router.use(bodyParser.json());

router
.all('/', (req, res, next)=>{
  next();
})
.get('/', (req, res, next)=>{
  res.render('res', { statuscode: '400', message: 'Bad request' });
})
.post('/verify', async (req,res,next)=>{
  if(!req.body.id || !req.body.userId){
    res.statusCode = 400;
    res.end(JSON.stringify({status: "Wrong arguments"}));
  }
  else {
    if((await dbQuery.checkSubscription(req.body.userId, req.body.id).catch(err => {throw err}))){
      await dbQuery.removeSubscription(req.body.userId, req.body.id).catch(err => { 
        res.statusCode = 418
        res.end(JSON.stringify({status: "SQL Error", err: err}));
      });
      res.statusCode = 200
      res.end(JSON.stringify({status: "Success"}));
    }
    else {
      // console.log(1);
      await dbQuery.setSubscription(req.body.userId, req.body.id).catch(err => { 
        res.statusCode = 418
        res.end(JSON.stringify({status: "SQL Error", err: err}));
      });
      res.statusCode = 200
      res.end(JSON.stringify({status: "Success"}));
    }
  }
})

module.exports = router;