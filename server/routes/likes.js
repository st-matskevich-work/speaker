const express = require('express');
const router = express.Router();
const dbQuery = require('../db/dbQuery');
const bodyparser = require('body-parser');
// const stringGenerator = require('crypto-random-string');
// const nodemailer = require("nodemailer");
// const fetch = require('node-fetch');

// const sendpulse = require('sendpulse-api');
// const API_USER_ID = "9f2646648d7bf444a85ad5675bb32cae";
// const API_SECRET = "8313752e99494cd4609caa1aaa2d9af1";
// const TOKEN_STORAGE = "/tmp/";
// const https = require('https');

router.use(bodyparser.json());

router
.all('/', (req, res, next)=>{
  next();
})
.get('/', (req, res, next)=>{
  res.render('res', { statuscode: '400', message: 'Bad request' });
})
.post('/', async (req, res, next)=>{
  if(!req.body.content_id || !req.body.id || !req.body.content_type){
    res.statusCode = 400;
    res.end(JSON.stringify({status: "Wrong arguments"}));    
  }
  else {
    if((await dbQuery.checkLike(req.body.content_id, req.body.id, req.body.content_type).catch(err => {throw err}))){
      await dbQuery.removeLike(req.body.content_id, req.body.id, req.body.content_type).catch(err => { 
        res.statusCode = 418
        res.end(JSON.stringify({status: "SQL Error1", err: err}));
      });
      res.statusCode = 200
      res.end(JSON.stringify({status: "Success"}));
    }
    else {
      await dbQuery.setLike(req.body.content_id, req.body.id, req.body.content_type)
      .then(result =>{ 
        res.statusCode = 200
        res.end(JSON.stringify({status: "Success"}));
      })
      .catch(err => { 
        res.statusCode = 418
        res.end(JSON.stringify({status: "SQL Error", err: err}));
      });
    }
  }
})

module.exports = router;