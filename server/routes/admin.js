const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const pool = require('../db/db_config');;

router.use(bodyParser.json());

router
.all('/', (req, res, next)=>{
  next();
})
.get('/', (req, res, next)=>{
  res.statusCode = 400;
  res.render('res', { statuscode: '400', message: 'Bad request' });
})
.post('/dropUser', async (req,res,next)=>{
  if(!req.body.id){
    res.statusCode = 400;
    res.end(JSON.stringify({ status: "Wrong arguments" }));
  }
  else{
    await pool.query('delete from users where id = $1',[req.body.id]) 
    .catch(err =>{
      res.statusCode = 418;
      res.end(JSON.stringify({status:"Sql error", err: err}));
    });
    await pool.query('delete from accounts where id = $1', [req.body.id])
    .catch(err =>{
      res.statusCode = 418;
      res.end(JSON.stringify({status:"Sql error", err: err}));
    });
    await pool.query('delete from tokens where id = $1', [req.body.id])
    .then(result =>{
      res.statusCode = 200;
      res.end(JSON.stringify({status:"Account drop successfully"}));
    })
    .catch(err =>{
      res.statusCode = 418;
      res.end(JSON.stringify({status:"Sql error", err: err}));
    });
  }
})
.post('/getInfo', async (req, res, next)=>{
  if(!req.body.id){
    res.statusCode = 400;
    res.end(JSON.stringify({ status: "Wrong arguments"}));
  }
  else{
    let response = { 
      "status":"Success" ,
      "name": null,
      "email": null, 
      "image": null, 
      "tag": null, 
      "headerImage": null, 
      "description": null,
      "city": null,
      "date": null 
    };
    await pool.query('select * from accounts where id = $1', [req.body.id])
    .then(result =>{
      response.image = result.rows[0].image;
      response.tag = result.rows[0].tag;
      response.description = result.rows[0].description;
      response.headerImage = result.rows[0].headerimage;
      response.city = result.rows[0].city;
    }).catch(err =>{ 
      console.log(err); 
      res.statusCode = 414;
      res.end(JSON.stringify({status:"Wrong id"}))
    });
    await pool.query('select * from users where id = $1', [req.body.id])
    .then(result =>{
      response.email = result.rows[0].email;
      response.name = result.rows[0].name;
      response.date = result.rows[0].registrationdate;
    })
    .catch(err =>{
      console.log(err); 
      res.statusCode = 414;
      res.end(JSON.stringify({status:"Wrong id"}));
    });
    res.statusCode = 200;
    console.log(response)
    res.end(JSON.stringify(response));
  }
})
.post('/getId', async (req, res, next)=>{
  if(!req.body.email){
    res.statusCode = 400;
    res.end(JSON.stringify({ status:"Wrong arguments" }));
    //next();
  }
  else {
    res.statusCode = 200;
    let ID = await getId(req.body.email);
    res.end(JSON.stringify({ status:"Success", id: ID }));
  }
})

module.exports = router;