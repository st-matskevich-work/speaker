const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const uniqueFilename = require('unique-filename');
const stringGenerator = require('crypto-random-string');
const fs = require('fs');
const pool = require('../db/db_config');
const path = require('path');
const os = require('os');
const dbQuery = require('../db/dbQuery');
// const sha256 = require('sha256');

router.use(bodyParser.json());

router
.all('/', (req, res, next)=>{
  next();
})
.get('/', (req, res, next)=>{
  res.render('res', { statuscode: '400', message: 'Bad request' });
})
.post('/setup', async (req, res, next)=>{
  if(!req.body.id || !req.body.token || !req.body.image || !req.body.extension){
    console.log('here' + req.body.image);
    next();
  }
  else{
    if(req.body.image == 'NaN' && req.body.extension == 'NaN'){
      await pool.query('update users set setup = 1 where id = $1',[req.body.id])
      .then(result =>{
        res.statusCode = 200;
        res.end(JSON.stringify({status: "Success"}));
      })
      .catch(err=>{ 
        console.log(err);
        res.statusCode = 418;
        res.end(JSON.stringify({status: "Ошибка SQL", err: err}));
      });
    }
    else{
      pool.query('select * from tokens where token = $1',[req.body.token])
      .then(result =>{
        if(result.rows[0].id == req.body.id){
          pool.query('select * from accounts where id = $1', [req.body.id])
          .then(async result =>{
            if(result.rowCount == 0){
              const filePath = uniqueFilename('/images/avatars',os.tmpdir(), req.body.id + '_') + '.' + req.body.extension;
              fs.writeFile("./public" + filePath, req.body.image, {encoding: 'base64'}, async (err) => {
                if(err) throw err;
                await pool.query('insert into accounts(id, image) values ($1, $2::text)',[req.body.id, filePath]).catch(err=>{ throw err });
                await pool.query('update users set setup = 1 where id = $1',[req.body.id]).catch(err=>{ throw err });
                res.statusCode = 200;
                res.end(JSON.stringify({status:"Success"}));
              });
            }
            else{
              const filePath = uniqueFilename('/images/avatars',os.tmpdir(), req.body.id + '_') + '.' + req.body.extension;
              fs.writeFile("./public" + filePath, req.body.image, {encoding: 'base64'}, async (err) => {
                if(err) throw err;
                await pool.query('update accounts set image = $2 where id = $1',[req.body.id, filePath]).catch(err=>{ throw err });
                await pool.query('update users set setup = 1 where id = $1',[req.body.id]).catch(err=>{ throw err });
                res.statusCode = 200;
                res.end(JSON.stringify({status:"Success"}));
              });
            }
          })
          .catch(err =>{ throw err });
        }
        else{
          res.statusCode = 400;
          res.end(JSON.stringify({ status:"Wrong token" }));
        }
      })
      .catch(err =>{
        res.statusCode = 400;
        res.end(JSON.stringify({ status:"Wrong token" }));
      })
    }
  }
})
.post('/setup', (req, res, next)=>{
  if(!req.body.id || !req.body.token || !req.body.tag){
    next();
  }
  else{
    pool.query('select * from tokens where token = $1',[req.body.token])
    .then(result =>{
      if(result.rows[0].id == req.body.id){
        pool.query('select * from accounts where id = $1', [req.body.id])
        .then(async result =>{
          //console.log(result);
          if(result.rowCount == 0){
            pool.query('insert into accounts(id, tag) values ($1, $2::text)', [req.body.id, req.body.tag])
            .then(async result =>{
              await pool.query('update users set setup = 2 where id = $1',[req.body.id]).catch(err=>{ throw err });
              res.statusCode = 200;
              res.end(JSON.stringify({ status:"Success" }));
            })
            .catch(err =>{ 
              res.statusCode = 401;
              res.end(JSON.stringify({ status:"Tag is already exists" }));
            });
          }
          else{
            await pool.query('update accounts set tag = $1 where id = $2', [req.body.tag, req.body.id])
            .then(async result =>{
              await pool.query('update users set setup = 2 where id = $1',[req.body.id])
              .catch(err=>{ throw err });
              res.statusCode = 200;
              res.end(JSON.stringify({ status:"Success"}));
            })
            .catch(err =>{
              res.statusCode = 401;
              res.end(JSON.stringify({ status:"Tag is already exists" }));
            });
          }
        })
        .catch(err =>{ throw err });
      }
      else{
        res.statusCode = 400;
        res.end(JSON.stringify({ status:"Wrong token" }));
      }
    })
    .catch(err =>{
      res.statusCode = 400;
      res.end(JSON.stringify({ status:"Wrong token" }));
    })
  }
})
.post('/setup', async(req, res, next)=>{
  if(!req.body.id || !req.body.token || !req.body.headerImage || !req.body.extension){
    next();
  }
  else{
    if(req.body.headerImage == 'NaN' && req.body.extension == 'NaN'){
      await pool.query('update users set setup = 1 where id = $1',[req.body.id])
      .then(result =>{
        res.statusCode = 200;
        res.end(JSON.stringify({status: "Success"}));
      })
      .catch(err=>{ 
        console.log(err);
        res.statusCode = 418;
        res.end(JSON.stringify({status: "Ошибка SQL", err: err}));
       });
    }
    else{
      pool.query('select * from tokens where token = $1',[req.body.token])
      .then(async result =>{
        if(result.rows[0].id == req.body.id){
          pool.query('select * from accounts where id = $1', [req.body.id])
          .then(async result =>{
            if(result.rowCount == 0){
              const filePath = uniqueFilename('/images/headers',os.tmpdir(), req.body.id + '_') + '.' + req.body.extension;
              fs.writeFile("./public" + filePath, req.body.headerImage, {encoding: 'base64'}, async (err) => {
                if(err) throw err;
                await pool.query('insert into accounts(id, headerimage) values ($1, $2::text)',[req.body.id, filePath]).catch(err=>{ throw err });
                await pool.query('update users set setup = 3 where id = $1',[req.body.id]).catch(err=>{ throw err });
                res.statusCode = 200;
                res.end(JSON.stringify({status:"Success"}));
              });
            }
            else{
              const filePath =  uniqueFilename('/images/headers',os.tmpdir(), req.body.id + '_') + '.' + req.body.extension;
              fs.writeFile("./public" + filePath, req.body.headerImage, {encoding: 'base64'}, async (err) => {
                if(err) throw err;
                await pool.query('update accounts set headerimage = $2 where id = $1',[req.body.id, filePath]).catch(err=>{ throw err });
                await pool.query('update users set setup = 3 where id = $1',[req.body.id]).catch(err=>{ throw err });
                res.statusCode = 200;
                res.end(JSON.stringify({status:"Success"}));
              });
            }
          })
          .catch(err =>{throw err});
        }
        else{
          res.statusCode = 400;
          res.end(JSON.stringify({ status:"Wrong token" }));
        }
      })
      .catch(err =>{
        res.statusCode = 400;
        res.end(JSON.stringify({ status:"Wrong token" }));
      })
    }
  }
})
.post('/setup', async (req, res, next)=>{
  if(!req.body.id || !req.body.token || !req.body.description){
    next();
  }
  else{
    if(req.body.description == 'NaN'){
      await pool.query('update users set setup = 4 where id = $1',[req.body.id])
      .then(result =>{
        res.statusCode = 200;
        res.end(JSON.stringify({status: "Success"}));
      })
      .catch(err =>{ 
        console.log(err);
        res.statusCode = 500;
        res.end(JSON.stringify({status: "Ошибка SQL", err: err}));
      });
    }
    else{
      pool.query('select * from tokens where token = $1',[req.body.token])
      .then(result =>{
        if(result.rows[0].id == req.body.id){
          pool.query('select * from accounts where id = $1', [req.body.id])
          .then(async result =>{
            if(result.rowCount == 0){
              pool.query('insert into accounts(id, description) values ($1, $2::text)', [req.body.id, req.body.description])
              .then(async result =>{
                await pool.query('update users set setup = 4 where id = $1',[req.body.id]).catch(err=>{ throw err });
                res.statusCode = 200;
                res.end(JSON.stringify({ status:"Success" }));
              })
              .catch(err =>{ throw err });
            }
            else{
              await pool.query('update accounts set description = $1 where id = $2', [req.body.description, req.body.id]).catch(err =>{throw err});
              await pool.query('update users set setup = 4 where id = $1',[req.body.id]).catch(err=>{ throw err });
              res.statusCode = 200;
              res.end(JSON.stringify({ status:"Success" }));
            }
          })
          .catch(err =>{ throw err });
        }
        else{
          res.statusCode = 400;
          res.end(JSON.stringify({ status:"Wrong token" }));
        }
      })
      .catch(err =>{
        res.statusCode = 400;
        res.end(JSON.stringify({ status:"Wrong token" }));
      })
    }
  }
})
.post('/setup', async (req, res, next)=>{
  if(!req.body.id || !req.body.token || !req.body.city){ 
    res.statusCode = 400;
    res.end(JSON.stringify({ status:"Wrong arguments" }))
  }
  else{
    if(req.body.city == 'NaN'){
      await pool.query('update users set setup = 5 where id = $1',[req.body.id])
      .then(result =>{
        res.statusCode = 200;
        res.end(JSON.stringify({status: "Success"}));
      })
      .catch(err=>{ 
        console.log(err);
        res.statusCode = 418;
        res.end(JSON.stringify({status: "Ошибка SQL", err: err}));
      });
    }
    else {
      pool.query('select * from tokens where token = $1',[req.body.token])
      .then(result =>{
        if(result.rows[0].id == req.body.id){
          pool.query('select * from accounts where id = $1', [req.body.id])
          .then(async account_result =>{
            if(result.rowCount == 0){
              pool.query('insert into accounts(id, city) values ($1, $2::text)', [req.body.id, req.body.city])
              .then(async result =>{
                if(account_result.rows[0].city == null){await pool.query('update users set setup = 5 where id = $1',[req.body.id]).catch(err=>{ throw err });}
                pool.query('select * from accounts where id = $1', [req.body.id])
                .then(async result_from_server =>{
                  let userResult = await getInfo(req.body.id);
                  res.statusCode = 200;
                  res.end(JSON.stringify({ 
                    status:"Success" , 
                    image: result_from_server.rows[0].image, 
                    tag: result_from_server.rows[0].tag, 
                    headerImage: result_from_server.rows[0].headerimage, 
                    description: result_from_server.rows[0].description,
                    city: result_from_server.rows[0].city,
                    date:userResult.date,
                    setup: userResult.setup
                  }));
                }).catch(err=>{throw err});
              })
              .catch(err =>{ throw err });
            }
            else{
              await pool.query('update accounts set city = $1 where id = $2', [req.body.city, req.body.id]).catch(err =>{ throw err });
              if(account_result.rows[0].city == null){await pool.query('update users set setup = 5 where id = $1',[req.body.id]).catch(err=>{ throw err });}
              pool.query('select * from accounts where id = $1', [req.body.id])
              .then(async result_from_server =>{
                res.statusCode = 200;
                let userResult = await getInfo(req.body.id);
                res.end(JSON.stringify({
                  status:"Success",
                  image: result_from_server.rows[0].image, 
                  tag: result_from_server.rows[0].tag, 
                  headerImage: result_from_server.rows[0].headerimage, 
                  description: result_from_server.rows[0].description,
                  city: result_from_server.rows[0].city, 
                  date: userResult.date,
                  setup: userResult.setup
                }));
              }).catch(err=>{throw err});
            }
          })
          .catch(err =>{ throw err });
        }
        else{
          res.statusCode = 400;
          res.end(JSON.stringify({ status:"Wrong token" }));
        }
      })
      .catch(err =>{
        res.statusCode = 400;
        res.end(JSON.stringify({ status:"Wrong token" }));
      })
    }
  }
})
.post('/getInfo', async (req, res, next)=>{
  if(!req.body.id){
    res.statusCode = 400;
    res.end(JSON.stringify({ status: "Wrong arguments"}));
  }
  else{
    let response = await dbQuery.getInfoById(req.body.id)
    .catch(err=>{
      res.statusCode = 418;
      res.end(JSON.stringify({status: "SQL Error", err: err}));
    });
    res.statusCode = 200;
    console.log(response)
    res.end(JSON.stringify(response));
  }
})
.post('/getInfoByTag', async (req, res, next)=>{
  if(!req.body.tag || !req.body.id){
    res.statusCode = 400;
    res.end(JSON.stringify({ status: "Wrong arguments"}));
  }
  else{
    dbQuery.getInfoByIdAndTag(req.body.id, req.body.tag)
    .then(response =>{
      res.statusCode = 200;
      res.end(JSON.stringify(response));
    })
    .catch(err =>{
      res.statusCode = 418;
      console.log(err);
      res.end(JSON.stringify({status: "SQL Error", err: err}));
    });
  }
})
.post('/getId', async (req, res, next)=>{
  if(!req.body.email){
    res.statusCode = 400;
    res.end(JSON.stringify({ status:"Wrong arguments" }));
    //next();
  }
  else {
    res.statusCode = 200;
    let ID = await getId(req.body.email);
    res.end(JSON.stringify({ status:"Success", id: ID }));
  }
})
.post('/search', async (req,res,next)=>{
  if(!req.body.query){
    res.statusCode = 400;
    res.end(JSON.stringify({ status:"Wrong arguments" }));
  }
  else{
    let response = [];
    let byNames = await dbQuery.searchUserByName(req.body.query).catch(err => { throw err });
    let byTags = await dbQuery.searchUserByTag(req.body.query).catch(err => { throw err });
    res.statusCode = 200;
    response = [...byNames, ...byTags];

    response.map((value, index) => response.map((postValue, postIndex) => {
      //console.log(response)
      return JSON.stringify(value) === JSON.stringify(postValue) && postIndex != index ? response.splice(postIndex, 1) : postValue})
    )
    res.end(JSON.stringify({status: "Success", users: response}));
  }
})

module.exports = router;







// let response = { 
//   "status":"Success" ,
//   "name": null,
//   "email": null, 
//   "image": null, 
//   "tag": null, 
//   "headerImage": null, 
//   "description": null,
//   "city": null,
//   "date":null,
//   "setup":null,
//   "id":null 
// };
// let id;
// await pool.query('select * from accounts where tag = $1', [req.body.tag])
// .then(result =>{
//   response.image = result.rows[0].image;
//   response.tag = result.rows[0].tag;
//   response.description = result.rows[0].description;
//   response.headerImage = result.rows[0].headerimage;
//   response.city = result.rows[0].city;
//   id = result.rows[0].id;
//   response.id = id;
// }).catch(err =>{ 
//   console.log(err); 
//   res.statusCode = 400;
//   res.end(JSON.stringify({status:"Wrong id"}))
// });
// await pool.query('select * from users where id = $1', [id])
// .then(result =>{
//   response.email = result.rows[0].email;
//   response.name = result.rows[0].name;
//   response.date = result.rows[0].registrationdate;
//   response.setup = result.rows[0].setup;
// })
// .catch(err =>{
//   console.log(err); 
//   res.statusCode = 400;
//   res.end(JSON.stringify({status:"Wrong id"}));
// });
// res.statusCode = 200;
// console.log(response)
// res.end(JSON.stringify(response));







// .post('/searchByTag', async (req,res,next)=>{
//   if(!req.body.tag){
//     res.statusCode = 400;
//     res.end(JSON.stringify({ status:"Wrong arguments" }));
//   }
//   else{
//     let result = await dbQuery.searchUserByTag(req.body.tag)
//     .catch(err =>{
//       res.statusCode = 400;
//       res.end(JSON.stringify({ status: "Wrong user tag" }));
//     });
//     res.statusCode = 200;
//     res.end(JSON.stringify({status: "Success", users: result}))
//   }
// })

async function getInfo(tag){
  let response = { 
    "status":"Success" ,
    "name": null,
    "email": null, 
    "image": null, 
    "tag": null, 
    "headerImage": null, 
    "description": null,
    "city": null,
    "date": null,
    "setup": null 
  };
  let id;
  await pool.query('select * from accounts where tag = $1', [tag])
  .then(result =>{
    response.image = result.rows[0].image;
    response.tag = result.rows[0].tag;
    response.description = result.rows[0].description;
    response.headerImage = result.rows[0].headerimage;
    response.city = result.rows[0].city;
    id = result.rows[0].id;
  }).catch(err =>{ 
    console.log(err); 
  });
  await pool.query('select * from users where id = $1', [id])
  .then(result =>{
    response.email = result.rows[0].credentials;
    response.name = result.rows[0].name;
    response.date = result.rows[0].registrationdate;
    response.setup = result.rows[0].setup;
  })
  .catch(err =>{
    console.log(err);
  });
  return response;
}

async function getInfo(id){
  let response = { 
    "status":"Success" ,
    "name": null,
    "email": null, 
    "image": null, 
    "tag": null, 
    "headerImage": null, 
    "description": null,
    "city": null,
    "date": null,
    "setup": null 
  };
  await pool.query('select * from accounts where id = $1', [id])
  .then(result =>{
    response.image = result.rows[0].image;
    response.tag = result.rows[0].tag;
    response.description = result.rows[0].description;
    response.headerImage = result.rows[0].headerimage;
    response.city = result.rows[0].city;
  }).catch(err =>{ 
    console.log(err); 
  });
  await pool.query('select * from users where id = $1', [id])
  .then(result =>{
    response.email = result.rows[0].credentials;
    response.name = result.rows[0].name;
    response.date = result.rows[0].registrationdate;
    response.setup = result.rows[0].setup;
  })
  .catch(err =>{
    console.log(err);
  });
  return response;
}

async function getId(email){
  let result = await pool.query('select * from users where credentials = $1::text', [email])
  return result.rows[0].id;
}
