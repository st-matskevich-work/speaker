const express = require('express');
const router = express.Router();
const dbQuery = require('../db/dbQuery');
const bodyparser = require('body-parser');

router.use(bodyparser.json());

router
.all('/', (req, res, next)=>{
  next();
})
.get('/', (req, res, next)=>{
  res.render('res', { statuscode: '400', message: 'Bad request' });
})
.post('/', async(req, res, next)=>{
  if(!req.body.data || !req.body.id || !req.body.token){
    res.statusCode = 400;
    res.end(JSON.stringify({ status:"Wrong arguments" }))
  }
  else {
    await dbQuery.checkToken(req.body.token, req.body.id)
    .then(async bool =>{
      if(!bool){
        res.statusCode = 400;
        res.end(JSON.stringify({ status:"Wrong token" }));        
      }
      else {
        let response = [];
        for await(dataOne of req.body.data) {
          if(!dataOne.data || !dataOne.type){
            res.statusCode = 400;
            res.end(JSON.stringify({status: "Wrong arguments"}));
          }
          response.push(await dbQuery.setAttachment(dataOne.data, dataOne.type, req.body.id).catch(err =>{throw err}));
        }
        res.statusCode = 200;
        res.end(JSON.stringify({status: "Success", id: response }));  
      }
    })
    .catch(err => {throw err})
  }
})

module.exports = router;