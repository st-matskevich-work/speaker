var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var dotenv = require('dotenv').config();

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var loginRouter = require('./routes/login');
var registerRouter = require('./routes/register');
var logoutRouter = require('./routes/logout');
var adminRouter = require('./routes/admin');
var recoveryRouter = require('./routes/recovery');
var speakRouter  = require('./routes/speaks');
var subscriptionRouter = require('./routes/subscription');
var feedRouter = require('./routes/feed');
var attachmentsRouter = require('./routes/attachments');
var messagesRouter = require('./routes/messages');
var notificationsRouter = require('./routes/notifications');
var likesRouter = require('./routes/likes');
var profileRouter = require('./routes/profile');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json({limit:'100mb', type:'json'})); 
app.use(express.urlencoded({limit: '100mb', extended: true, parameterLimit: 100000 }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/login/activation/:activationId', express.static(path.join(__dirname, '/public/confirmation')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/login', loginRouter);
app.use('/register', registerRouter);
app.use('/logout', logoutRouter);
app.use('/admin', adminRouter);
app.use('/recovery', recoveryRouter);
app.use('/speak', speakRouter);
app.use('/subscription', subscriptionRouter);
app.use('/feed', feedRouter);
app.use('/attachments', attachmentsRouter);
app.use('/messages', messagesRouter);
app.use('/notifications', notificationsRouter);
app.use('/likes', likesRouter);
app.use('/profile', profileRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
