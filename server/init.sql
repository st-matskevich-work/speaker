CREATE TABLE users (id SERIAL PRIMARY KEY,  name VARCHAR(60), email VARCHAR(100) UNIQUE, password VARCHAR(65), status BOOLEAN, setup int, registrationDate DATE);

CREATE TABLE tokens (id int, token VARCHAR(64) UNIQUE, valid BOOLEAN);

CREATE TABLE activators (id int, code VARCHAR(32) UNIQUE);

CREATE TABLE accounts (id int UNIQUE, image varchar(100), tag varchar(32) UNIQUE, headerimage varchar(100), description varchar(255), city varchar(30));

CREATE TABLE speaks (id SERIAL PRIMARY KEY, userid int, text TEXT, respeak_link BOOLEAN); 

CREATE TABLE subscriptions (id SERIAL PRIMARY KEY, userid int, subscriber_id int);

CREATE TABLE likes (id SERIAL PRIMARY KEY, speakid int, liker_id int);

CREATE TABLE comments (comment_id SERIAL PRIMARY KEY, user_id int, speak_id int, comment_text TEXT);

CREATE TABLE attachments(id SERIAL PRIMARY KEY, attach_id int, data_type TEXT, attach_to TEXT, path TEXT);

CREATE TABLE messages(id SERIAL PRIMARY KEY, sender_id int, message TEXT);

-- DELETE FROM users;
-- DELETE FROM tokens;
-- DELETE FROM activators;
-- DELETE FROM accounts;
-- DELETE FROM speaks;
-- DELETE FROM respeaks;
-- DELETE FROM subscriptions;
-- DELETE FROM likes;
-- DELETE FROM comments;
-- DELETE FROM attachments;
-- DELETE FROM messages;
-- DELETE FROM mentions;
-- DELETE FROM chats;
-- DELETE FROM chats_state;
-- DELETE FROM admins;