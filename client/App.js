import React from 'react';
import { StyleSheet, Text, View, StatusBar, Image, TouchableOpacity, KeyboardAvoidingView, KeyboardAvoidingViewBase } from 'react-native';
import { createStore, combineReducers } from 'redux'
import { Provider, connect } from 'react-redux'
import LoginReducer from './src/Reducers/LoginReducer'
import PasswordReducer from './src/Reducers/PasswordReducer'
import IDReducer from './src/Reducers/IDReducer'
import UserReducer from './src/Reducers/UserReducer'
import TokenReducer from './src/Reducers/TokenReducer'
import SpeaksReducer from './src/Reducers/SpeaksReducer'
import FeedReducer from './src/Reducers/FeedReducer'
import SocketReducer from './src/Reducers/SocketReducer'
import MessagesReducer from './src/Reducers/MessagesReducer'
import NotificationsReducer from './src/Reducers/NotificationsReducer'
import { NavigationActions } from 'react-navigation';
import AppNavigator from './src/Navigators/AppNavigator'
import * as Actions from './src/Actions/Actions'
import * as AuthAPI from './src/API/Auth'
import * as SpeaksAPI from './src/API/Speaks'
import {DotIndicator} from 'react-native-indicators';
import {SafeAreaProvider} from 'react-native-safe-area-context'
import FlashMessage from "react-native-flash-message";
import AudioRecord from 'react-native-audio-record';
import {check, PERMISSIONS, RESULTS, request} from 'react-native-permissions';
import {MessageBar as MessageBarAlert, MessageBarManager} from 'react-native-message-bar'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import io from 'socket.io-client';
import * as Constants from './src/Constants/Constants'
import PushNotification from 'react-native-push-notification'
import { setLocale, strings } from './translation/i18n'
import I18n from 'react-native-i18n';

let store = createStore(combineReducers({email: LoginReducer, 
                                          password: PasswordReducer, 
                                          token: TokenReducer, 
                                          id: IDReducer, 
                                          user: UserReducer, 
                                          userSpeaks: SpeaksReducer, 
                                          feed: FeedReducer, 
                                          socket: SocketReducer,
                                          messages: MessagesReducer,
                                          notifications: NotificationsReducer}))

export default class App extends React.Component {
  state = {
    showApp: false,
    connecting: false,
    updateRefs: [],
    currentScreen: "",
    popupMessages: [],
    popupNotifications: [],
    previousScreen: ""
  }

  async componentDidMount(){
    //console.disableYellowBox = true
    //ADD IOS MICRO PERMISSION
    check(PERMISSIONS.ANDROID.RECORD_AUDIO)
    .then(result => {
      switch(result){
        case RESULTS.UNAVAILABLE:
        console.log(
          'This feature is not available (on this device / in this context)',
        );
        break;
      case RESULTS.DENIED:
      {
        request(PERMISSIONS.ANDROID.RECORD_AUDIO)
        .then(result => {
          if(result == 'granted'){
            const options = {
              sampleRate: 16000,  // default 44100
              channels: 1,        // 1 or 2, default 1
              bitsPerSample: 16,  // 8 or 16, default 16
              audioSource: 6,     // android only (see below)
              wavFile: 'test.wav' // default 'audio.wav'
            };
            
            AudioRecord.init(options);

            console.log('GRANTED')
          }
        })
        break;
      }
      case RESULTS.GRANTED:
      {
        const options = {
          sampleRate: 16000,  // default 44100
          channels: 1,        // 1 or 2, default 1
          bitsPerSample: 16,  // 8 or 16, default 16
          audioSource: 6,     // android only (see below)
          wavFile: 'test.wav', // default 'audio.wav'
        };
        
        AudioRecord.init(options);
        
        console.log('GRANTED')
      }
      case RESULTS.BLOCKED:
        console.log('The permission is denied and not requestable anymore');
        break;
      }
    })

    PushNotification.configure({
      popInitialNotification: false,
      onNotification: notification => {
        console.log(notification, '\n\n\nNOTIFICATION')
        if(notification.type == "message"){
          this.navigator.dispatch(NavigationActions.navigate({
            routeName: 'ChatScreen',
            params: {
              user: notification.user,
              disableStatusUpdate: null
            }
          }))
        }else{
          this.navigator.dispatch(NavigationActions.navigate({
            routeName: 'Notifications',
            params: {}
          }))
        }
        /*this.navigator.dispatch(NavigationActions.navigate({
          routeName: 'Notifications',
          params: {}
        }))*/
        //notification.finish(PushNotificationIOS.FetchResult.NoData);
      }
    })
    this.tryAuth()
  }

  tryAuth = async () => {
    this.setState({connecting: true})
    let token = await Actions.getToken()
    let id = await Actions.getID()
    let locale = await Actions.getLocale()
    console.log('LOCALE', locale)
    if(locale) {
      setLocale(locale);
      this.forceUpdate()
    }

    console.log(id, token)
    AuthAPI.loginByToken(token, id)
    .then(result => {
      if(result.statusCode === 200){
        store.dispatch(Actions.setUser(result))
        store.dispatch(Actions.setToken(token.toString()))
        store.dispatch(Actions.setID(id.toString()))

        this.socketInit(id, token)

        this.setState({showApp: true})
      }else{
        Actions.writeToken("")
        Actions.writeID("")
        this.setState({showApp: true})
      }
    })
    .catch(err => {
      this.setState({connecting: false})
      console.log(err)
    })
  }

  socketInit = (id, token) => {
    SpeaksAPI.getMessages(id, token)
        .then(res => {
          if(res.statusCode === 200){
            //console.log(res.messages[65].messages)
            store.dispatch(Actions.setMessages(res.messages))
          }
        })
        .catch(res => {

        })

        const socket = io(Constants.SERVER_ADDRESS, {
            query: "id=" + id + "&token=" + token
        });

        socket.on('user_connected', params => {
          console.log(params)
        })

        socket.on('notification', param => {
          console.log(param, '\n\n\n\n\n', this.state.currentScreen)

          if(this.currentScreen != "Notifications"){
            let text = param.data.user.name + " "

            switch(param.type){
              case 'like': {
                  text += param.data.content.content_type === "speak" ? strings('NotificationComponent.likeSpeak') : strings('NotificationComponent.likeComment')
                  break
              }
              case 'respeak':{
                  text += strings('NotificationComponent.respeak')
                  break
              }
              case 'subscription': {
                  text += strings('NotificationComponent.subscription')
                  break;
              }
              case 'comment': {
                  text += strings('NotificationComponent.comment') + '"' + param.data.comment.text + '"'
              }
          }

            let id = Math.floor(Math.random() * 10000)
            this.state.popupNotifications.push(id);
            this.forceUpdate()
            //console.log(id)

            //PushNotification.cancelLocalNotifications({id: '321'})

            PushNotification.localNotification({
              title: I18n.currentLocale() == "en" ? "Notification" : "Уведомление",
              message: text, 
              type: "notification",
              id: JSON.stringify(id),
              userInfo: { id: JSON.stringify(id) }
            })


            //setTimeout(() => PushNotification.clearLocalNotification(id), 2000)
          }
        })

        socket.on('message', (params) => {
          //console.log(params, 'paparaaaaaaaaaaaaaaam')
          /*if(params.user.admin){
            console.log('MEH ADMIN')
            store.dispatch(Actions.setMessages(params.messages, params.user.id))
            return
          }*/
          //alert(JSON.stringify(params.messages))
          if(params.user.id == id) return
          
          if(this.currentScreen != "ChatScreen"){
            PushNotification.localNotification({
              title: params.user.name,
              message: params.messages[0].text,
              id: ""+params.user.id,
              type: 'message',
              user: params.user
            })

           // PushNotification.cancelLocalNotifications({id: params.user.id})
          }

          store.dispatch(Actions.addMessage(params.messages[0], params.user))
          this.updateRefs(params)
          //console.log(this.state.updateRefs)
        })

        store.dispatch(Actions.setSocket(socket))
  }

  updateRefs = (params) => this.state.updateRefs.forEach(value => value.messageReceived(params)) 

  getCurrentRouteName = (navigationState) => {
    if (!navigationState) {
      return null;
    }
    const route = navigationState.routes[navigationState.index];
    // dive into nested navigators
    if (route.routes) {
      return this.getCurrentRouteName(route);
    }
    return route.routeName;
  }

  render(){
    StatusBar.setBarStyle('dark-content')
    return (
      this.state.showApp ? 
      <Provider store={store}>
        <SafeAreaProvider>
        <KeyboardAvoidingView/>
          <AppNavigator
            onNavigationStateChange={(prevState, currentState) => {
              this.currentScreen = this.getCurrentRouteName(currentState);
              this.previousScreen = this.getCurrentRouteName(prevState);
              //this.setState({currentScreen: currentScreen, previousScreen: previousScreen});
            }} 
            ref={navigator => this.navigator = navigator} 
            screenProps={{
              socketInit: this.socketInit,
              addUpdateRef: (ref) => {
                this.state.updateRefs.push(ref)
                this.forceUpdate()
              },
              updateRefs: this.updateRefs,
              popupNotifications: this.state.popupNotifications,
              updateLocale: (lang) => {
                setLocale(lang);
                Actions.writeLocale(lang)
                this.forceUpdate()
              }
          }}/>
          <FlashMessage position="bottom" />
          <KeyboardAvoidingView/>
        </SafeAreaProvider>
      </Provider> : 
      <View style={styles.container}>
        <Image source={require('./src/Images/Logo1.png')} style={{height: 200, resizeMode: 'contain'}}/>
        {
          this.state.connecting ? (
            <View style={{height: 100}}>  
              <DotIndicator count={3} color="#0AB4BE" size={20}/>
            </View>
          ) : (
            <View style={{height: 100}}>
              <TouchableOpacity style={{backgroundColor: '#0AB4BE',width: 200, justifyContent: 'center', borderRadius: 40, height: 50, marginTop: 25}} onPress={this.tryAuth}>
                  <Text style={{fontSize: 16, color: 'white', alignSelf: 'center'}}>{strings('Errors.retry')}</Text>
              </TouchableOpacity>
            </View>
          )
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});