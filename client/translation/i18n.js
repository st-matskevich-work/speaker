import ReactNative from 'react-native';
import I18n from 'react-native-i18n';
import moment from 'moment'
import 'moment/locale/ru'
// Import all locales
import en from './en.json';
import ru from './ru.json';
import { NativeModules, Platform } from 'react-native';

// Should the app fallback to English if user locale doesn't exists
I18n.fallbacks = true;

const locale =
  Platform.OS === 'ios'
    ? NativeModules.SettingsManager.settings.AppleLocale
    : NativeModules.I18nManager.localeIdentifier;


// Define the supported translations
I18n.translations = {
  en,
  ru
};

const currentLocale = I18n.currentLocale();

if(locale.includes('ru')){
  I18n.locale = "ru"
  moment.locale("ru")
}else{
  I18n.locale = "en"
  moment.locale("en")
}
// Is it a RTL language?
export const isRTL = false;

// Allow RTL alignment in RTL languages
ReactNative.I18nManager.allowRTL(isRTL);

// The method we'll use instead of a regular string
export function strings(name, params = {}) {
  return I18n.t(name, params);
};

export function setLocale(lang){
  if(lang == "ru"){
    I18n.locale = "ru"
    moment.locale("ru")
  }
  else if(lang == "en") {
    I18n.locale = "en"
    moment.locale("en")
  }else{

  }
}

export default I18n;