import * as Constants from '../Constants/Constants'
import AsyncStorage from '@react-native-community/async-storage';

export function setID(id){
    return {
        type: Constants.SET_ID,
        data: id
    }
}

export function setEmail(email){
    return {
        type: Constants.SET_EMAIL,
        data: email
    }
}

export function setPassword(password){
    return {
        type: Constants.SET_PASSWORD,
        data: password
    }
}

export function setToken(token){
    return {
        type: Constants.SET_TOKEN,
        data: token
    }
}

export function setUser(user){
    return {
        type: Constants.SET_USER,
        data: user
    }
}

export function addSpeak(speak){
    return {
        type: Constants.ADD_SPEAK,
        data: speak
    }
}

export function setSpeaks(speaks, userId){
    return {
        type: Constants.SET_SPEAKS,
        data: speaks,
        userId: userId
    }
}

export function setProfileLikemark(id, speakerId, userId){
    return {
        type: Constants.SET_LIKE_MARK_PROFILE,
        data: id,
        speakerId: speakerId,
        userId: userId
    }
}

export async function writeToken(token){
    try{
        AsyncStorage.setItem('@token', token)
    }catch{

    }
}

export async function writeID(id){
    try{
        AsyncStorage.setItem('@id', id)
    }catch{
           
    }
}

export async function writeLocale(locale){
    try{
        AsyncStorage.setItem('@locale', locale)
    }catch{
           
    }
}

export async function writeUserSpeaks(speaks){
    try{
        AsyncStorage.setItem('@userSpeaks', JSON.stringify(speaks))
    }catch(err){
        console.log(err)
    }
}

export async function getToken(){
    let token = ""
    try{
        token = await AsyncStorage.getItem('@token')
    }catch{}

    return token
}

export async function getLocale(){
    let locale = ""
    try{
        locale = await AsyncStorage.getItem('@locale')
    }catch{}

    return locale
}

export async function getID(){
    let id = ""
    try{
        id = await AsyncStorage.getItem('@id')
    }catch{}

    return id
}

export async function getUserSpeaks(){
    let speaks = []
    try{
        speaks = await AsyncStorage.getItem('@userSpeaks')
        speaks = JSON.parse(speaks)
    }catch{
        speaks = []
    }

    return speaks
}

export function setFeed(speaks){
    return {
        type: Constants.SET_FEED,
        data: speaks
    }
}

export function addSpeakToFeed(speak){
    return {
        type: Constants.ADD_SPEAK,
        data: speak
    }
}

export function setFeedLikemark(id){
    return {
        type: Constants.SET_LIKE_MARK_FEED,
        data: id
    }
}

export function setSocket(socket){
    return {
        type: Constants.SET_SOCKET,
        data: socket
    }
}

export function setMessages(messages, chatId){
    return {
        type: Constants.SET_MESSAGES,
        data: messages,
        chatId: chatId
    }
}

export function addMessage(message, user){
    return {
        type: Constants.ADD_MESSAGE,
        data: message,
        user: user
    }
}

export function setChat(messages, user){
    return {
        type: Constants.SET_MESSAGES_BY_ID,
        data: messages,
        user: user
    }
}

export function skipAndTrigger(messages){
    return {
        type: Constants.SKIP_AND_TRIGGER,
        data: messages
    }
}

export async function setMentions(mentions){
    return {
        type: Constants.SET_MENTIONS,
        data: mentions
    }
}

export async function setNotifications(notifications){
    return {
        type: Constants.SET_NOTIFICATIONS,
        data: notifications
    }
}

export function setReadState(chatId){
    return {
        type: Constants.SET_READ_STATE,
        data: chatId
    }
}