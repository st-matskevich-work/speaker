import * as Network from './NetworkBasics'

export async function getUserInfoById(id){
    let response = await Network.getServerResponse('/users/getInfo', JSON.stringify({
        id: id
    }))

    return response
}

export async function getUsersByQuery(query, id){
    let response = await Network.getServerResponse('/users/search', JSON.stringify({
        query: query,
        id: id
    }))

    return response
}

export async function getUserInfoByTag(id, tag){
    let response = await Network.getServerResponse('/users/getInfoByTag', JSON.stringify({
        id: id,
        tag: tag
    }))

    return response
}

export async function subscribe(id, userid){
    //console.log(id, userid)
    let response = await Network.getServerResponse('/subscription/verify', JSON.stringify({
        id: id,
        userId: userid
    }))

    return response
}

export async function getMensions(userId, token){
    //console.log(userId, token)
    let response = await Network.getServerResponse('/notifications/mentions', JSON.stringify({
        token: token,
        userId: userId
    }))

    return response
}

export async function getNotifications(userId, token){
    //console.log(userId, token)
    let response = await Network.getServerResponse('/notifications/all', JSON.stringify({
        token: token,
        userId: userId
    }))

    return response
}

export async function setLastReadMessage(sender_id, receiver_id, token, last_message){
    let response = await Network.getServerResponse('/messages/setLastReadMessage', JSON.stringify({
        sender_id: sender_id,
        receiver_id: receiver_id,
        token: token,
        last_message: last_message
    }))

    return response
}

export async function updateAvatar(id, token, base64, extension){
    let response = await Network.getServerResponse('/profile/changeAvatar', JSON.stringify({
        id: id,
        token: token,
        avatar: base64,
        extension: extension
    }))

    return response
}