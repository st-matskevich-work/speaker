import * as Network from './NetworkBasics'

async function createRequest(body){
    let response = await Network.getServerResponse('/users/setup', body)
    return response
} 

export async function setAvatar(base64, extension, token, id){
    let response = await createRequest(JSON.stringify({
        image: base64,
        extension: extension,
        token: token,
        id: id
    }))

    return response
}

export async function setHeader(base64, extension, token, id){
    let response = await createRequest(JSON.stringify({
        headerImage: base64,
        extension: extension,
        token: token,
        id: id
    }))

    return response
}

export async function setTag(tag, token, id){
    let response = await createRequest(JSON.stringify({
        tag: tag,
        token: token,
        id: id
    }))

    return response
}

export async function setDescription(description, token, id){
    let response = await createRequest(JSON.stringify({
        description: description,
        token: token,
        id: id
    }))

    return response
}

export async function setCity(city, token, id){
    let response = await createRequest(JSON.stringify({
        city: city,
        token: token,
        id: id
    }))

    return response
}

