import * as Network from './NetworkBasics'

export async function loginByEmail(email, password){
    console.log('LOGINING IN')
    let response = await Network.getServerResponse('/login', JSON.stringify({
        credentials: email,
        password: password
    }))

    return response
}

export async function register(name, email, password, isPhone = false){
    let response = await Network.getServerResponse('/register', JSON.stringify(
        isPhone ? {
            name: name,
            phone: email,
            password: password
        } : {
            name: name,
            email: email,
            password: password
        }
    ))

    return response
}

export async function loginByToken(token, id){
    let response = await Network.getServerResponse('/login', JSON.stringify({
        token: token,
        id: id
    }))

    return response   
}

export async function logout(token, id){
    let response = await Network.getServerResponse('/logout', JSON.stringify({
        token: token,
        id: id
    }))

    return response
}

export async function getIdByEmail(email){
    let response = await Network.getServerResponse('/users/getId', JSON.stringify({
        email: email
    }))

    return response
}

export async function confirmRegistration(id, code){
    let response = await Network.getServerResponse('/register/checkActivation', JSON.stringify({
        activationCode: code,
        id: id
    }))

    return response
}

export async function sendRecoveryMessage(email, isPhone){
    let response = await Network.getServerResponse('/recovery', JSON.stringify(
        isPhone ? {
            phone: email
        } : {
            email: email
        }
    ))

    return response
}

export async function confirmRestore(id, code){
    let response = await Network.getServerResponse('/recovery', JSON.stringify({
        recoveryCode: code,
        id: id
    }))

    return response
}

export async function getInfo(id){
    let response = await Network.getServerResponse('/users/getInfo', JSON.stringify({
        id: id
    }))

    return response
}

export async function changePassword(id, token, password){
    let response = await Network.getServerResponse('/recovery', JSON.stringify({
        id: id,
        token: token,
        password: password
    }))

    return response
}

export async function resendActivationCode(email, isPhone){
    let response = await Network.getServerResponse('/register/verify', JSON.stringify(isPhone ? {
        phone: email
    } : {
        email: email
    }))

    return response
}

export async function checkAvailability(body){
    let response = await Network.getServerResponse('/register/checkAvailability', JSON.stringify(body))

    return response
}