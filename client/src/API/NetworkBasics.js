import * as Constants from '../Constants/Constants'
import {strings} from '../../translation/i18n'

export function timeout(time, promise)
{
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            reject(new Error("Timeout error!"));
        }, time);
        promise.then(resolve, reject);
    });
}

export async function getServerResponse(url, body, out = 30000){
    //console.log(body, Constants.SERVER_ADDRESS + url)
    let result = {}
    let status = 0

    await timeout(out, fetch(Constants.SERVER_ADDRESS + url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: body
    })).then(res => {
        //console.log(res)
        status = res.status
        let response = {}
        
        try{
            response = res.json()
        }finally{}
        //console.log(response)
        return response
    }).then(res => {
        result = res;
        result.statusCode = status
        console.log('Errors.Status' + status)
        result.status = strings('Errors.Status' + status)
    })
    .catch(err => {
        console.log(err)
        throw strings('Errors.timeout')
        //result = err
    })
    
    return result
}