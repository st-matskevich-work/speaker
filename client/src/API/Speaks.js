import * as Network from './NetworkBasics'

export async function addSpeak(id, token, speak){
    let response = await Network.getServerResponse('/speak/add', JSON.stringify({
        id: id,
        token: token,
        speak: speak
    }))

    return response
}

export async function getSpeaksById(id, userid){
    console.log(id, userid)
    let response = await Network.getServerResponse('/speak/get', JSON.stringify({
        id: id,
        userid: userid
    }))

    return response
}

export async function sendLikeRequest(postid, userid, token, type = "speak"){
    //console.log(postid, userid)
    let response = await Network.getServerResponse('/likes', JSON.stringify({
        id: userid,
        content_id: postid,
        token: token,
        content_type: type
    }))

    return response
}

export async function getFeed(id){
    let response = await Network.getServerResponse('/feed', JSON.stringify({
        id: id
    }))

    return response
}

export async function getCurrentSpeak(id, speakId){
    let response = await Network.getServerResponse('/speak/getOne', JSON.stringify({
        id: id,
        speakId: speakId
    }))

    return response
}

export async function addComment(userId, speakId, commentText){
    let response = await Network.getServerResponse('/speak/comments/add', JSON.stringify({
        userId: userId,
        speakId: speakId,
        commentText: commentText
    }))

    return response
}

export async function addAttachments(data, id, token){
    let response = await Network.getServerResponse('/attachments', JSON.stringify({
        data: data,
        id: id,
        token: token
    }), 60000)

    return response
}


export async function getMessages(id, token){
    let response = await Network.getServerResponse('/messages/get', JSON.stringify({
        sender_id: id,
        token: token
    }))

    return response 
}

export async function getChatById(sender_id, reciever_id, token){
    let response = await Network.getServerResponse('/messages/getOne', JSON.stringify({
        sender_id: sender_id,
        receiver_id: reciever_id,
        token: token
    }))

    return response 
}