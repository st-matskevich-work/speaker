import * as Constants from '../Constants/Constants'

export default function UserReducer(state, action){
    //console.log(action)
    if(typeof state === 'undefined'){
        return {}
    }

    //console.log('USER REDUCER')

    switch(action.type){
        case Constants.SET_USER:{
            return action.data
        }
        default:{
            return state
        }
    }
}