import * as Constants from '../Constants/Constants'
import * as Actions from '../Actions/Actions'

export default function SpeaksReducer(state, action){
    //console.log(action)
    if(typeof state === 'undefined'){
            return {}
    }

    //console.log('SPEAKS REDUCER')

    switch(action.type){
        case Constants.ADD_SPEAK:{
            let newState = state
            newState.speaks.unshift(action.data)
            newState.respeaks.unshift(action.data)
            //Actions.writeUserSpeaks(newState)
            return newState
        }
        case Constants.SET_SPEAKS:{
            //console.log(action.data, 'ACTION')
            let newState = {...state}
            newState[action.userId] = {speaks: [], respeaks: [], media: [], liked: []}

            action.data?.speaks?.map((value, i) => newState[action.userId].speaks.push(value))
            action.data?.respeaks?.map((value, i) => newState[action.userId].respeaks.push(value))
            action.data?.media?.map((value, i) => newState[action.userId].media.push(value))
            action.data?.liked?.map((value, i) => newState[action.userId].liked.push(value))

            console.log(newState, 'NEW STATE', action.data, 'ACTION DATA')

            return newState 
        }
        case Constants.SET_LIKE_MARK_PROFILE:{
            console.log(action, 'ACTION');
            let newState = {...state}
            let likedPost
            let dislikedPost
            
            console.log('SET_LIKE_MARK_PROFILE', action, newState[action.userId])

            newState[action.speakerId]?.respeaks?.forEach(value => {
                console.log(value, 'REDUX\n\n\n\n')
                if(value.id == action.data){
                    //console.log(value.liked, 'LIKED RESPEAKS')
                    if(value.liked) {
                        console.log('GOTCHA')
                        value.likes -= 1
                        value.liked = false
                        dislikedPost = value
                        return value
                    }
                    else {
                        value.liked = true
                        value.likes += 1
                        likedPost = value
                        return value
                    }
                }

                return value
            })

            newState[action.userId]?.respeaks?.forEach(value => {
                if(value.user.id == action.speakerId && action.speakerId != +action.userId) {
                    if(value.id == action.data){
                        //console.log(value.liked, 'LIKED RESPEAKS')
                        if(value.liked) {
                            console.log('GOTCHA')
                            value.likes -= 1
                            value.liked = false
                            dislikedPost = value
                            return value
                        }
                        else {
                            value.liked = true
                            value.likes += 1
                            likedPost = value
                            return value
                        }
                    }
    
                    return value
                }
            })

            let speaksArray = []

            newState[action.speakerId]?.speaks?.forEach(value => {
                if(value.id == action.data){
                    if(value.liked) {
                        if(!dislikedPost){
                            console.log('BUG', likedPost)
                            speaksArray.push(likedPost)
                        }
                        else speaksArray.push(dislikedPost)
                        return
                    }
                    else {
                        value.liked = !value.liked
                        value.likes += 1
                        likedPost = value
                        speaksArray.push(value)
                        return
                    }
                }

                speaksArray.push(value)
            })

            if(!newState[action.speakerId]) newState[action.speakerId] = {speaks: [], respeaks: [], media: [], liked: []}
            newState[action.speakerId].speaks = speaksArray


            if(action.userId != action.speakerId){
                newState[action.userId]?.speaks?.forEach(value => {
                    if(value.id == action.data){
                        if(value.liked) {
                            return dislikedPost
                        }
                        else {
                            value.liked = !value.liked
                            value.likes += 1
                            likedPost = value
                            return value
                        }
                    }
    
                    //if(value.liked) newState.liked.push(value)
    
                    return value
                })
            }

            newState[action.speakerId]?.media?.forEach(value => {
                if(value.id == action.data){
                    if(value.liked) value.likes -= 1
                    else value.likes += 1

                    value.liked = !value.liked
                }

                //if(value.liked) newState.liked.push(value)

                return value
            })

            let globalIndex;

            let likedArray = [];
            
            newState[action.speakerId]?.liked?.map((value, i) => {
                if(value.id == action.data){
                    if(value.liked){
                        value.likes -= 1
                        //return;
                    }
                    else value.likes += 1

                    value.liked = !value.liked
                }

                return value             
            })

            newState[action.userId]?.liked?.map((value, i) => {
                if(value.id == action.data){
                    //console.log(value, 'LIKED')
                    return
                }

                likedArray.push(value) 
            })

            console.log(likedArray, action.speakerId == action.userId)

            if(!newState[action.userId]) newState[action.userId] = {speaks: [], respeaks: [], media: [], liked: []}
            newState[action.userId].liked = likedArray
            if(likedPost) newState[action.userId].liked.unshift(likedPost)
            /*if(globalIndex >= 0) {
                newState[action.userId].liked.splice(globalIndex, 1)
                console.log(newState[action.speakerId].liked)
            }*/
                                                

            console.log(newState, 'NEW STATE');
        //console.log(newState, "NEW STATE")

            return newState
            //return state
        }
        default:{
            return state
        }
    }
}