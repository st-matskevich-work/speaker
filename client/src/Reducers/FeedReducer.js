import * as Constants from '../Constants/Constants'
import * as Actions from '../Actions/Actions'

export default function FeedReducer(state, action){
    if(typeof state === 'undefined'){
        return []
}

    //console.log('FEED REDUCER')

switch(action.type){
    case Constants.ADD_SPEAK_TO_FEED:{
        let newState = [...state]
        newState.unshift(action.data)
        return newState
    }
    case Constants.SET_FEED:{
        //newState.push(state)
        return action.data
    }
    case Constants.SET_LIKE_MARK_FEED:{
        //console.log(state)
        let newState = [...state]

        newState.forEach(value => {
            if(value.id == action.data){
                if(value.liked) value.likes -= 1
                else value.likes += 1

                value.liked = !value.liked
            }

            return value
        })

        //console.log(newState, "NEW STATE")

        return newState
    }
    default:{
        return state
    }
    }
}