import * as Constants from '../Constants/Constants'

export default function LoginReducer(state, action){
    //console.log(action)
    if(typeof state === 'undefined'){
        return ""
    }

    //console.log('LOGIN REDUCER')

    switch(action.type){
        case Constants.SET_EMAIL:{
            return action.data
        }
        default:{
            return state
        }
    }
}