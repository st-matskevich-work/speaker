import * as Constants from '../Constants/Constants'
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';

export default function MessagesReducer(state, action){
    //console.log('MESSAGE REDUCER')
    if(typeof state === 'undefined'){
        return []
    }

    switch(action.type){
        case Constants.SET_MESSAGES:{
            let newState = action.data

            if(action.chatId){
                newState[action.chatId]?.messages.map((value, i) => {
                    value.read = true
                    return value
                })
            }

            return newState
        }
        case Constants.ADD_MESSAGE: {
            let newState = state;
            
            if(!newState["" + action.user.id]){

                newState["" + action.user.id] = {
                    user: action.user,
                    messages: [
                        action.data
                    ]
                }

                return newState
            }else{
                //console.log(GiftedChat.append(newState["" + action.user.id].messages, action.data))
                newState["" + action.user.id].messages.unshift(action.data) //= GiftedChat.append(newState["" + action.user.id].messages, action.data)
            }

            return newState
        }
        case Constants.SET_MESSAGES_BY_ID: {
            let newState = state;

           newState["" + action.user.id] = {
               user: action.user,
               messages: action.data
           }

            return newState
        }
        case Constants.SKIP_AND_TRIGGER: {
            let newState = [...state]
            return newState
        }
        case Constants.SET_READ_STATE: {
            let newState = [...state]

            newState[action.data]?.messages.map((value, i) => {
                value.read = true
                return value
            })

            console.log(newState[action.data]?.messages)

            return newState
        }
        default:{
            return state
        }
    }
}