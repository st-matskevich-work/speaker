import * as Constants from '../Constants/Constants'

export default function SocketReducer(state, action){
    //console.log(action)
    if(typeof state === 'undefined'){
        return ""
    }

    //console.log('SOCKET REDUCER')

    switch(action.type){
        case Constants.SET_SOCKET:{
            return action.data
        }
        default:{
            return state
        }
    }
}