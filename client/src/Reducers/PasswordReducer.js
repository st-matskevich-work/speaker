import * as Constants from '../Constants/Constants'

export default function PasswordReducer(state, action){
    //console.log(action)
    if(typeof state === 'undefined'){
        return ""
    }

    //console.log('PASSWORD REDUCER')

    switch(action.type){
        case Constants.SET_PASSWORD:{
            return action.data
        }
        default:{
            return state
        }
    }
}