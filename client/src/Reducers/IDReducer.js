import * as Constants from '../Constants/Constants'

export default function IDReducer(state, action){
    //console.log(action)
    if(typeof state === 'undefined'){
        return ""
    }

    //console.log('ID REDUCER')

    switch(action.type){
        case Constants.SET_ID:{
            return action.data
        }
        default:{
            return state
        }
    }
}