import * as Constants from '../Constants/Constants'

export default function TokenReducer(state, action){
    //console.log(action)
    if(typeof state === 'undefined'){
        return ""
    }

    //console.log('TOKEN REDUCER')

    switch(action.type){
        case Constants.SET_TOKEN:{
            return action.data
        }
        default:{
            return state
        }
    }
}