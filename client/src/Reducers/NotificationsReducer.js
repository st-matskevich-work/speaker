import * as Constants from '../Constants/Constants'

export default function NotificationsReducer(state, action){
    
    if(typeof state === 'undefined'){
        return {mentions: [], all: []}
    }

    //console.log('MENTIONS REDUCER')

    switch(action.type){
        case Constants.SET_MENTIONS: {
            state.mentions = action.data
            return state
        }
        case Constants.SET_NOTIFICATIONS: {
            state.all = action.data
            return state
        }
        default: {
            return state
        }
    }
}