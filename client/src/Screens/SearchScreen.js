import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import {View, Text, StyleSheet, Image, ScrollView, TouchableOpacity, TextInput} from 'react-native'
import { connect } from 'react-redux'
import * as Constants from '../Constants/Constants'
import UserTile from '../Components/UserTile'
import * as UsersAPI from '../API/Users'
import { strings } from '../../translation/i18n'

class Search extends React.Component{
    state = {
        users: [
            
        ],
        noResults: false
    }

    onTextChange = (text) => {
        if(!text.length){
            this.setState({users: [], noResults: false})
            return;
        }

        UsersAPI.getUsersByQuery(text)
        .then(res => {
            if(res.statusCode === 200){
                this.setState({users: res.users})
                if(res.users.length) this.setState({noResults: false})
                else this.setState({noResults: true})
            }else{
                this.setState({users: [], noResults: false})
            }
        })
        .catch(err => {

        })
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                <View style={{height: 60, backgroundColor: 'white', elevation: 1, flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={{uri: Constants.SERVER_ADDRESS + this.props.user.image}} style={{height: 36, width: 36, borderRadius: 25, marginLeft: 15, marginRight: 15, backgroundColor: '#f6f7f8'}}/>
                    <TextInput onChangeText={this.onTextChange} selectTextOnFocus={true} style={{flex: 1 , backgroundColor: '#EDF7F8', borderRadius: 20, marginRight: 15, height: 40, paddingLeft: 15, paddingRight: 15, fontSize: 14}} placeholder={strings('SearchScreen.placeholder')}/>
                </View>
                <ScrollView scrollEnabled={true} style={{flex: 1}}>
                {
                    this.state.noResults? (
                        <View>
                            <Text style={{alignSelf: 'center', marginTop: 10, color: '#0AB4BE', fontWeight: 'bold', fontSize: 16}}>{strings('SearchScreen.zeroResultTitle')}</Text>
                            <Text style={{alignSelf: 'center', fontWeight: 'normal', color: '#0AB4BE', marginTop: 5}}>{strings('SearchScreen.zeroResultText')}</Text>
                        </View>
                    ) : this.state.users.map((value, index) => {
                        if(this.state.users.length == 1 && value.tag == this.props.user.tag){
                            this.setState({noResults: true})
                            return;
                        }
                        if(value.tag != this.props.user.tag)
                            return <UserTile id={this.props.id} navigation={this.props.navigation} user={value} key={index}/>
                    })
                }
                </ScrollView>
                <TouchableOpacity style={{height: 60, width: 60, backgroundColor: '#0AB4BE', position: 'absolute', bottom: 10, borderRadius: 35, right: 10, justifyContent: 'center', alignItems: 'center'}}
                                    onPressOut={() => {
                                        this.props.navigation.navigate('CreateNewSpeak')
                                    }}>
                        <Image source={require('../Images/button_speak.png')} style={{height: 60, width: 60, alignSelf: 'center'}}/>
                </TouchableOpacity>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
    }
})

export default connect(state => ({login: state.login, user: state.user, id: state.id}))(Search)