import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import {View, Text, StyleSheet, TouchableOpacity, Image, Button, KeyboardAvoidingView, Keyboard, TouchableWithoutFeedbackBase} from 'react-native'
import { connect } from 'react-redux'
import CustomInput from '../../Components/CustomInput'
import {DotIndicator} from 'react-native-indicators';
import Modal, {
    ModalTitle,
    ModalContent,
    ModalFooter,
    ModalButton,
    SlideAnimation,
    ScaleAnimation,
  } from 'react-native-modals';
import * as Actions from '../../Actions/Actions'
import * as AuthAPI from '../../API/Auth'
import * as Constants from '../../Constants/Constants'
import {MessageBar as MessageBarAlert, MessageBarManager} from 'react-native-message-bar'
import { ScrollView } from 'react-native-gesture-handler'
import { showMessage, hideMessage } from "react-native-flash-message";
import { strings } from '../../../translation/i18n'

class EnterNewPassword extends React.Component{
    static navigationOptions = ({navigation}) => {
        return {
            //headerTitleAlign: 'center',
            headerTitle: () => (
                <Text style={{fontSize: 18}}>{strings('EnterNewPasswordScreen.header')}</Text>
            ),
            headerLeft: () => (
                <TouchableOpacity style={{marginLeft: 20}} onPressOut={() => navigation.navigate('ConfirmRestore')}>
                    <Image source={require('../../Images/back.png')} style={{height: '100%', width: 16, resizeMode: 'contain'}}/>
                </TouchableOpacity>
            ),
            headerStyle: {
                elevation: 0, //for android
                shadowOpacity: 0, //for ios
                borderBottomWidth: 0.3, //for i
                borderBottomColor: 'rgba(151,151,151,0.5)'
            }
        }
    };

    state = {
        showPopUp: false,
        isLoading: false,
        password: "",
        repeat: "",
        errorText: ""
    }

    componentDidMount(){
        //MessageBarManager.registerMessageBar(this.refs.alert);
    }

    componentWillUnmount(){
        //MessageBarManager.unregisterMessageBar();
    }

    confirm = async () => {
        if(this.state.password != this.state.repeat){
            /*MessageBarManager.showAlert({
                title: 'Ошибка',
                message: 'Введенные пароли не совпадают',
                alertType: 'error',
                position: 'bottom',
            });*/
            /*showMessage({
                message: 'Ошибка',
                description: 'Введенные пароли не совпадают',
                type: "danger",
              });*/
            
              this.setState({errorText: 'Введенные пароли не совпадают'})

            return
        }

        this.setState({isLoading: true})
        AuthAPI.changePassword(this.props.id, this.props.token, this.state.password)
        .then(result => {
            if(result.statusCode === 200){
                
                Actions.writeToken(this.props.token.toString())
                Actions.writeID(this.props.id.toString())
                //this.props.dispatch(Actions.setToken(this.props.token.toString()))
                //this.props.dispatch(Actions.setID(this.props.id.toString()))
                this.props.navigation.navigate('FinishRestore')
                this.setState({isLoading: false})
            }else{
                /*MessageBarManager.showAlert({
                    title: 'Ошибка',
                    message: result.status,
                    alertType: 'error',
                    position: 'bottom',
                });*/
                this.setState({isLoading: false, errorText: result.status})
            }
        })
        .catch(err => {
            /*MessageBarManager.showAlert({
                title: 'Ошибка',
                message: err,
                alertType: 'error',
                position: 'bottom',
            });*/
            this.setState({isLoading: false, errorText: err})
        })
        /*let result = await LoginActions.login(JSON.stringify({
            email: this.props.login, 
            password: this.props.password
        }))
        if(result.statusCode === 200){
            console.log(result)
            this.props.dispatch(TokenActions.setToken(result.token))
            this.props.dispatch(IDActions.setID(result.id))
            this.props.navigation.navigate('SetupNewAccount')
        }
        else{
            MessageBarManager.showAlert({
                title: 'Error',
                message: result.status,
                alertType: 'error',
                position: 'bottom',
            });
            this.setState({isLoading: false})
        }*/
        //this.props.navigation.navigate('SetupNewAccount')
        //console.log(result)
    }

    //Lonely Astronaut
    //thelonelyastronaut1337@gmail.com
    //LoginHorizon

    render(){
        //console.log(this.props.login, this.props.password)
        return(
            <SafeAreaView style={styles.container}>
                <KeyboardAvoidingView style={{flex: 1}}>
                    <ScrollView contentContainerStyle={{padding: 30}}>
                        <Text style={styles.title}>{strings('EnterNewPasswordScreen.screenTitle')}</Text>
                        <View style={{width: '100%', flexDirection: 'row', marginBottom: 10}}>
                            <Image source={{uri: Constants.SERVER_ADDRESS + this.props.user.image}} style={{height: 50, width: 50, borderRadius: 25, backgroundColor: '#f6f7f8'}}/>
                            <View style={{flex: 1, marginLeft: 10, justifyContent: 'space-around'}}>
                                <Text style={{fontWeight: 'bold', fontSize: 18}}>{this.props.user.name}</Text>
                                <Text>@{this.props.user.tag}</Text>
                            </View>
                        </View>
                        <CustomInput disableSpacing={true} placeholder={strings('EnterNewPasswordScreen.passwordPlaceholder')} type="password" onChangeText={(text) => this.setState({password: text, errorText: ""})}/>
                        <CustomInput disableSpacing={true} placeholder={strings('EnterNewPasswordScreen.repeatPassPlaceholder')} type="password" onChangeText={(text) => this.setState({repeat: text, errorText: ""})}/>
                        {
                            this.state.errorText ? <Text style={{fontSize: 16, color: 'red', marginBottom: 15}}>{this.state.errorText}</Text> : null
                        }
                    </ScrollView>
                    <View style={{flexGrow: 1}}/>
                    <View style={{flexDirection: 'row', height: 60, width: '100%', alignItems: 'center'}}>
                            <View style={{flex: 1}}/>
                            <TouchableOpacity style={{backgroundColor: '#0AB4BE',marginRight: 30,width: 100, justifyContent: 'center', borderRadius: 40, height: 40}} onPressOut={this.confirm} disabled={this.state.isLoading}>
                            {
                                this.state.isLoading ? <DotIndicator count={3} color="white" size={8}/> : <Text style={{fontSize: 15, color: 'white', alignSelf: 'center'}}>{strings('EnterNewPasswordScreen.send')}</Text>
                            }
                            </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
    },
    title: {
        fontSize: 32,
        marginBottom: 20,
        fontWeight: 'bold'
    },
})
export default connect(state => ({email: state.email, password: state.password, token: state.token, id: state.id, user: state.user}))(EnterNewPassword)