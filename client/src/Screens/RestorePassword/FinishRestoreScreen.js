import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import {View, Text, StyleSheet, TouchableOpacity, Image, Button, KeyboardAvoidingView, Keyboard, TouchableWithoutFeedbackBase} from 'react-native'
import { connect } from 'react-redux'
import CustomInput from '../../Components/CustomInput'
import {DotIndicator} from 'react-native-indicators';
import Modal, {
    ModalTitle,
    ModalContent,
    ModalFooter,
    ModalButton,
    SlideAnimation,
    ScaleAnimation,
  } from 'react-native-modals';
import * as Actions from '../../Actions/Actions'
import * as AuthAPI from '../../API/Auth'
import {MessageBar as MessageBarAlert, MessageBarManager} from 'react-native-message-bar'
import { ScrollView } from 'react-native-gesture-handler'
import { strings } from '../../../translation/i18n'

class FinishRestore extends React.Component{
    static navigationOptions = ({navigation}) => {
        return {
            //headerTitleAlign: 'center',
            headerTitle: () => (
                <Text style={{fontSize: 18}}>Изменить пароль</Text>
            ),
            headerLeft: () => (
                <TouchableOpacity style={{marginLeft: 20}} onPressOut={() => navigation.navigate('EnterNewPassword')}>
                    <Image source={require('../../Images/back.png')} style={{height: '100%', width: 16, resizeMode: 'contain'}}/>
                </TouchableOpacity>
            ),
            headerStyle: {
                elevation: 0, //for android
                shadowOpacity: 0, //for ios
                borderBottomWidth: 0.3, //for i
                borderBottomColor: 'rgba(151,151,151,0.5)'
            }
        }
    };

    state = {
        showPopUp: false,
        isLoading: false,
        password: "",
        repeat: ""
    }

    componentDidMount(){
        //MessageBarManager.registerMessageBar(this.refs.alert);
    }

    componentWillUnmount(){
        //MessageBarManager.unregisterMessageBar();
    }

    confirm = async () => {
        let user = {
            name: this.props.user.name,
            email: this.props.user.email,
            avatar: this.props.user.image,
            header: this.props.user.headerImage,
            tag: this.props.user.tag,
            description: this.props.user.description,
            city: this.props.user.city,
            date: this.props.user.date,
            setup: this.props.user.setup,
            id: this.props.id
        }

        //console.log(this.props.token, this.props.id)
        //console.log('SECOND BRUUUUUUUUUUUH')
        this.props.dispatch(Actions.setUser(this.props.user))

        switch(this.props.user.setup){
            case 0:{
                this.props.navigation.navigate('AddPhoto')
                break
            }
            case 1:{
                this.props.navigation.navigate('SetUsername')
                break
            }
            case 2:{
                this.props.navigation.navigate('ChooseHeader')
                break
            }
            case 3:{
                this.props.navigation.navigate('DescribeYourself')
                break
            }
            case 4:{
                this.props.navigation.navigate('YourLocation')
                break
            }
            default:{
                this.props.navigation.navigate('Feed')
                break;
            }
        }
        /*let result = await LoginActions.login(JSON.stringify({
            email: this.props.login, 
            password: this.props.password
        }))
        if(result.statusCode === 200){
            console.log(result)
            this.props.dispatch(TokenActions.setToken(result.token))
            this.props.dispatch(IDActions.setID(result.id))
            this.props.navigation.navigate('SetupNewAccount')
        }
        else{
            MessageBarManager.showAlert({
                title: 'Error',
                message: result.status,
                alertType: 'error',
                position: 'bottom',
            });
            this.setState({isLoading: false})
        }*/
        //this.props.navigation.navigate('SetupNewAccount')
        //console.log(result)
    }

    //Lonely Astronaut
    //thelonelyastronaut1337@gmail.com
    //LoginHorizon

    render(){
        //console.log(this.props.login, this.props.password)
        return(
            <SafeAreaView style={styles.container}>
                <KeyboardAvoidingView style={{flex: 1, padding: 30, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={styles.title}>{strings('FinishRestoreScreen.screenTitle')}</Text>
                    <TouchableOpacity style={{backgroundColor: '#0AB4BE',width: 250, justifyContent: 'center', borderRadius: 40, height: 40, marginTop: 40, alignSelf: 'center'}} onPressOut={this.confirm} disabled={this.state.isLoading}>
                        {
                            this.state.isLoading ? <DotIndicator count={3} color="white" size={8}/> : <Text style={{fontSize: 15, color: 'white', alignSelf: 'center'}}>{strings('FinishRestoreScreen.send')}</Text>
                        }
                    </TouchableOpacity>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 32,
        marginBottom: 20,
        fontWeight: 'bold',
        textAlign: 'center'
    },
})
export default connect(state => ({email: state.email, password: state.password, token: state.token, id: state.id, user: state.user}))(FinishRestore)