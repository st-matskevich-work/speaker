import React from 'react'
import {FlatList, StyleSheet, View, Text, ScrollView, Dimensions, StatusBar} from 'react-native'
import {NavigationEvents} from 'react-navigation'
import SpeakBlock from '../../Components/SpeakBlock'
import { connect } from 'react-redux'
import * as Actions from '../../Actions/Actions'
import { strings } from '../../../translation/i18n'

class SpeaksList extends React.Component{
    static navigationOptions = ({navigation}) => {
        return {
            tabBarLabel: 'Home!'
        }
    }

    state = {
        currentRef: {},
        previousRef: {}
    }

    render(){
        return(
            <View ref={ref => this.ref = ref} onLayout={(event) => {
                this.props.heightCallback(event.nativeEvent.layout.height)
                //console.log(event.nativeEvent.layout.height, 'event')
            }} style={{minHeight: Dimensions.get('window').height - 52 - StatusBar.currentHeight, backgroundColor: 'white'}}>
               {
                   this.props?.speaks?.length ? 
                        this.props.speaks.map((value, index) => {
                            console.log(value.id+index, 'value.id+index')
                            return <SpeakBlock 
                                            syncronize={(ref) => {
                                                //console.log('REF', ref)
                                                this.state.previousRef = this.state.currentRef
                                                this.state.currentRef = ref
                                                this.forceUpdate()
                                            }}
                                            currentUserId={this.props.userId}
                                            navigation={this.props.navigation} 
                                            player={this.props.player}
                                            setNewPlayer={(player) => {
                                                //console.log(this.state.currentRef, '\n\n\n\n\n\n\n\n\n\n')
                                                //this.props.player?.pause()
                                                if(this.state.previousRef.resetProgress) this.state.previousRef.resetProgress()
                                                this.props.setNewPlayer(player)
                                            }}
                                            key={value.uniqueId ? value.uniqueId : value.id} 
                                            speak={value}
                                            parentUpdate={() => this.props.updateList(false)}
                                            setLikeMark={(id, speakerId, userId) => {
                                                //console.log(id)
                                                
                                                this.props.dispatch(Actions.setProfileLikemark(id, speakerId, userId))
                                                this.forceUpdate()
                                                
                                                setTimeout(() => this.props.updateList(false), 500)
                            }               }/>
                        }) : (
                            <View style={{flex: 1, alignItems: 'center', marginTop: 10}}>
                                <Text style={{color: '#0AB4BE', fontWeight: 'bold', fontSize: 18}}>{strings('ProfileScreen.scrollTitle')}</Text>
                                <Text style={{textAlign: 'center', marginTop: 5, color: '#0AB4BE'}}>{this.props.nullText}</Text>
                            </View>
                    )
               } 
            </View>
        )
    }
}

export default connect(state => ({userSpeaks: state.userSpeaks}))(SpeaksList)