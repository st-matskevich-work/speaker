import React from 'react'
import {ScrollView, View, Text, Image, TouchableOpacity, RefreshControl} from 'react-native'
import { connect } from 'react-redux'
import NotificationsComponent from '../Components/NotificationComponent'
import { strings } from '../../translation/i18n'

class NotificationList extends React.Component{
    state = {
        refreshing: false
    }

    onRefresh = () => {
        this.setState({refreshing: true})
        this.props.onRefresh(() => this.setState({refreshing: false}) )
    }
    
    render(){
        return(
            <ScrollView contentContainerStyle={{flexGrow: 1, backgroundColor: 'white'}} refreshControl={
                <RefreshControl colors={['#0AB4BE']} refreshing={this.state.refreshing} onRefresh={this.onRefresh}/>
            }>
                {
                    this.props.data?.length ? (
                        this.props.data.map((value, i) => {
                            return <NotificationsComponent type={this.props.type} key={i} payload={value} navigation={this.props.navigation}/>
                        })
                    ) : (
                        <View style={{flexGrow: 1, backgroundColor: '#DBEFF0', justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{fontSize: 26, fontWeight: 'bold'}}>{this.props.title}</Text>
                            <Text style={{fontSize: 15, textAlign: 'center', margin: 20}}>{this.props.body}</Text>
                        </View>
                    )
                }
            </ScrollView>
        )
    }
}

export default connect(state => ({}))(NotificationList)