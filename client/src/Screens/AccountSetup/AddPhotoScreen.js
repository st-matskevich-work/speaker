import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import {View, Text, StyleSheet, TouchableOpacity, Image,NativeModules, Button, KeyboardAvoidingView, Keyboard, TouchableWithoutFeedbackBase, Platform} from 'react-native'
import { connect } from 'react-redux'
import CustomInput from '../../Components/CustomInput'
import ImagePicker from 'react-native-image-crop-picker';
import * as Constants from '../../Constants/Constants'
import {DotIndicator} from 'react-native-indicators';
import * as SetupAPI from '../../API/AccountSetup'
import { showMessage, hideMessage } from "react-native-flash-message";
import {MessageBar as MessageBarAlert, MessageBarManager} from 'react-native-message-bar'
import { NavigationEvents } from 'react-navigation'
import { strings } from '../../../translation/i18n'

class AddPhoto extends React.Component{
    static navigationOptions = ({navigation}) => {
        // headerTitle instead of title
        return {
            headerTitleAlign: 'center',
            headerTitle: () => (
                <Image source={require('../../Images/Logo1.png')} style={{height: 30, width: 30, resizeMode: 'contain'}}/>
            ),
            headerStyle: {
                elevation: 0, //for android
                shadowOpacity: 0, //for ios
                borderBottomWidth: 0.3, //for i
                borderBottomColor: 'rgba(151,151,151,0.5)'
            }
        }
    };

    state = {
        showPopUp: false,
        uploaded: false,
        loading: false,
        base64: "NaN",
        extension: "NaN",
        errorText: ""
    }

    componentDidMount(){
        //MessageBarManager.registerMessageBar(this.refs.alert);
    }

    componentWillUnmount(){
        //MessageBarManager.unregisterMessageBar();
    }


    choosePhoto = () => {
        this.setState({errorText: ""})

        ImagePicker.openPicker({
            cropping: true,
            includeBase64: true
        })
        .catch(error => this.setState({loading: false}))
        .then(photo => {
            this.setState({base64: photo.data, extension: photo.mime.split('/')[1], uploaded: true})
        })
        .catch(err => this.setState({loading: false}))
    }

    upload = () => {
        this.setState({loading: true})
        //console.log(this.state.base64)
        SetupAPI.setAvatar(this.state.base64, this.state.extension, this.props.token, this.props.id)
        .then(res => {
            console.log(res, "RESULT")
            if(res.statusCode === 200){
                this.props.navigation.navigate('SetUsername')
                this.setState({loading: false})
            }else{
                /*MessageBarManager.showAlert({
                    title: 'Ошибка',
                    message: res.status,
                    alertType: 'error',
                    position: 'bottom',
                });*/
                /*showMessage({
                    message: 'Ошибка',
                    description: res.status,
                    type: "danger",
                  });*/
                this.setState({loading: false, errorText: res.status})
            }
        })
        .catch(err => {
            /*MessageBarManager.showAlert({
                title: 'Ошибка',
                message: err,
                alertType: 'error',
                position: 'bottom',
            });*/
            /*showMessage({
                message: 'Ошибка',
                description: err,
                type: "danger",
            });*/
            this.setState({loading: false, errorText: err})
        })
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                <NavigationEvents onWillBlur={() => this.setState({errorText: ""})}/>
                <KeyboardAvoidingView style={{padding: 30, paddingBottom: 0, flex: 1}} behavior="height">
                    <Text style={styles.title}>{strings('AddPhotoScreen.screenTitle')}</Text>
                    <Text style={{fontSize: 15, marginBottom: 15}}>{strings('AddPhotoScreen.screenDescription')}</Text>
                    {
                        this.state.errorText ? <Text style={{fontSize: 16, color: 'red', marginBottom: 15}}>{this.state.errorText}</Text> : null
                    }
                    <View style={{flex: 1}}/>
                    <TouchableOpacity style={{alignSelf:'center', height: 164, width: 164, borderWidth: 3, borderColor: '#0AB4BE', borderRadius: 20, justifyContent: 'center', alignItems: 'center'}} onPressOut={this.choosePhoto}>
                        {
                            this.state.extension != "NaN" ? <Image source={{uri: `data:image/${this.state.extension};base64,${this.state.base64}`}} style={{height: 158, width: 158, borderRadius: 17}}/> : 
                            <View>
                                <Image source={require('../../Images/photo.png')}/>
                                <Text style={{color: '#0AB4BE', marginTop: 5, textAlign: 'center'}}>{strings('AddPhotoScreen.upload')}</Text>
                            </View>
                        }
                    </TouchableOpacity>
                    <View style={{flex: 4}}/>
                    <View style={{flexDirection: 'row', height: 50, width: '100%'}}>
                        <TouchableOpacity style={{height: 40, width: 100, justifyContent: 'center', alignItems: 'center'}} onPressOut={() => {
                            this.setState({base64: "NaN", extension: "NaN"})
                            this.state.base64 = "NaN"
                            this.state.extension = "NaN"
                            this.forceUpdate()
                            this.upload()
                        }} disabled={this.state.loading}>
                            <Text style={{color: '#0AB4BE', fontSize: 15}}>{strings('AddPhotoScreen.skip')}</Text>
                        </TouchableOpacity>
                        <View style={{flex: 1}}/>
                        <TouchableOpacity style={{backgroundColor: '#0AB4BE', width: 100, justifyContent: 'center', borderRadius: 40, height: 40}} onPressOut={() => this.state.uploaded ? (this.state.base64 ? this.upload() : null) : null} disabled={this.state.loading}>
                            {
                                this.state.loading ? <DotIndicator count={3} color="white" size={8}/> : <Text style={{fontSize: 15, color: 'white', alignSelf: 'center'}}>{strings('AddPhotoScreen.next')}</Text>
                            }
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
    },
    title: {
        fontSize: 32,
        marginBottom: 30,
        fontWeight: 'bold'
    },
})
export default connect(state => ({token: state.token, id: state.id}))(AddPhoto)