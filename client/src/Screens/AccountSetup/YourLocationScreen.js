import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import {View, Text, StyleSheet, TouchableOpacity, Image, Button, ScrollView, KeyboardAvoidingView, Keyboard, TouchableWithoutFeedbackBase} from 'react-native'
import { connect } from 'react-redux'
import CustomInput from '../../Components/CustomInput'
import * as Constants from '../../Constants/Constants'
import {DotIndicator} from 'react-native-indicators';
import * as SetupAPI from '../../API/AccountSetup'
import { showMessage, hideMessage } from "react-native-flash-message";
import {MessageBar as MessageBarAlert, MessageBarManager} from 'react-native-message-bar'
import { NavigationEvents } from 'react-navigation'
import { strings } from '../../../translation/i18n'

class YourLocation extends React.Component{
    static navigationOptions = ({navigation}) => {
        return {
            headerTitleAlign: 'center',
            headerTitle: () => (
                <Image source={require('../../Images/Logo1.png')} style={{height: 30, width: 30, resizeMode: 'contain'}}/>
            ),
            headerLeft: () => (
                <TouchableOpacity style={{marginLeft: 20}} onPressOut={() => navigation.navigate('DescribeYourself')}>
                    <Image source={require('../../Images/back.png')} style={{height: '100%', width: 16, resizeMode: 'contain'}}/>
                </TouchableOpacity>
            ),
            headerStyle: {
                elevation: 0, //for android
                shadowOpacity: 0, //for ios
                borderBottomWidth: 0.3, //for i
                borderBottomColor: 'rgba(151,151,151,0.5)'
            }
        }
    };

    state = {
        showPopUp: false,
        city: "",
        loading: false,
        errorText: ""
    }

    componentDidMount(){
        //MessageBarManager.registerMessageBar(this.refs.bruh);
    }

    componentWillUnmount(){
        //MessageBarManager.unregisterMessageBar();
    }

    setCity = (skip) => {
        //if(!this.state.city.length) return
        //let responseStatus = 200
        //if(skip) this.setState({loading: true, city: "NaN"})
        this.setState({loading: true})

        console.log(this.state)

        SetupAPI.setCity(skip ? "NaN" : this.state.city, this.props.token, this.props.id)
        .then(res => {
            if(res.statusCode === 200){
                this.props.navigation.navigate('FinalScreen')
                this.setState({loading: false})
            }else{
                /*MessageBarManager.showAlert({
                    title: 'Ошибка',
                    message: res.status,
                    alertType: 'error',
                    position: 'bottom',
                });*/
                this.setState({loading: false, errorText: res.status})
            }
        })
        .catch(err => {
            /*MessageBarManager.showAlert({
                title: 'Ошибка',
                message: err,
                alertType: 'error',
                position: 'bottom',
            });*/
            this.setState({loading: false, errorText: err})
        })
        /*fetch(ServerConstants.SERVER_ADDRESS + '/users/setup', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                city: this.state.city,
                token: this.props.token,
                id: this.props.id
            })
        }).then(response => {
            responseStatus = response.status
            //console.log(responseStatus)
            return response.json()
        }).then(response => {
            if(responseStatus === 200){
                this.props.navigation.navigate('FinalScreen')
            }
            else this.setState({loading: false})
        }).catch(err => console.log(err))*/
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                <NavigationEvents onWillBlur={() => this.setState({errorText: ""})}/>
                <KeyboardAvoidingView style={{flex: 1}}>
                    <ScrollView contentContainerStyle={{padding: 30}}>
                        <Text style={styles.title}>{strings('YourLocationScreen.screenTitle')}</Text>
                        <Text style={{fontSize: 18, marginBottom: 30}}>{strings('YourLocationScreen.screenDescription')}</Text>
                        <CustomInput placeholder={strings('YourLocationScreen.placeholder')} type="none" onChangeText={(text) => this.setState({city: text, errorText: ""})}/>
                        {
                            this.state.errorText ? <Text style={{fontSize: 16, color: 'red', marginBottom: 15}}>{this.state.errorText}</Text> : null
                        }
                    </ScrollView>
                    <View style={{flex: 1}}/>
                    <View style={{flexDirection: 'row', height: 60, width: '100%', alignItems: 'center'}}>
                        <TouchableOpacity style={{height: 40,marginLeft: 30, width: 100, justifyContent: 'center', alignItems: 'center'}} disabled={this.state.loading} onPressOut={() => {
                            //this.setState({city: "NaN"})
                            this.setCity(true)
                        }}>
                            <Text style={{color: '#0AB4BE', fontSize: 15}}>{strings('YourLocationScreen.skip')}</Text>
                        </TouchableOpacity>
                        <View style={{flex: 1}}/>
                        <TouchableOpacity style={{backgroundColor: '#0AB4BE',marginRight: 30, width: 100, justifyContent: 'center', borderRadius: 40, height: 40}} onPressOut={() => this.setCity(false)} disabled={this.state.loading || !this.state.city.length ? true : false}>
                            {
                                this.state.loading ? <DotIndicator count={3} color="white" size={8}/> : <Text style={{fontSize: 15, color: 'white', alignSelf: 'center'}}>{strings('YourLocationScreen.next')}</Text>
                            }
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
    },
    title: {
        fontSize: 32,
        marginBottom: 30,
        fontWeight: 'bold'
    },
})
export default connect(state => ({token: state.token, id: state.id}))(YourLocation)