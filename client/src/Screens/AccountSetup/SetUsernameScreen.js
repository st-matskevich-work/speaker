import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import {View, Text, StyleSheet, TouchableOpacity, Image, Button, KeyboardAvoidingView, Keyboard, TouchableWithoutFeedbackBase, ScrollView} from 'react-native'
import { connect } from 'react-redux'
import CustomInput from '../../Components/CustomInput'
import * as Constants from '../../Constants/Constants'
import {DotIndicator} from 'react-native-indicators';
import * as Actions from '../../Actions/Actions'
import * as SetupAPI from '../../API/AccountSetup'
import * as UsersAPI from '../../API/Users'
import { showMessage, hideMessage } from "react-native-flash-message";
import {MessageBar as MessageBarAlert, MessageBarManager} from 'react-native-message-bar'
import { NavigationEvents } from 'react-navigation'
import { strings } from '../../../translation/i18n'

class SetUsername extends React.Component{
    static navigationOptions = ({navigation}) => {
        // headerTitle instead of title
        return {
            headerTitleAlign: 'center',
            headerTitle: () => (
                <Image source={require('../../Images/Logo1.png')} style={{height: 30, width: 30, resizeMode: 'contain'}}/>
            ),
            headerLeft: () => (
                <TouchableOpacity style={{marginLeft: 20}} onPressOut={() => navigation.navigate('AddPhoto')}>
                    <Image source={require('../../Images/back.png')} style={{height: '100%', width: 16, resizeMode: 'contain'}}/>
                </TouchableOpacity>
            ),
            headerStyle: {
                elevation: 0, //for android
                shadowOpacity: 0, //for ios
                borderBottomWidth: 0.3, //for i
                borderBottomColor: 'rgba(151,151,151,0.5)'
            }
        }
    };

    state = {
        showPopUp: false,
        tag: "",
        loading: false,
        errorText: ""
    }

    componentDidMount(){
        //MessageBarManager.registerMessageBar(this.refs.bruh);
    }

    componentWillUnmount(){
        //MessageBarManager.unregisterMessageBar();
    }

    
    setTag = () => {
        //console.log(this.state.tag.length == 0)
        if(this.state.tag.length == 0){
            /*showMessage({
                message: 'Ошибка',
                description: "Имя пользователя не может быть пустым",
                type: "danger",
              });*/
            this.setState({errorText: strings('Errors.noUsername')})
            return
        }

        if(!this.tagInput.getFieldState()) return
        
        this.setState({loading: true})
        SetupAPI.setTag(this.state.tag, this.props.token, this.props.id)
        .then(res => {
            if(res.statusCode === 200){
                console.log(res)
                UsersAPI.getUserInfoById(this.props.id)
                .then(res => {
                    console.log(res, this.state.tag, this.props.id)
                    if(res.statusCode === 200){
                        this.props.dispatch(Actions.setUser(res))

                        this.props.navigation.navigate('ChooseHeader')
                        this.setState({loading: false})
                    }else{
                        /*MessageBarManager.showAlert({
                            title: 'Ошибка',
                            message: res.status,
                            alertType: 'error',
                            position: 'bottom',
                        });*/
                        /*showMessage({
                            message: 'Ошибка',
                            description: res.status,
                            type: "danger",
                          });*/
                        this.setState({loading: false, errorText: res.status}) 
                    }
                })
                .catch(err => {
                    /*MessageBarManager.showAlert({
                        title: 'Ошибка',
                        message: err,
                        alertType: 'error',
                        position: 'bottom',
                    });*/
                    /*showMessage({
                        message: 'Ошибка',
                        description: err,
                        type: "danger",
                      });*/
                    this.setState({loading: false, errorText: err})
                })
            }else{
               /* MessageBarManager.showAlert({
                    title: 'Ошибка',
                    message: res.status,
                    alertType: 'error',
                    position: 'bottom',
                });*/
                /*showMessage({
                    message: 'Ошибка',
                    description: res.status,
                    type: "danger",
                  });*/
                this.setState({loading: false, errorText: res.status})   
            }
        })  
        .catch(err => {
            /*MessageBarManager.showAlert({
                title: 'Ошибка',
                message: err,
                alertType: 'error',
                position: 'bottom',
            });*/
            /*showMessage({
                message: 'Ошибка',
                description: err,
                type: "danger",
              });*/
            this.setState({loading: false, errorText: err})
        })

        /*fetch(ServerConstants.SERVER_ADDRESS + '/users/setup', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                tag: this.state.tag,
                token: this.props.token,
                id: this.props.id
            })
        }).then(response => {
            responseStatus = response.status
            return response.json()
        }).then(response => {
            console.log(response)
            if(responseStatus === 200){
                fetch(ServerConstants.SERVER_ADDRESS + '/users/getInfo', {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        id: this.props.id
                    })
                }).then((res) => {
                    responseStatus = res.status
                    return res.json()
                }).then(res => {
                    if(responseStatus === 200){
                        let user = {
                            name: res.name,
                            email: res.email,
                            avatar: res.image,
                            header: res.headerImage,
                            tag: res.tag,
                            description: res.description,
                            city: res.city
                        }
                        this.props.dispatch(UserActions.setUser(user))
                        this.props.navigation.navigate('ChooseHeader')
                    }else{
                        this.setState({loading: false})
                    }
                }).catch(err => console.log(err))
            }
            else{
                this.setState({loading: false})
            }
        })*/
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                <NavigationEvents onWillBlur={() => this.setState({errorText: ""})}/>
                <KeyboardAvoidingView style={{flex: 1}}>
                    <ScrollView contentContainerStyle={{padding: 30}}>
                        <Text style={styles.title}>{strings('SetUsernameScreen.screenTitle')}</Text>
                        <Text style={{fontSize: 18, marginBottom: 30}}>{strings('SetUsernameScreen.screenDescription')}</Text>
                        <CustomInput checkAvailability={true} regex={/^[0-9a-zA-Z]+$/} onRef={ref => this.tagInput = ref} placeholder={strings('SetUsernameScreen.placeholder')} type="username" onChangeText={(text) => this.setState({tag: text, errorText: ""})}/>
                        {
                            this.state.errorText ? <Text style={{fontSize: 16, color: 'red', marginBottom: 15}}>{this.state.errorText}</Text> : null
                        }
                    </ScrollView>
                    <View style={{flex: 1}}/>
                    <View style={{flexDirection: 'row', height: 60, width: '100%', alignItems: 'center'}}>
                        <View style={{flex: 1}}/>
                        <TouchableOpacity style={{backgroundColor: '#0AB4BE',marginRight: 30,width: 100, justifyContent: 'center', borderRadius: 40, height: 40}} onPressOut={this.setTag} disabled={this.state.loading}>
                            {
                                this.state.loading ? <DotIndicator count={3} color="white" size={8}/> : <Text style={{fontSize: 15, color: 'white', alignSelf: 'center'}}>{strings('SetUsernameScreen.next')}</Text>
                            }
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
    },
    title: {
        fontSize: 32,
        marginBottom: 30,
        fontWeight: 'bold'
    },
})

export default connect(state => ({token: state.token, id: state.id}))(SetUsername)