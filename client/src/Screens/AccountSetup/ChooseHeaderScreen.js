import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import {View, Text, StyleSheet, TouchableOpacity, Image, Button, KeyboardAvoidingView, Keyboard, TouchableWithoutFeedbackBase} from 'react-native'
import { connect } from 'react-redux'
import CustomInput from '../../Components/CustomInput'
import ImagePicker from 'react-native-image-crop-picker';
import * as Constants from '../../Constants/Constants'
import {DotIndicator} from 'react-native-indicators';
import * as SetupAPI from '../../API/AccountSetup'
import { showMessage, hideMessage } from "react-native-flash-message";
import {MessageBar as MessageBarAlert, MessageBarManager} from 'react-native-message-bar'
import { NavigationEvents } from 'react-navigation'
import {strings} from '../../../translation/i18n'

class ChooseHeader extends React.Component{
    static navigationOptions = ({navigation}) => {
        // headerTitle instead of title
        return {
            headerTitleAlign: 'center',
            headerTitle: () => (
                <Image source={require('../../Images/Logo1.png')} style={{height: 30, width: 30, resizeMode: 'contain'}}/>
            ),
            headerLeft: () => (
                <TouchableOpacity style={{marginLeft: 20}} onPressOut={() => navigation.navigate('SetUsername')}>
                    <Image source={require('../../Images/back.png')} style={{height: '100%', width: 16, resizeMode: 'contain'}}/>
                </TouchableOpacity>
            ),
            headerStyle: {
                elevation: 0, //for android
                shadowOpacity: 0, //for ios
                borderBottomWidth: 0.3, //for i
                borderBottomColor: 'rgba(151,151,151,0.5)'
            }
        }
    };

    state = {
        showPopUp: false,
        uploaded: false,
        loading: false,
        base64: "NaN",
        extension: "NaN",
        errorText: ""
    }

    componentDidMount(){
       // MessageBarManager.registerMessageBar(this.refs.alert);
    }

    componentWillUnmount(){
        //MessageBarManager.unregisterMessageBar();
    }

    choosePhoto = () => {
        this.setState({errorText: ""})
        ImagePicker.openPicker({
            cropping: true,
            includeBase64: true
        }).catch(error => this.setState({loading: false}))
        .then(photo => {
            this.setState({base64: photo.data, extension: photo.mime.split('/')[1], uploaded: true})
        })
        .catch(err => this.setState({loading: false}))
        /*.then(photo => {
            if(!photo) return
            this.setState({loading: true})
            console.log(photo.path)
            this.setState({image: photo})
            fetch(ServerConstants.SERVER_ADDRESS + '/users/setup', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    headerImage: photo.data,
                    extension: photo.mime.split('/')[1],
                    token: this.props.token,
                    id: this.props.id
                })
            })
            .then(response => {
                responseStatus = response.status
                console.log(responseStatus, photo.data.length)
                console.log(response)
                return response.json()
            })
            .then(response => {
                if(responseStatus === 200){
                    //save image logic
                    this.setState({uploaded: true, loading: false})
                }
            })
        }).catch(error => {
            console.log(error)
            this.setState({loading: false})
        });*/
    }

    upload = (upload) => {
        this.setState({loading: true})
        console.log(this.state)
        SetupAPI.setHeader(upload ? this.state.base64 : "NaN", upload ? this.state.extension : "NaN", this.props.token, this.props.id)
        .then(res => {
            if(res.statusCode === 200){
                this.props.navigation.navigate('DescribeYourself')
                this.setState({loading: false})
            }else{
               /* MessageBarManager.showAlert({
                    title: 'Ошибка',
                    message: res.status,
                    alertType: 'error',
                    position: 'bottom',
                });*/
                /*showMessage({
                    message: 'Ошибка',
                    description: res.status,
                    type: "danger",
                  });*/
                this.setState({loading: false, errorText: res.status})
            }
        })
        .catch(err => {
            /*MessageBarManager.showAlert({
                title: 'Ошибка',
                message: err,
                alertType: 'error',
                position: 'bottom',
            });*/
            /*showMessage({
                message: 'Ошибка',
                description: err,
                type: "danger",
              });*/
            this.setState({loading: false, errorText: err})
        })
    }

    render(){
        console.log(Constants.SERVER_ADDRESS + this.props.user.avatar)
        return(
            <SafeAreaView style={styles.container}>
                <NavigationEvents onWillBlur={() => this.setState({errorText: ""})}/>
                <KeyboardAvoidingView style={{padding: 30, paddingBottom: 0, flex: 1}} behavior="height">
                    <Text style={styles.title}>{strings('ChooseHeaderScreen.screenTitle')}</Text>
                    <Text style={{fontSize: 15, marginBottom: 15}}>{strings('ChooseHeaderScreen.screenDescription')}</Text>
                    {
                        this.state.errorText ? <Text style={{fontSize: 16, color: 'red', marginBottom: 15}}>{this.state.errorText}</Text> : null
                    }
                    <View style={{height: 120}}>
                        <TouchableOpacity style={{width: '100%', backgroundColor: '#EDF7F8', height: 90, justifyContent: 'center', alignItems: 'center'}} onPressOut={this.choosePhoto}>
                            {
                                this.state.base64 != "NaN" ? <Image source={{uri: `data:image/${this.state.extension};base64,${this.state.base64}`}} style={{height: 90, width: '100%'}}/> :
                                <View>
                                    <Image source={require('../../Images/photo.png')} style={{height: 32, width: 32, alignSelf: 'center'}}/>
                                    <Text style={{color: '#0AB4BE', marginTop: 5, alignSelf: 'center'}}>{strings('ChooseHeaderScreen.upload')}</Text>
                                </View>
                            }
                        </TouchableOpacity>
                        <View style={{borderWidth: 3, borderColor: 'white', top: 60, left: 20, position: 'absolute', height: 60, width: 60, borderRadius: 60}}>
                            <Image source={{uri: Constants.SERVER_ADDRESS + this.props.user.image}} style={{height: 54, width: 54, borderRadius: 60, backgroundColor: '#f6f7f8'}}/>
                        </View>
                    </View>
                    <Text style={{marginLeft: 20, fontSize: 18, marginTop: 10}}>{this.props.user.name}</Text>
                    <Text style={{marginTop: 5, marginLeft: 20}}>@{this.props.user.tag}</Text>
                    <View style={{flex: 1}}/>
                    <View style={{flexDirection: 'row', height: 50, width: '100%'}}>
                        <TouchableOpacity style={{height: 40, width: 100, justifyContent: 'center', alignItems: 'center'}} onPressOut={() => {
                            this.setState({base64: "NaN", extension: "NaN"})
                            this.upload(false)
                        }} disabled={this.state.loading}>
                            <Text style={{color: '#0AB4BE', fontSize: 15}}>{strings('ChooseHeaderScreen.skip')}</Text>
                        </TouchableOpacity>
                        <View style={{flex: 1}}/>
                        <TouchableOpacity style={{backgroundColor: '#0AB4BE', width: 100, justifyContent: 'center', borderRadius: 40, height: 40}} onPressOut={() => this.state.uploaded ? (this.state.base64 ? this.upload(true) : null) : null} disabled={this.state.loading}>
                            {
                                this.state.loading ? <DotIndicator count={3} color="white" size={8}/> : <Text style={{fontSize: 15, color: 'white', alignSelf: 'center'}}>{strings('ChooseHeaderScreen.next')}</Text>
                            }
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
    },
    title: {
        fontSize: 32,
        marginBottom: 30,
        fontWeight: 'bold'
    },
})
export default connect(state => ({token: state.token, id: state.id, user: state.user}))(ChooseHeader)