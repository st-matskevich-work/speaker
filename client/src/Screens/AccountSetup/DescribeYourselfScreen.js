import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import {View, Text, StyleSheet, TouchableOpacity, Image, ScrollView, Button, KeyboardAvoidingView, Keyboard, TouchableWithoutFeedbackBase} from 'react-native'
import {DotIndicator} from 'react-native-indicators'
import { connect } from 'react-redux'
import CustomInput from '../../Components/CustomInput'
import * as Constants from '../../Constants/Constants'
import * as SetupAPI from '../../API/AccountSetup'
import { showMessage, hideMessage } from "react-native-flash-message";
import {MessageBar as MessageBarAlert, MessageBarManager} from 'react-native-message-bar'
import { NavigationEvents } from 'react-navigation'
import {strings} from '../../../translation/i18n'

class DescribeYourself extends React.Component{
    static navigationOptions = ({navigation}) => {
        return {
            headerTitleAlign: 'center',
            headerTitle: () => (
                <Image source={require('../../Images/Logo1.png')} style={{height: 30, width: 30, resizeMode: 'contain'}}/>
            ),
            headerLeft: () => (
                <TouchableOpacity style={{marginLeft: 20}} onPressOut={() => navigation.navigate('ChooseHeader')}>
                    <Image source={require('../../Images/back.png')} style={{height: '100%', width: 16, resizeMode: 'contain'}}/>
                </TouchableOpacity>
            ),
            headerStyle: {
                elevation: 0, //for android
                shadowOpacity: 0, //for ios
                borderBottomWidth: 0.3, //for i
                borderBottomColor: 'rgba(151,151,151,0.5)'
            }
        }
    };

    state = {   
        showPopUp: false,
        description: "",
        loading: false,
        errorText: ""
    }

    componentDidMount(){
        //MessageBarManager.registerMessageBar(this.refs.bruh);
    }

    componentWillUnmount(){
        //MessageBarManager.unregisterMessageBar();
    }

    setDescription = (skip) => {
        //if(skip) this.setState({loading: true, city: "NaN"})
        this.setState({loading: true})

        SetupAPI.setDescription(skip ? "NaN" : this.state.description, this.props.token, this.props.id)
        .then(res => {
            if(res.statusCode === 200){
                this.props.navigation.navigate('YourLocation')
                this.setState({loading: false})
            }else{
                /*MessageBarManager.showAlert({
                    title: 'Ошибка',
                    message: res.status,
                    alertType: 'error',
                    position: 'bottom',
                });*/
                /*showMessage({
                    message: 'Ошибка',
                    description: res.status,
                    type: "danger",
                  });*/
                this.setState({loading: false, errorText: res.status})
            }
        })
        .catch(err => {
            /*MessageBarManager.showAlert({
                title: 'Ошибка',
                message: err,
                alertType: 'error',
                position: 'bottom',
            });*/
            /*showMessage({
                message: 'Ошибка',
                description: err,
                type: "danger",
              });*/
            this.setState({loading: false, errorText: err})
        })

        /*fetch(ServerConstants.SERVER_ADDRESS + '/users/setup', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                description: this.state.description,
                token: this.props.token,
                id: this.props.id
            })
        }).then(response => {
            responseStatus = response.status
            //console.log(responseStatus)
            return response.json()
        }).then(response => {
            if(responseStatus === 200){
                this.props.navigation.navigate('YourLocation')
            }
            else this.setState({loading: false})
        })*/
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                <NavigationEvents onWillBlur={() => this.setState({errorText: ""})}/>
                <KeyboardAvoidingView style={{flex: 1}}>
                    <ScrollView contentContainerStyle={{padding: 30}}>
                        <Text style={styles.title}>{strings('DescribeYourselfScreen.screenTitle')}</Text>
                        <Text style={{fontSize: 18, marginBottom: 30}}>{strings('DescribeYourselfScreen.screenDescription')}</Text>
                        <CustomInput blurOnSubmit={true} maxLength={256} placeholder={strings('DescribeYourselfScreen.placeholder')} type="none" onChangeText={(text) => this.setState({description: text, errorText: ""})} multiline={true}/>
                        {
                            this.state.errorText ? <Text style={{fontSize: 16, color: 'red', marginBottom: 15}}>{this.state.errorText}</Text> : null
                        }
                    </ScrollView>
                    <View style={{flex: 1}}/>
                    <View style={{flexDirection: 'row', height: 60, width: '100%', alignItems: 'center'}}>
                        <TouchableOpacity style={{height: 40,marginLeft: 30, width: 100, justifyContent: 'center', alignItems: 'center'}} disabled={this.state.loading} onPressOut={() => {
                            //this.setState({description: "NaN"})
                            this.setDescription(true)
                        }}>
                            <Text style={{color: '#0AB4BE', fontSize: 15}}>{strings('DescribeYourselfScreen.skip')}</Text>
                        </TouchableOpacity>
                        <View style={{flex: 1}}/>
                        <TouchableOpacity style={{backgroundColor: '#0AB4BE', width: 100, justifyContent: 'center', borderRadius: 40,marginRight: 30, height: 40}} onPressOut={() => this.setDescription(false)} disabled={this.state.loading || !this.state.description.length ? true : false}>
                            {
                                this.state.loading ? <DotIndicator count={3} color="white" size={8}/> : <Text style={{fontSize: 15, color: 'white', alignSelf: 'center'}}>{strings('DescribeYourselfScreen.next')}</Text>
                            }
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
    },
    title: {
        fontSize: 32,
        marginBottom: 30,
        fontWeight: 'bold'
    },
})
export default connect(state => ({token: state.token, id: state.id}))(DescribeYourself)