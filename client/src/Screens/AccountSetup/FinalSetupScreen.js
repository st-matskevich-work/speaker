import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import {View, Text, StyleSheet, TouchableOpacity, Image, Button, KeyboardAvoidingView, Keyboard, TouchableWithoutFeedbackBase} from 'react-native'
import { connect } from 'react-redux'
import CustomInput from '../../Components/CustomInput'
import * as Actions from '../../Actions/Actions'
import * as Constants from '../../Constants/Constants'
import {DotIndicator} from 'react-native-indicators';
import * as UsersAPI from '../../API/Users'
import { showMessage, hideMessage } from "react-native-flash-message";
import {MessageBar as MessageBarAlert, MessageBarManager} from 'react-native-message-bar'
import { strings } from '../../../translation/i18n'

class FinalSetup extends React.Component{
    static navigationOptions = ({navigation}) => {
        // headerTitle instead of title
        return {
            headerTitleAlign: 'center',
            headerTitle: () => (
                <Image source={require('../../Images/Logo1.png')} style={{height: 30, width: 30, resizeMode: 'contain'}}/>
            ),
            headerLeft: () => (
                <TouchableOpacity style={{marginLeft: 20}} onPressOut={() => navigation.goBack()}>
                    <Image source={require('../../Images/back.png')} style={{height: '100%', width: 16, resizeMode: 'contain'}}/>
                </TouchableOpacity>
            ),
            headerStyle: {
                elevation: 0, //for android
                shadowOpacity: 0, //for ios
                borderBottomWidth: 0.3, //for i
                borderBottomColor: 'rgba(151,151,151,0.5)'
            }
        }
    };

    state = {
        showPopUp: false,
        loading: false
    }

    componentDidMount(){
        //MessageBarManager.registerMessageBar(this.refs.bruh);
    }

    componentWillUnmount(){
        //MessageBarManager.unregisterMessageBar();
    }

    toProfile = () => {
        UsersAPI.getUserInfoById(this.props.id)
        .then(result => {
            if(result.statusCode === 200){
                this.props.dispatch(Actions.setUser(result))
                this.props.navigation.navigate('Feed')
            }else{
                showMessage({
                    message: 'Ошибка',
                    description: result.status,
                    type: "danger",
                  });
                this.setState({isLoading: false})
            }
        }).catch(err => {
            showMessage({
                message: 'Ошибка',
                description: err,
                type: "danger",
              });
            this.setState({isLoading: false})
        })
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                <KeyboardAvoidingView style={{padding: 30, flex: 1}} behavior="height">
                <View style={{flex: 2}}/>
                    <Text style={styles.title}>{strings('FinalSetupScreen.screenTitle')}</Text>
                    <TouchableOpacity style={{backgroundColor: '#0AB4BE', marginTop: 20, alignSelf: 'center', width: 250, justifyContent: 'center', borderRadius: 40, height: 40}} onPressOut={this.toProfile} disabled={this.state.loading}>
                        {
                            this.state.loading ? <DotIndicator count={3} color="white" size={8}/> : <Text style={{fontSize: 15, color: 'white', alignSelf: 'center'}}>{strings('FinalSetupScreen.next')}</Text>
                        }
                    </TouchableOpacity>
                    <View style={{flex: 3}}/>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white', 
        alignItems: 'center'
    },
    title: {
        fontSize: 32,
        fontWeight: 'bold'
    },
})

export default connect(state => ({id: state.id}))(FinalSetup)