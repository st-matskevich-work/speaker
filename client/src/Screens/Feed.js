import React from 'react'
import {NavigationEvents} from 'react-navigation'
import {View, Text, TouchableOpacity, Iamge, ScrollView, Image, RefreshControl, FlatList} from 'react-native'
import SafeAreaView from 'react-native-safe-area-view'
import {connect} from 'react-redux'
import * as Constants from '../Constants/Constants'
import * as SpeaksAPI from '../API/Speaks'
import SpeakBlock from '../Components/SpeakBlock'
import * as Actions from '../Actions/Actions'
import { strings } from '../../translation/i18n'

class Feed extends React.Component{
    state = {
        speaks: [],
        refreshing: false,
        player: "",
        currentRef: {},
        previousRef: {}
    }

    onRefresh = () => {
        this.setState({refreshing: true})
        this.refreshLogic()
    }

    refreshLogic = () => {
        //console.log('REFRESHING')
        SpeaksAPI.getFeed(this.props.id)
        .then(res => {
            //console.log(res)
            if(res.statusCode === 200){
                this.props.dispatch(Actions.setFeed(res.feed))
                //this.setState({speaks: res.speaks.reverse()})
                //this.setState({refreshing: false})
            }
        })
        .catch(err => console.log(err))
        .finally(() => {
            this.setState({refreshing: false})
            this.forceUpdate()
        })
    }

    render(){
        return(
            <SafeAreaView style={{flexGrow: 1, backgroundColor: 'white'}}>
                <NavigationEvents onDidFocus={this.refreshLogic}/>
                <View style={{height: 60, backgroundColor: 'white', elevation: 1, flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={{uri: Constants.SERVER_ADDRESS + this.props.user.image}} style={{height: 36, width: 36, borderRadius: 25, marginLeft: 15, backgroundColor: '#f6f7f8'}}/>
                    <View style={{flex: 1}}/>
                    <Image source={require('../Images/Logo1.png')} style={{height: 36, resizeMode: 'center', width: 36}}/>
                    <View style={{flex: 1}}/>
                    <TouchableOpacity onPressOut={() => this.props.navigation.openDrawer()}>
                        <Image source={require('../Images/menu.png')} style={{height: 25, width: 25, marginRight: 15, marginLeft: 11}}/>
                    </TouchableOpacity>
                </View>
                {
                    this.props.feed.length ? 
                    <FlatList style={{flex: 1}}
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this.onRefresh}
                                    colors={['#0AB4BE']}
                                 />
                              }
                            data={this.props.feed}
                            keyExtractor={item => {
                                //console.log(item.id)
                                return item.id
                            }}
                            extraData={this.props}
                            renderItem={({item}) => {
                                //console.log(item)
                                return <SpeakBlock 
                                                syncronize={(ref) => {
                                                    //console.log('REF', ref)
                                                    this.state.previousRef = this.state.currentRef
                                                    this.state.currentRef = ref
                                                    this.forceUpdate()
                                                }}
                                                player={this.state.player}
                                                setNewPlayer={(player) => {
                                                    //console.log(this.state.currentRef, '\n\n\n\n\n\n\n\n\n\n')
                                                    //this.props.player?.pause()
                                                    if(this.state.previousRef.resetProgress) this.state.previousRef.resetProgress()
                                                    this.state.player = player
                                                    this.forceUpdate()
                                                }}
                                                navigation={this.props.navigation} 
                                                speak={item} 
                                                setLikeMark={id => {
                                                    this.props.dispatch(Actions.setFeedLikemark(id))
                                                }}/>
                            }}/> : 
                    (
                        <View style={{flex: 1, alignItems: 'center', marginTop: 10}}>
                            <Text style={{color: '#0AB4BE', fontWeight: 'bold', fontSize: 18}}>{strings('FeedScreen.zeroPostsHeader')}</Text>
                            <Text style={{textAlign: 'center', marginTop: 5, color: '#0AB4BE'}}>{strings('FeedScreen.zeroPostsBody')}</Text>
                        </View>
                    )
                }
                <TouchableOpacity style={{height: 60, width: 60, backgroundColor: '#0AB4BE', position: 'absolute', bottom: 10, borderRadius: 35, right: 10, justifyContent: 'center', alignItems: 'center'}}
                                    onPressOut={() => {
                                        this.props.navigation.navigate('CreateNewSpeak')
                                    }}>
                        <Image source={require('../Images/button_speak.png')} style={{height: 60, width: 60, alignSelf: 'center'}}/>
                </TouchableOpacity>
            </SafeAreaView>
        )
    }
}

export default connect(state => ({user: state.user, id: state.id, feed: state.feed}))(Feed)

/*
<ScrollView scrollEnabled={true} style={{flex: 1}} refreshControl={
                    <RefreshControl colors={['#0AB4BE']} refreshing={this.state.refreshing} onRefresh={this.onRefresh}/>
                }>
                {
                    this.state.speaks.length ? this.state.speaks.map((value, index) => {
                        console.log(value.id, value.liked)
                        return <SpeakBlock key={value.id} speak={{
                            ...value
                        }}/>
                    }) :  
                    (
                        <View style={{flex: 1, alignItems: 'center', marginTop: 10}}>
                            <Text style={{color: '#0AB4BE', fontWeight: 'bold', fontSize: 18}}>Вы пока ничего не спикнули</Text>
                            <Text style={{textAlign: 'center', marginTop: 5, color: '#0AB4BE'}}>Когда вы опубликуете спик,{'\n'}он будет отображаться здесь</Text>
                        </View>
                    )
                }
                </ScrollView>
=======
import React from 'react'
import {NavigationEvents} from 'react-navigation'
import {View, Text, TouchableOpacity, Iamge, ScrollView, Image, RefreshControl, FlatList} from 'react-native'
import SafeAreaView from 'react-native-safe-area-view'
import {connect} from 'react-redux'
import * as Constants from '../Constants/Constants'
import * as SpeaksAPI from '../API/Speaks'
import SpeakBlock from '../Components/SpeakBlock'
import * as Actions from '../Actions/Actions'

class Feed extends React.Component{
    state = {
        speaks: [],
        refreshing: false,
        player: "",
        currentRef: {},
        previousRef: {}
    }

    componentDidMount(){
        this.onRefresh()
    }

    onRefresh = () => {
        this.setState({refreshing: true})
        this.refreshLogic()
    }

    refreshLogic = () => {
        //console.log('REFRESHING')
        SpeaksAPI.getFeed(this.props.id)
        .then(res => {
            //console.log(res)
            if(res.statusCode === 200){
                this.props.dispatch(Actions.setFeed(res.feed))
                //this.setState({speaks: res.speaks.reverse()})
                //this.setState({refreshing: false})
            }
        })
        .catch(err => console.log(err))
        .finally(() => {
            this.setState({refreshing: false})
            this.forceUpdate()
        })
    }

    render(){
        return(
            <SafeAreaView style={{flexGrow: 1, backgroundColor: 'white'}}>
                <NavigationEvents onDidFocus={this.refreshLogic}/>
                <View style={{height: 60, backgroundColor: 'white', elevation: 1, flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={{uri: Constants.SERVER_ADDRESS + this.props.user.image}} style={{height: 36, width: 36, borderRadius: 25, marginLeft: 15}}/>
                    <View style={{flex: 1}}/>
                    <Image source={require('../Images/Logo1.png')} style={{height: 36, resizeMode: 'center', width: 36}}/>
                    <View style={{flex: 1}}/>
                    <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                        <Image source={require('../Images/menu.png')} style={{height: 25, width: 25, marginRight: 15, marginLeft: 11}}/>
                    </TouchableOpacity>
                </View>
                {
                    this.props.feed.length ? 
                    <FlatList style={{flex: 1}}
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this.onRefresh}
                                    colors={['#0AB4BE']}
                                 />
                              }
                            data={this.props.feed}
                            keyExtractor={item => {
                                //console.log(item.id)
                                return item.id
                            }}
                            extraData={this.props}
                            renderItem={({item}) => {
                                //console.log(item)
                                return <SpeakBlock 
                                                syncronize={(ref) => {
                                                    //console.log('REF', ref)
                                                    this.state.previousRef = this.state.currentRef
                                                    this.state.currentRef = ref
                                                    this.forceUpdate()
                                                }}
                                                player={this.state.player}
                                                setNewPlayer={(player) => {
                                                    //console.log(this.state.currentRef, '\n\n\n\n\n\n\n\n\n\n')
                                                    //this.props.player?.pause()
                                                    if(this.state.previousRef.resetProgress) this.state.previousRef.resetProgress()
                                                    this.state.player = player
                                                    this.forceUpdate()
                                                }}
                                                navigation={this.props.navigation} 
                                                speak={item} 
                                                setLikeMark={id => {
                                                    this.props.dispatch(Actions.setFeedLikemark(id))
                                                }}/>
                            }}/> : 
                    (
                        <View style={{flex: 1, alignItems: 'center', marginTop: 10}}>
                            <Text style={{color: '#0AB4BE', fontWeight: 'bold', fontSize: 18}}>Вы пока ничего не спикнули</Text>
                            <Text style={{textAlign: 'center', marginTop: 5, color: '#0AB4BE'}}>Когда вы опубликуете спик,{'\n'}он будет отображаться здесь</Text>
                        </View>
                    )
                }
                <TouchableOpacity style={{height: 60, width: 60, backgroundColor: '#0AB4BE', position: 'absolute', bottom: 10, borderRadius: 35, right: 10, justifyContent: 'center', alignItems: 'center'}}
                                    onPress={() => {
                                        this.props.navigation.navigate('CreateNewSpeak')
                                    }}>
                        <Image source={require('../Images/button_speak.png')} style={{height: 60, width: 60, alignSelf: 'center'}}/>
                </TouchableOpacity>
            </SafeAreaView>
        )
    }
}

export default connect(state => ({user: state.user, id: state.id, feed: state.feed}))(Feed)

/*
<ScrollView scrollEnabled={true} style={{flex: 1}} refreshControl={
                    <RefreshControl colors={['#0AB4BE']} refreshing={this.state.refreshing} onRefresh={this.onRefresh}/>
                }>
                {
                    this.state.speaks.length ? this.state.speaks.map((value, index) => {
                        console.log(value.id, value.liked)
                        return <SpeakBlock key={value.id} speak={{
                            ...value
                        }}/>
                    }) :  
                    (
                        <View style={{flex: 1, alignItems: 'center', marginTop: 10}}>
                            <Text style={{color: '#0AB4BE', fontWeight: 'bold', fontSize: 18}}>Вы пока ничего не спикнули</Text>
                            <Text style={{textAlign: 'center', marginTop: 5, color: '#0AB4BE'}}>Когда вы опубликуете спик,{'\n'}он будет отображаться здесь</Text>
                        </View>
                    )
                }
                </ScrollView>
>>>>>>> 63f034d080f31b1c0bb0ef0be3bed266eaa0bc28
*/