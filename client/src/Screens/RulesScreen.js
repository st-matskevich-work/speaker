import React from 'react'
import {View, Text, StatusBar, ScrollView} from 'react-native'
import SafeAreaView from 'react-native-safe-area-view'
import CustomHeader from '../Components/CustomHeader'
import { strings } from '../../translation/i18n'

export default class Rules extends React.Component{
    render(){
        StatusBar.setBarStyle('dark-content')
        return(
            <SafeAreaView style={{flex: 1}}>
                <CustomHeader title={strings('RulesScreen.header')} navigation={this.props.navigation}/>
                <ScrollView style={{flex: 1, padding: 15}}>
                    <Text style={{color: '#0AB4BE', fontSize: 20}}>{strings('RulesScreen.screenTitle')}</Text>
                    <Text style={{fontSize: 15, marginTop: 5, marginBottom: 20}}>{strings('RulesScreen.rules')}</Text>
                </ScrollView>
            </SafeAreaView>
        )
    }
}