import React from 'react'
import {View, KeyboardAvaidingView, Text, Image, TouchableOpacity, Keyboard, KeyboardAvoidingView, Vibration} from 'react-native'
import { connect } from 'react-redux'
import SafeAreaView from 'react-native-safe-area-view'
import * as Constants from '../Constants/Constants'
import { ScrollView } from 'react-native-gesture-handler'
import CustomInput from '../Components/CustomInput'
import * as Actions from '../Actions/Actions'
import * as SpeaksAPI from '../API/Speaks'
import {DotIndicator} from 'react-native-indicators';
import RNSoundLevel from 'react-native-sound-level'
import VoiceRecorder from '../Components/VoiceRecorderButton'
import VoiceTile from '../Components/VoiceTile'
import { Player } from '@react-native-community/audio-toolkit'
import AudioSyncBlock from '../Components/AudioSyncBlock'
import ImageViewer from '../Components/ImageViewerComponent'
import ImagePicker from 'react-native-image-crop-picker';
import {check, PERMISSIONS, RESULTS, request} from 'react-native-permissions';
import {NavigationEvents} from 'react-navigation'
import { strings } from '../../translation/i18n'

class CreateNewSpeak extends React.Component{
    state = {
        text: "",
        voice: false,
        uploading: false,
        voiceAttachments: [],
        images: [],
        player: ""
    }

    addVoice = (audio) => {
        this.state.voiceAttachments.push(audio)
        this.forceUpdate()
    }

    removeVoice = (i) => {
        /*this.state.voiceRefs.splice(i, 1)[0].deleteProcess(() => {
            this.state.voiceAttachments.splice(i, 1)
            this.forceUpdate() 
        });*/
        this.state.voiceAttachments.splice(i, 1)
        this.forceUpdate()
    }

    addSpeak = () => {
        if(!this.state.text.trim().length && !this.state.voiceAttachments.length && !this.state.images.length)
            if(!this.props.navigation.state.params?.id)
                return

        let base64Attach = this.state.voiceAttachments.map((value, i) => {
            return {
                data: value.base64,
                type: 'wav'
            }
        })

        this.state.images.map((value, i) => {
            base64Attach.push({
                data: value.props.base64,
                type: value.props.extension
            })
        })
        
        this.setState({uploading: true})

        SpeaksAPI.addAttachments(base64Attach, this.props.id, this.props.token) //Upload attachments
        .then(res => {
            if(res.statusCode === 200){ //If all OK
                //console.log(res);
                //console.log(res.id, '\n\n\n\n\n\n\n\n\n\n')
                SpeaksAPI.addSpeak(this.props.id, this.props.token, { //Uplaod speak
                    text: this.state.text.trim(), //text
                    attachments: res.id, //Array of attachmets id (response from previous request)
                    respeaked: this.props.navigation.state.params?.id ? this.props.navigation.state.params?.id : 0
                })
                .then(res => {
                    if(res.statusCode === 200){ //if add speak OK
                        this.props.navigation.goBack()
                        this.props.navigation.state.params?.parentUpdate ? this.props.navigation.state.params?.parentUpdate() : null //Go back
                        this.setState({uploading: false})
                    }else{ //if add speak failed
                        console.log(res) 
                    }
                })
                .catch(err => { //if add speak failed
                    console.log(err)
                })
            }
        })
        .catch(err => { //if upload attachments failed
            console.log(err)
        })  
        .finally(() => {
            this.setState({uploading: false})
        })
    }

    uploadFromGallery = () => {
        ImagePicker.openPicker({
            cropping: true,
            includeBase64: true
        }).catch(error => console.log(error))
        .then(photo => {
            //console.log(photo.mime)
            this.state.images.push({
                url: photo.path,
                props: {
                    base64: photo.data,
                    extension: photo.mime.split('/')[1]
                }
            })
            this.forceUpdate()
        })
        .catch(error => console.log(error))
    }

    uploadFromCamera = () => {
        check(PERMISSIONS.ANDROID.CAMERA)
        .then(result => {
          switch(result){
            case RESULTS.UNAVAILABLE:
            console.log(
              'This feature is not available (on this device / in this context)',
            );
            break;
          case RESULTS.DENIED:
          {
            request(PERMISSIONS.ANDROID.RECORD_AUDIO)
            .then(result => {
                if(result == 'granted'){
                    ImagePicker.openCamera({
                        cropping: true,
                        includeBase64: true
                    }).catch(error => console.log(error))
                    .then(photo => {
                        //console.log(photo.mime)
                        this.state.images.push({
                            url: photo.path,
                            props: {
                                base64: photo.data,
                                extension: photo.mime.split('/')[1]
                            }
                        })
                        this.forceUpdate()
                    })
                    .catch(error => console.log(error))
                }
            })
            break;
          }
          case RESULTS.GRANTED:
          {
            ImagePicker.openCamera({
                cropping: true,
                includeBase64: true
            }).catch(error => console.log(error))
            .then(photo => {
                //console.log(photo.mime)
                this.state.images.push({
                    url: photo.path,
                    props: {
                        base64: photo.data,
                        extension: photo.mime.split('/')[1]
                    }
                })
                this.forceUpdate()
            })
            .catch(error => console.log(error)) 
          }
          case RESULTS.BLOCKED:
            console.log('The permission is denied and not requestable anymore');
            break;
          }
        })
    }
    

    render(){
        return(
            <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
                <NavigationEvents onWillFocus={() => Keyboard.dismiss()} onWillBlur={() => Keyboard.dismiss()}/>
                <View style={{height: 60, alignItems: 'center', flexDirection: 'row'}}>
                    <TouchableOpacity style={{height: 60, width: 60, justifyContent: 'center', alignItems: 'center'}} onPressOut={() => this.props.navigation.goBack()}>
                        <Image source={require('../Images/close.png')} style={{height: 14, width: 14, resizeMode: 'contain'}}/>
                    </TouchableOpacity>
                    <View style={{flex: 1}}/>
                    <TouchableOpacity style={{marginRight: 20, backgroundColor: '#0AB4BE', borderRadius: 50, width: 110, height: 32, justifyContent: 'center', alignItems: 'center'}} onPressOut={this.addSpeak}>
                        {
                            this.state.uploading ? <DotIndicator count={3} color="white" size={8}/> : <Text style={{color: 'white', fontSize: 15, paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15}}>{strings('CreateNewSpeakScreen.speak')}</Text>
                        }
                    </TouchableOpacity>
                </View>
                <KeyboardAvoidingView style={{flex: 1}}>
                    <ScrollView contentContainerStyle={{padding: 12}}>
                        {
                            this.props.navigation.state.params?.id ? (
                                <Text style={{marginBottom: 5, color: '#979797', marginLeft: 65}}>{strings('CreateNewSpeakScreen.respeakFrom', {name: this.props.navigation.state.params.name})}</Text>
                            ) : null
                        }
                        <View style={{flex: 1, flexDirection: 'row'}}>
                            <Image source={{uri: Constants.SERVER_ADDRESS + this.props.user.image}} style={{height: 50, width: 50, borderRadius: 50, backgroundColor: '#f6f7f8'}}/>
                            <CustomInput disableBorder={true} maxLength={1024} blurOnSubmit={false} placeholder={strings('CreateNewSpeakScreen.placeholder')} type="none" onChangeText={(text) => this.setState({text: text})} multiline={true} style={{flex: 1, marginLeft: 12, borderBottomWidth: 0}}/>
                        </View>
                        <ImageViewer 
                                    images={this.state.images}
                                    deleteImage={(index) => {
                                        this.state.images.splice(index, 1)
                                        this.forceUpdate()
                                    }}/>
                        <AudioSyncBlock
                                        voiceAttachments={this.state.voiceAttachments}
                                        player={this.state.player}
                                        setNewPlayer={(player) => {
                                            this.setState({player: player})
                                        }}
                                        removeVoice={this.removeVoice}
                                        allowDeleting={true}
                        />
                        {
                            this.props.navigation.state.params?.speak ? (
                                <TouchableOpacity style={{width: '100%', backgroundColor: 'white', marginBottom: 5, maxHeight: 100, borderColor: '#979797', borderRadius: 8, borderWidth: 0.3, padding: 7}}>
                                    <View style={{width: '100%', flexDirection: 'row'}}>
                                        <TouchableOpacity>
                                            <Image source={{uri: Constants.SERVER_ADDRESS + this.props.navigation.state.params.speak.user.avatar}} style={{height: 40, width: 40, borderRadius: 30, backgroundColor: '#f6f7f8'}}/>
                                        </TouchableOpacity>
                                        <View style={{flex: 1, marginLeft: 10}}>
                                            <Text style={{fontSize: 16}}>{this.props.navigation.state.params.speak.user.name}</Text>
                                            <Text style={{color: '#979797', fontSize: 14}}>@{this.props.navigation.state.params.speak.user.tag}</Text>
                                        </View>
                                    </View>
                                    <Text style={{marginTop: 5}}>{this.props.navigation.state.params.speak.speak}</Text>
                                </TouchableOpacity>
                            ) : null
                        }
                    </ScrollView>
                    <View style={{height: 60, flexDirection: 'row', alignItems: 'center', borderTopWidth: 1, borderTopColor: '#EDEDED', overflow: 'visible'}}>
                        <VoiceRecorder onRecorded={this.addVoice} style={{flex: 1, marginLeft: 0, marginRight: 0}}/>
                        <TouchableOpacity style={{zIndex: 1, width: 60, alignItems: 'center', flex: 1}} onPressOut={this.uploadFromGallery}>
                            <Image source={require('../Images/gallery.png')} style={{height: '100%', width: 28, resizeMode: 'contain'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={{width: 60, alignItems: 'center', flex: 1}} onPressOut={this.uploadFromCamera}>
                            <Image source={require('../Images/photo.png')} style={{height: '100%', width: 30, resizeMode: 'contain'}}/>
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

export default connect(state => ({user: state.user, token: state.token, id: state.id}))(CreateNewSpeak)