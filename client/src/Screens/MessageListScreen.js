import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import { View, Text, StyleSheet, Button, Image, TouchableOpacity, TextInput, RefreshControl, FlatList } from 'react-native'
import { connect } from 'react-redux'
import * as Constants from '../Constants/Constants'
import * as Actions from '../Actions/Actions'
import { NavigationEvents } from 'react-navigation'
import * as SpeaksAPI from '../API/Speaks'
import { ScrollView } from 'react-native-gesture-handler'
import MessageTile from '../Components/MessageTile'
import * as UsersAPI from '../API/Users'
import moment from 'moment'
import 'moment/locale/ru'
import { strings } from '../../translation/i18n'
import { string } from 'react-native-redash'

class Messages extends React.Component {
    state = {
        searchUsers: [],
        refreshing: false,
        messages: [],
        renderedId: [],
        holdCurrentStatus: null
    }

    componentDidMount() {
        this.props.screenProps.addUpdateRef(this)
        console.log('REF WAS ADDED')
    }

    messageReceived = (params, inChat) => {
        console.log('Meh')
        //this.onRefresh()
        this.props.dispatch(Actions.skipAndTrigger(params))
        this.forceUpdate()
    }

    dontUpdateChatStatus = (id) => {
        console.log('DONT UPDATING CHAT WITH ID', id)
        this.setState({holdCurrentStatus: id})
    }

    onRefresh = (refresh = true) => {
        this.setState({ refreshing: refresh })

        SpeaksAPI.getMessages(this.props.id, this.props.token)
            .then(res => {
                if (res.statusCode === 200) {
                    console.log('DISPATCHING MESSAGES WITH BLOCKED ID', this.state.holdCurrentStatus)
                    //console.log(res.messages[51].messages)
                    this.props.dispatch(Actions.setMessages(res.messages, this.state.holdCurrentStatus))
                    this.state.messages = res.messages
                    console.log(this.state.messages)
                    this.forceUpdate()
                }
            })
            .catch(err => {

            })
            .finally(() => {
                this.setState({ refreshing: false })
                this.setState({holdCurrentStatus: null})
            })


        //this.forceUpdate()
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <NavigationEvents onWillFocus={() => {
                    this.onRefresh(false)
                }} />
                <View style={{ height: 60, backgroundColor: 'white', elevation: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <Image source={{ uri: Constants.SERVER_ADDRESS + this.props.user.image }} style={{ height: 36, width: 36, borderRadius: 25, marginLeft: 15, marginRight: 15, backgroundColor: '#f6f7f8' }} />
                    <Text style={{ fontSize: 18 }}>{strings('MessageListScreen.header')}</Text>
                </View>
                <ScrollView style={{ backgroundColor: 'white', flexGrow: 1 }} refreshControl={
                    <RefreshControl colors={['#0AB4BE']} refreshing={this.state.refreshing} onRefresh={this.onRefresh} />
                }>
                    {
                        this.props.messages.map((value, i) => {
                            //console.log(value?.messages.length, 'LOL')
                            if (value?.messages.length) {
                                //console.log(value, 'LOL')
                                //console.log(this.state.renderedId.indexOf(value.messages[0]._id))
                                //if(this.state.renderedId.indexOf(value.messages[0]._id) != -1) return
                                this.state.renderedId.push(value.messages[0]._id)
                                console.log(value.messages[0].read, 'readed')
                                //console.log(value.messages)
                                return <MessageTile isRead={value.messages[0].read} navigation={this.props.navigation}
                                    user={value.user}
                                    disableStatusUpdate={this.dontUpdateChatStatus}
                                    key={value.messages[0]._id}
                                    lastMessage={value.messages[0].text.length ? value.messages[0].text : "Изображение"}
                                    lastSpeaker={value.messages[0].user._id}
                                    time={moment(value.messages[0].createdAt).locale('ru').format("HH:mm")} />
                            }
                        })
                    }
                </ScrollView>
                <TouchableOpacity style={{ height: 60, width: 60, backgroundColor: '#0AB4BE', position: 'absolute', bottom: 10, borderRadius: 35, right: 10, justifyContent: 'center', alignItems: 'center' }}
                    onPressOut={() => {
                        this.props.navigation.navigate('AddNewChat')
                    }}>
                    <Image source={require('../Images/newChat.png')} style={{ height: 40, width: 40, alignSelf: 'center' }} />
                </TouchableOpacity>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    }
})

export default connect(state => ({ user: state.user, messages: state.messages, id: state.id, token: state.token }))(Messages)

/*
<ScrollView style={{backgroundColor: 'white', flexGrow: 1}} refreshControl={
                    <RefreshControl colors={['#0AB4BE']} refreshing={this.state.refreshing} onRefresh={this.onRefresh}/>
                }>
                    {
                        this.state.searchUsers.length ? (
                            this.state.searchUsers.map((value, i) => {
                                return (
                                    <MessageTile navigation={this.props.navigation} user={value} key={i} lastMessage={this.props.messages["" + value.id] ? this.props.messages["" + value.id].messages[0].text : "Пока пусто..."}/>
                                )
                            })
                        ) : this.props.messages.map((value, i) => {
                            //console.log('Rendering....', i)
                            if(value){
                                console.log(value.messages)
                                return <MessageTile navigation={this.props.navigation} user={value.user} key={i} lastMessage={value.messages[0].text}/>
                            }
                        })
                    }
                </ScrollView>
*/