import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import {View, Text, StyleSheet, TouchableOpacity, Image, Button, KeyboardAvoidingView, ScrollView} from 'react-native'
import { connect } from 'react-redux'
import CustomInput from '../../Components/CustomInput'
import { NavigationEvents } from 'react-navigation'
import * as Actions from '../../Actions/Actions'
import {DotIndicator} from 'react-native-indicators';
import * as AuthAPI from '../../API/Auth'
import { showMessage, hideMessage } from "react-native-flash-message";
import { strings } from '../../../translation/i18n'
import {MessageBar as MessageBarAlert, MessageBarManager} from 'react-native-message-bar'

class Register extends React.Component{
    static navigationOptions = ({navigation}) => {
        return {
            headerTitleAlign: 'center',
            headerTitle: () => (
                <Image source={require('../../Images/Logo1.png')} style={{height: 30, width: 30, resizeMode: 'contain'}}/>
            ),
            headerLeft: () => (
                <TouchableOpacity style={{marginLeft: 20}} onPressOut={() => navigation.navigate('LoginScreen')}>
                    <Image source={require('../../Images/back.png')} style={{height: '100%', width: 16, resizeMode: 'contain'}}/>
                </TouchableOpacity>
            ),
            headerStyle: {
                elevation: 0, //for android
                shadowOpacity: 0, //for ios
                borderBottomWidth: 0.3, //for i
                borderBottomColor: 'rgba(151,151,151,0.5)'
            }
        }
    };

    state = {
        name: "",
        email: "",
        password: "",
        isLoading: false,
        errorText: ""
    }

    componentDidMount(){
        //MessageBarManager.registerMessageBar(this.refs.alert);
    }

    componentWillUnmount(){
        //MessageBarManager.unregisterMessageBar();
    }

    createAccount = async () => {
        //console.log(this.emailInput.getFieldState(), '\n\n\n\n')        

        if(!this.nameInput.getFieldState() || !this.passwordField.getFieldState() || !this.emailInput.getFieldState()){
            //alert('here')
            return;
        }

        this.setState({isLoading: true})

        AuthAPI.register(this.state.name.trim(), this.state.email, this.state.password, this.emailInput.state.isPhone)
        .then(res => {
            if(res.statusCode === 200){
                console.log(res)
                this.props.dispatch(Actions.setEmail(this.state.email))
                this.props.dispatch(Actions.setPassword(this.state.password))
                this.props.dispatch(Actions.setID(res.id))
                this.props.navigation.navigate('ConfirmRegistrationScreen')
                this.setState({isLoading: false})
            }else{
                /*MessageBarManager.showAlert({
                    title: 'Ошибка',
                    message: res.status,
                    alertType: 'error',
                    position: 'bottom',
                });*/
                /*showMessage({
                    message: 'Ошибка',
                    description: res.status,
                    type: "danger",
                  });*/
                this.setState({errorText: res.status})
                this.setState({isLoading: false})
            }
        })
        .catch(err => {
            /*MessageBarManager.showAlert({
                title: 'Ошибка',
                message: err,
                alertType: 'error',
                position: 'bottom',
            });*/
            /*showMessage({
                message: 'Ошибка',
                description: err,
                type: "danger",
              });*/
            this.setState({errorText: err})
            this.setState({isLoading: false})
        })
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                <NavigationEvents onWillBlur={() => this.setState({errorText: ""})}/>
                <KeyboardAvoidingView style={{flex: 1}}>
                    <ScrollView contentContainerStyle={{padding: 30}}>
                        <Text style={styles.title}>{strings('RegisterScreen.screenTitle')}</Text>
                        <CustomInput onRef={ref => this.nameInput = ref} regex={/^[а-яёА-ЯЁ\w\s\(\)\[\]\-\.\\\/\#\&\*\@]+$/} placeholder={strings('RegisterScreen.namePlaceholder')} type="name" onChangeText={(text) => this.setState({name: text, errorText: ""})}/>
                        <CustomInput checkAvailability={true} onRef={ref => this.emailInput = ref} regex={/(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/} phoneRegex={/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/} disableSpacing={true} placeholder={strings('RegisterScreen.emailPlaceholder')} type="emailAddress" onChangeText={(text) => this.setState({email: text, errorText: ""})}/>
                        <CustomInput onRef={ref => this.passwordField = ref} regex={/^[\w\s\.\$\-\[\]\{\}\(\)\<\>\!\?\\\/\:\;\`\@\#\%\^\&\*\+\~\'\"]+$/} disableSpacing={true} placeholder={strings('RegisterScreen.passwordPlaceholder')} type="password" onChangeText={(text) => this.setState({password: text, errorText: ""})}/>
                        {
                            this.state.errorText ? <Text style={{fontSize: 16, color: 'red'}}>{this.state.errorText}</Text> : null
                        }
                            <Text style={{fontSize: 15}}>{strings('RegisterScreen.alert1')}{'\n'}
                            <Text style={{color: '#0AB4BE', fontSize: 15}}>{strings('RegisterScreen.terms')}</Text>{strings('RegisterScreen.alert2')}
                            <Text style={{color: '#0AB4BE', fontSize: 15}}>{strings('RegisterScreen.privacy')}</Text>{strings('RegisterScreen.alert3')}{'\n'}
                            <Text style={{color: '#0AB4BE', fontSize: 15}}>{strings('RegisterScreen.cookie')}</Text></Text>
                    </ScrollView>
                    <View style={{flexGrow: 1}}/>
                    <View style={{flexDirection: 'row', height: 60, width: '100%', alignItems: 'center'}}>
                        <View style={{flex: 1}}/>
                        <TouchableOpacity style={{backgroundColor: '#0AB4BE',marginRight: 30,width: 100, justifyContent: 'center', borderRadius: 40, height: 40}} onPressOut={this.createAccount} disabled={this.state.isLoading}>
                            {
                                this.state.isLoading ? <DotIndicator count={3} color="white" size={8}/> : <Text style={{fontSize: 15, color: 'white', alignSelf: 'center'}}>{strings('RegisterScreen.login')}</Text>
                            }
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
    },
    title: {
        fontSize: 32,
        marginBottom: 30,
        fontWeight: 'bold'
    },
})

export default connect(state => ({email: state.email, password: state.password, id: state.id}))(Register)