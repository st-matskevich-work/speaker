import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import {View, Text, StyleSheet, TouchableOpacity, Image, Button, KeyboardAvoidingView, Keyboard, TouchableWithoutFeedbackBase} from 'react-native'
import { connect } from 'react-redux'
import { NavigationEvents } from 'react-navigation'
import CustomInput from '../../Components/CustomInput'
import { showMessage, hideMessage } from "react-native-flash-message";
import { strings } from '../../../translation/i18n'

import {DotIndicator} from 'react-native-indicators';
import Modal, {
    ModalTitle,
    ModalContent,
    ModalFooter,
    ModalButton,
    SlideAnimation,
    ScaleAnimation,
  } from 'react-native-modals';
import * as Actions from '../../Actions/Actions'
import * as AuthAPI from '../../API/Auth'
import {MessageBar as MessageBarAlert, MessageBarManager} from 'react-native-message-bar'
import { ScrollView } from 'react-native-gesture-handler'

class ConfirmRegistration extends React.Component{
    static navigationOptions = ({navigation}) => {
        return {
            headerTitleAlign: 'center',
            headerTitle: () => (
                <Image source={require('../../Images/Logo1.png')} style={{height: 30, width: 30, resizeMode: 'contain'}}/>
            ),
            headerLeft: () => (
                <TouchableOpacity style={{marginLeft: 20}} onPressOut={() => navigation.goBack()}>
                    <Image source={require('../../Images/back.png')} style={{height: '100%', width: 16, resizeMode: 'contain'}}/>
                </TouchableOpacity>
            ),
            headerStyle: {
                elevation: 0, //for android
                shadowOpacity: 0, //for ios
                borderBottomWidth: 0.3, //for i
                borderBottomColor: 'rgba(151,151,151,0.5)'
            }
        }
    };

    state = {
        showPopUp: false,
        isLoading: false,
        code: "",
        locked: false,
        timeout: 0,
        errorText: ""
    }

    componentDidMount(){
        //MessageBarManager.registerMessageBar(this.refs.alert);
    }

    componentWillUnmount(){
        //MessageBarManager.unregisterMessageBar();
    }

    confirm = async () => {
        this.setState({isLoading: true})
        console.log(this.state.code)
        AuthAPI.confirmRegistration(this.props.id, this.state.code)
        .then(result => {
            if(result.statusCode === 200){
                this.props.dispatch(Actions.setToken(result.token))
                Actions.writeToken(result.token.toString())
                Actions.writeID(this.props.id.toString())
                this.props.screenProps.socketInit(this.props.id, result.token)
                this.props.navigation.navigate('SetupNewAccount')
            }else{
                /*MessageBarManager.showAlert({
                    title: 'Ошибка',
                    message: result.status,
                    alertType: 'error',
                    position: 'bottom',
                });*/
                /*showMessage({
                    message: 'Ошибка',
                    description: result.status,
                    type: "danger",
                  });*/
                this.setState({isLoading: false, errorText: result.status})
            }
        })
        .catch(err => {
            /*MessageBarManager.showAlert({
                title: 'Ошибка',
                message: err,
                alertType: 'error',
                position: 'bottom',
            });*/
            /*showMessage({
                message: 'Ошибка',
                description: err,
                type: "danger",
              });*/
            this.setState({isLoading: false, errorText: err})
        })
        /*let result = await LoginActions.login(JSON.stringify({
            email: this.props.login, 
            password: this.props.password
        }))
        if(result.statusCode === 200){
            console.log(result)
            this.props.dispatch(TokenActions.setToken(result.token))
            this.props.dispatch(IDActions.setID(result.id))
            this.props.navigation.navigate('SetupNewAccount')
        }
        else{
            MessageBarManager.showAlert({
                title: 'Error',
                message: result.status,
                alertType: 'error',
                position: 'bottom',
            });
            this.setState({isLoading: false})
        }*/
        //this.props.navigation.navigate('SetupNewAccount')
        //console.log(result)
    }

    //Lonely Astronaut
    //thelonelyastronaut1337@gmail.com
    //LoginHorizon

    render(){
        //console.log(this.props.login, this.props.password)
        return(
            <SafeAreaView style={styles.container}>
                <NavigationEvents onWillBlur={() => this.setState({errorText: ""})}/>
                <KeyboardAvoidingView style={{flex: 1}}>
                    <ScrollView contentContainerStyle={{padding: 30}}>
                        <Text style={styles.title}>{strings('ConfirmRegistrationScreen.screenTitle')}</Text>
                        <Text style={{fontSize: 15, marginBottom: 15}}>{strings('ConfirmRegistrationScreen.screenDescription')}</Text>
                        <CustomInput placeholder={strings('ConfirmRegistrationScreen.placeholder')} type="none" onChangeText={(text) => this.setState({code: text, errorText: ""})} disableSpacing={true}/>
                        {
                            this.state.errorText ? <Text style={{fontSize: 16, color: 'red'}}>{this.state.errorText}</Text> : null
                        }
                        <View style={{flexDirection: 'row'}}>
                        <Text style={{color: this.state.locked ? '#979797' : '#0AB4BE', fontSize: 15}} onPressOut={() => {
                            Keyboard.dismiss()
                            if(!this.state.locked){
                                console.log(this.props.email)
                                AuthAPI.resendActivationCode(this.props.email, this.props.email.match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/)?.length)
                                .catch((err) => {
                                    /*showMessage({
                                        message: 'Ошибка',
                                        description: err,
                                        type: "danger",
                                      });*/
                                    this.setState({errorText: err})
                            })
                            this.setState({locked: true, timeout: 30})
                            let interval = setInterval(() => {
                                this.setState({timeout: this.state.timeout - 1})
                            }, 1000)
                            setTimeout(() => {
                                this.setState({locked: false})
                                clearInterval(interval)
                            }, 31000)
                            }
                        }}>{strings('ConfirmRegistrationScreen.resend')}{!this.state.locked ? "" : strings('ConfirmRegistrationScreen.after') + this.state.timeout}</Text>
                        </View>
                    </ScrollView>
                    <View style={{flexGrow: 1}}/>
                    <View style={{flexDirection: 'row', height: 60, width: '100%', alignItems: 'center'}}>
                            <View style={{flex: 1}}/>
                            <TouchableOpacity style={{backgroundColor: '#0AB4BE',marginRight: 30,width: 100, justifyContent: 'center', borderRadius: 40, height: 40}} onPressOut={this.confirm} disabled={this.state.isLoading}>
                            {
                                this.state.isLoading ? <DotIndicator count={3} color="white" size={8}/> : <Text style={{fontSize: 15, color: 'white', alignSelf: 'center'}}>{strings('ConfirmRegistrationScreen.login')}</Text>
                            }
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
    },
    title: {
        fontSize: 32,
        marginBottom: 30,
        fontWeight: 'bold'
    },
})
export default connect(state => ({email: state.email, password: state.password, token: state.token, id: state.id}))(ConfirmRegistration)