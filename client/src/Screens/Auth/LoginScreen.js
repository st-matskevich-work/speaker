import React from 'react'
import {View, Text, StyleSheet, Button, TextInput, Image, ScrollView, KeyboardAvoidingView, TouchableOpacity, Keyboard, KeyboardAvoidingViewBase} from 'react-native'
import { connect } from 'react-redux'
import { NavigationEvents } from 'react-navigation';
import SafeAreaView from 'react-native-safe-area-view'
import CustomInput from '../../Components/CustomInput'
import * as Actions from '../../Actions/Actions'
import {MessageBar as MessageBarAlert, MessageBarManager} from 'react-native-message-bar'
import * as Constants from '../../Constants/Constants'
import {DotIndicator} from 'react-native-indicators';
import * as AuthAPI from '../../API/Auth'
import * as UsersAPI from '../../API/Users'
import { showMessage, hideMessage } from "react-native-flash-message";
import { strings } from '../../../translation/i18n'

class LoginScreen extends React.Component{
    static navigationOptions = ({navigation}) => {
        return {
            headerTitleAlign: 'center',
            headerTitle: () => (
                <Image source={require('../../Images/Logo1.png')} style={{height: 30, width: 30, resizeMode: 'contain'}}/>
            ),
            headerRight: () => (
                <TouchableOpacity style={{marginRight: 20}} onPressOut={() => navigation.navigate('RegisterScreen')}>
                    <Text style={{color: '#0AB4BE', fontSize: 15}}>{strings('LoginScreen.register')}</Text>
                </TouchableOpacity>
            ),
            headerStyle: {
                elevation: 0, //for android
                shadowOpacity: 0, //for ios
                borderBottomWidth: 0.3, //for i
                borderBottomColor: 'rgba(151,151,151,0.5)'
            }
        }
    };

    state = {
        email: "",
        password: "",
        isLoading: false,
        toAuth: true,
        height: 80,
        errorText: ""
    }

    componentDidMount(){
        console.log(this.props.token.length)
        if(this.props.token.length){
            console.log(this.props.user.setup)
            switch(this.props.user.setup){
                case 0:{
                    this.props.navigation.navigate('AddPhoto')
                    break
                }
                case 1:{
                    this.props.navigation.navigate('SetUsername')
                    break
                }
                case 2:{
                    this.props.navigation.navigate('ChooseHeader')
                    break
                }
                case 3:{
                    this.props.navigation.navigate('DescribeYourself')
                    break
                }
                case 4:{
                    this.props.navigation.navigate('YourLocation')
                    break
                }
                default:{
                    /*this.props.navigation.navigate('Profile', {
                        user: this.props.user
                    })*/
                    this.props.navigation.navigate('Feed')
                    break;
                }
            }   
        }

        /*if(this.props.token){
            LoginActions.login(JSON.stringify({
                token: this.props.token,
                id: this.props.id
            })).then(res => {
                responseStatus = res.status
                return res.json()
            }).then(res => {
                if(responseStatus === 200){
                    console.log(res)
                }else{
                    this.setState({toAuth: true})
                }
            })
            .catch((err) => console.log(err))
        }*/

        //MessageBarManager.registerMessageBar(this.refs.loginAlert);
        /*Keyboard.addListener('keyboardDidShow', () => {
            this.setState({height: 50})
        })
        Keyboard.addListener('keyboardDidHide', () => this.setState({height: 80}))*/
        
    }

    componentWillUnmount(){
        //MessageBarManager.unregisterMessageBar();
        //Keyboard.removeAllListeners('keyboardWillShow')
    }

    login = async () => {
        this.setState({isLoading: true})

        AuthAPI.loginByEmail(this.state.email, this.state.password)
        .then(res => {
            console.log(res)
            if(res.statusCode === 200){
                this.props.dispatch(Actions.setEmail(this.state.email))
                this.props.dispatch(Actions.setPassword(this.state.password))
                this.props.dispatch(Actions.setToken(res.token))
                this.props.dispatch(Actions.setID(res.id))
                Actions.writeToken(res.token.toString())
                Actions.writeID(res.id.toString())

                this.props.screenProps.socketInit(res.id, res.token)

                UsersAPI.getUserInfoById(res.id)
                .then(result => {
                    if(result.statusCode === 200){
                        /*let user = {
                            name: result.name,
                            email: result.email,
                            avatar: result.image,
                            header: result.headerImage,
                            tag: result.tag,
                            description: result.description,
                            city: result.city,
                            date: result.date,
                            id: this.props.id
                        }*/

                        this.props.dispatch(Actions.setUser(result))
                        
                        switch(res.setup){
                            case 0:{
                                this.props.navigation.navigate('AddPhoto')
                                break
                            }
                            case 1:{
                                this.props.navigation.navigate('SetUsername')
                                break
                            }
                            case 2:{
                                this.props.navigation.navigate('ChooseHeader')
                                break
                            }
                            case 3:{
                                this.props.navigation.navigate('DescribeYourself')
                                break
                            }
                            case 4:{
                                this.props.navigation.navigate('YourLocation')
                                break
                            }
                            default:{
                                this.props.navigation.navigate('Feed')
                                break;
                            }
                        }
                    }else{
                        /*showMessage({
                            message: 'Ошибка',
                            description: res.status,
                            type: "danger",
                          });*/
                        console.log('HUUUUUUUUUUUUUUH')
                        this.setState({errorText: res.status})
                        this.setState({isLoading: false})
                    }
                }).catch(err => {
                    /*showMessage({
                        message: 'Ошибка',
                        description: err,
                        type: "danger",
                      });*/
                    this.setState({errorText: err})  
                    this.setState({isLoading: false})
                })
            }else if(res.statusCode === 408){
                //console.log(res)
                this.props.dispatch(Actions.setID(res.id))
                this.props.dispatch(Actions.setEmail(this.state.email))

                AuthAPI.resendActivationCode(this.props.email, this.props.email.match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/)?.length)
                .catch((err) => {
                    /*showMessage({
                        message: 'Ошибка',
                        description: err,
                        type: "danger",
                    });*/
                    this.setState({errorText: err})
                })
                this.props.navigation.navigate('ConfirmRegistrationScreen')
                this.setState({isLoading: false})
            }
            else{
                /*showMessage({
                    message: 'Ошибка',
                    description: res.status,
                    type: "danger",
                  });*/
                this.setState({errorText: res.status})
                this.setState({isLoading: false})
            }
        }).catch(err => {
            /*showMessage({
                message: 'Ошибка',
                description: err,
                type: "danger",
              });*/
            this.setState({errorText: err})
            this.setState({isLoading: false})
        })
    }

    render(){
        return(
            this.state.toAuth ? (
                <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
                    <NavigationEvents
                                    onWillFocus={() => MessageBarManager.registerMessageBar(this.refs.loginAlert)}
                                    onDidBlur={() => this.setState({errorText: ""})}
                    />
                    <KeyboardAvoidingView style={{flex: 1}}>
                        <ScrollView contentContainerStyle={{padding: 30}}>
                            <Text style={styles.title}>{strings('LoginScreen.screenTitle')}</Text>
                            <Text style={styles.description}>{strings('LoginScreen.screenDescription')}</Text>
                            <CustomInput disableSpacing={true} placeholder={strings('LoginScreen.loginPlaceholder')} type="emailAddress" onChangeText={(text) => this.setState({email: text, errorText: ""})}/>
                            <CustomInput disableSpacing={true} placeholder={strings('LoginScreen.passwordPlaceholder')} type="password" onChangeText={(text) => this.setState({password: text, errorText: ""})}/>
                            {
                                this.state.errorText ? <Text style={{fontSize: 16, color: 'red', marginBottom: 5}}>{this.state.errorText}</Text> : null
                            }
                            <TouchableOpacity style={{alignSelf: 'center'}} onPressOut={() => this.props.navigation.navigate('InputEmail')}>
                                <Text style={{color: 'grey', fontSize: 15}}>{strings('LoginScreen.forgotPassword')}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{alignSelf: 'center', marginTop: 10}} onPressOut={() => this.props.navigation.navigate('ChangeLanguageAuth')}>
                                <Text style={{color: 'grey', fontSize: 15}}>{strings('SideBar.language')}</Text>
                            </TouchableOpacity>
                        </ScrollView>
                        <View style={{flexGrow: 1}}/>
                        <View style={{flexDirection: 'row', height: 60, width: '100%', alignItems: 'center'}}>
                            <View style={{flex: 1}}/>
                            <TouchableOpacity style={{backgroundColor: '#0AB4BE',marginRight: 30,width: 100, justifyContent: 'center', borderRadius: 40, height: 40}} onPressOut={() => {
                                //setLocale();
                                //this.forceUpdate()
                                this.login()
                            }} disabled={this.state.isLoading}>
                            {
                                this.state.isLoading ? <DotIndicator count={3} color="white" size={8}/> : <Text style={{fontSize: 15, color: 'white', alignSelf: 'center'}}>{strings('LoginScreen.login')}</Text>
                            }
                            </TouchableOpacity>
                        </View>
                    </KeyboardAvoidingView>
            </SafeAreaView>
            ) : null
        )
    }
}

const styles = StyleSheet.create({
    textInput:{
        borderBottomColor: 'blue',
        borderBottomWidth: 3
    },
    title: {
        fontSize: 32,
        marginBottom: 30,
        fontWeight: 'bold'
    },
    description:{
        fontSize: 15,
        marginBottom: 30
    }
})

export default connect(state => ({email: state.email, password: state.password, token: state.token, id: state.id, user: state.user}))(LoginScreen)
