import React from 'react'
import {View, Text, StatusBar, ScrollView, TouchableOpacity, StyleSheet, Image} from 'react-native'
import SafeAreaView from 'react-native-safe-area-view'
import CustomHeader from '../Components/CustomHeader'
import { strings } from '../../translation/i18n'
import I18n from 'react-native-i18n';

export default class ChangeLanguageScreen extends React.Component{
    render(){
        StatusBar.setBarStyle('dark-content')
        return(
            <SafeAreaView style={{flex: 1}}>
                <CustomHeader title={strings('ChangeLanguageScreen.header')} navigation={this.props.navigation}/>
                <ScrollView style={{flex: 1, padding: 15}}>
                    <Text style={{color: '#0AB4BE', fontSize: 20}}>{strings('ChangeLanguageScreen.screenTitle')}</Text>
                    <TouchableOpacity onPressOut={() => this.props.screenProps.updateLocale("ru")} style={styles.container}>
                        <Image source={require("../Images/russian.png")} style={{marginRight: 20, height: 25, width: 25, resizeMode: 'contain'}}/>
                        <Text style={{fontSize: 16, fontWeight: I18n.currentLocale() == "ru" ? 'bold' : 'normal'}}>Русский</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPressOut={() => this.props.screenProps.updateLocale("en")} style={styles.container}>
                        <Image source={require("../Images/english.png")} style={{marginRight: 20, height: 25, width: 25, resizeMode: 'contain'}}/>
                        <Text style={{fontSize: 16, fontWeight: I18n.currentLocale() == "en" ? 'bold' : 'normal'}}>English</Text>
                    </TouchableOpacity>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%', 
        height: 50,
        flexDirection: 'row', 
        alignItems: 'center',
        marginTop: 10, 
        
    }
})