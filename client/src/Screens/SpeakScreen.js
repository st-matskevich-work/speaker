import React from 'react'
import {View, Text, Image, TouchableOpacity, DeviceEventEmitter, ScrollView, KeyboardAvoidingView, Dimensions, RefreshControl, Keyboard, BackHandler, StatusBar} from 'react-native'
import {connect} from 'react-redux'
import SafeAreaView from 'react-native-safe-area-view'
import { TextInput } from 'react-native-paper'
import CustomInput from '../Components/CustomInput'
import {NavigationEvents} from 'react-navigation'
import * as Constants from '../Constants/Constants'
import * as SpeaksAPI from '../API/Speaks' 
import * as UsersAPI from '../API/Users'
import moment from 'moment'
import 'moment/locale/ru'
import CommentComponent from '../Components/CommentComponent'
import AudioSyncBlock from '../Components/AudioSyncBlock'
import ImageViewer from '../Components/ImageViewerComponent'
import ParsedText from 'react-native-parsed-text';
import * as Actions from '../Actions/Actions'
import { strings } from '../../translation/i18n'

class SpeakScreen extends React.Component{
    state = {
        speak : {},
        refreshing: false,
        comment: "",
        player: "",
        images: [],
        voices: [],
        alreadyScrolled: false,
        currentHeight: 0
    }

    onRefresh = () => {
        this.setState({refreshing: true})
        this.refreshLogic()
        //setTimeout(() => this.setState({refreshing: false}), 5000)
    }

    refreshLogic = () => {
        SpeaksAPI.getCurrentSpeak(this.props.id, this.props.navigation.state.params.speak.id)
        .then(res => {
            if(res.statusCode === 200){
                //console.log('RESULT 200', res)
                this.setState({speak: res.speaks})
            }
            //console.log(res)
        })
        .catch(err => console.log(err))
        .finally(() => {
            this.setState({refreshing: false})
        })
    }

    processDate = () => {
        return moment(new Date(+this.state.speak.date)).startOf('minute').fromNow()
    }

    like = () => {
        this.setState({
            speak: {
                ...this.state.speak,
                likes: this.state.speak.liked ? this.state.speak.likes - 1 : this.state.speak.likes + 1,
                liked: !this.state.speak.liked
            }
        })
        this.props.navigation.state.params.like(this.state.speak.id)
    }

    sendComment = () => {
        if(!this.state.comment.length) return

        SpeaksAPI.addComment(this.props.id, this.props.navigation.state.params.speak.id, this.state.comment)
        .then(res => {
            if(res.statusCode === 200) this.refreshLogic()
            //console.log(res)
        })
        .catch(err => console.log(err))
        .finally(() => {
            this.input.clearInput()
            this.setState({comment: ""})
        })
    }

    toRespeak = () => {
        SpeaksAPI.getCurrentSpeak(this.props.id, this.props.navigation.state.params.speak.respeaked.id)
        .then(res => {
            if(res.statusCode === 200){
                let key = Math.round( Math.random() * 10000000 );
                this.props.navigation.navigate({
                    routeName: 'SpeakScreen',
                    params: {
                        speak: res.speaks,
                        like: this.props.navigation.state.params.like,
                        key: key,
                        parentUpdate: this.props.navigation.state.params.parentUpdate ? this.props.navigation.state.params.parentUpdate : null 
                    },
                    key: key
                })
            }
        })
        .catch(err => console.log(err))
    }

    jumpToUser = (props) => {
        props = props.split('@')[1]
        let key = Math.round( Math.random() * 10000000 );

        UsersAPI.getUserInfoByTag(this.props.id, props)
        .then(result => {
            console.log(result)
            if(result.statusCode === 200){
                this.props.navigation.push('Profile', {
                    user: result
                })
            }
        })
        .catch(err=> console.log(err))
    }

    backBehaviour = () => {
        //console.log('BACK BUTTON')
        /*SpeaksAPI.getSpeaksById(this.props.id, this.props.navigation.state.params.currentUserId)
        .then(res => {
            if(res.statusCode === 200){
                console.log(res, 'DISPATCHING\n\n\n\n\n\n')
                this.props.dispatch(Actions.setSpeaks(res))
            }
        })
        .catch(err => console.log(err))*/
    }

    render(){
        return(
            <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
                <NavigationEvents onWillFocus={() => {
                    Keyboard.dismiss()
                    BackHandler.addEventListener('hardwareBackPress', this.backBehaviour)
                   // console.log('HELLLLLLLLO')
                    //console.log(this.props.navigation.state.params.speak.respeaked)
                    //console.log(this.props.navigation.state.params.speak)
                    this.state.speak = this.props.navigation.state.params.speak
                    this.forceUpdate()
                    //console.log(this.state.speak)
                    this.state.voices = []
                    this.state.images = []
                    this.props.navigation.state.params.speak.attachments.map((value, i) => {
                        if(value.type == 'voice'){
                            //console.log('BEGIN RENDERING...')
                            this.state.voices.push({
                                type: value.type,
                                path: Constants.SERVER_ADDRESS + value.path
                            })
                            this.forceUpdate()
                        }
                        if(value.type == 'image'){
                            this.state.images.push({
                                url: Constants.SERVER_ADDRESS + value.path
                            })
                            this.forceUpdate()
                        }
                    })

                    this.refreshLogic()
                }} onWillBlur={() => {
                    this.setState({speak: {}, comments: []})
                    Keyboard.dismiss()
                    if(this.props.navigation.state?.params?.parentUpdate) this.props.navigation.state?.params?.parentUpdate()
                    BackHandler.removeEventListener('hardwareBackPress', this.backBehaviour)
                }}/>
                <View style={{height: 60, alignItems: 'center', flexDirection: 'row', borderBottomWidth: 0.5, borderBottomColor: '#EDEDED',}}>
                    <TouchableOpacity style={{height: '100%', width: 50, justifyContent: 'center', alignItems: 'center'}} onPressOut={() => this.props.navigation.pop()}>
                        <Image source={require('../Images/back.png')} style={{height: 14, width: 14, resizeMode: 'contain'}}/>
                    </TouchableOpacity>
                    <Text style={{marginLeft: 10, fontSize: 18, textAlign: 'center', textAlignVertical: 'center'}}>{strings('SpeakScreen.header')}</Text>
                    <View style={{flex: 1}}/>
                </View>
                <KeyboardAvoidingView style={{flex: 1}} onLayout={(event) => this.setState({currentHeight: event.nativeEvent.layout.height})}>
                    <ScrollView style={{flex: 1}} 
                                ref={ref => this.scroll = ref}
                                refreshControl={
                                    <RefreshControl colors={['#0AB4BE']} 
                                                    refreshing={this.state.refreshing} 
                                                    onRefresh={this.onRefresh}/>
                                }>
                        <View style={{flex: 1, margin: 20, marginBottom: 10}}>       
                            <View style={{flexDirection: 'row-reverse', maxWidth: '100%'}}>
                                <Text style={{color: '#979797'}}>{this.processDate()}</Text>
                                <View style={{flex: 1}}/>
                                <Text style={{fontSize: 18, fontWeight: 'bold'}}>{this.state.speak.user?.name}{'\n'}<Text style={{fontSize: 15, fontWeight: 'normal', color: '#979797'}}>@{this.state.speak.user?.tag}</Text></Text>
                                <TouchableOpacity onPressOut={() => this.jumpToUser('@' + this.state.speak.user?.tag)}>
                                    <Image source={{uri: Constants.SERVER_ADDRESS + this.state.speak.user?.avatar}} style={{height: 46, width: 46, borderRadius: 30, marginRight: 20, backgroundColor: '#f6f7f8'}}/>
                                </TouchableOpacity>
                            </View>
                            <View style={{marginTop: 10}}>
                                {
                                    this.state.speak.speak ? <ParsedText style={{marginBottom: 10}}
                                                                        parse={[
                                                                            {
                                                                                pattern: /@[a-zA-Z0-9_-]{3,32}/,
                                                                                style: {color: '#0AB4BE'},
                                                                                onPress: this.jumpToUser
                                                                            }
                                                                        ]}>
                                                                        {this.state.speak.speak}
                                                            </ParsedText> : null
                                }
                                <ImageViewer 
                                    images={this.state.images}
                                />
                                <AudioSyncBlock
                                            voiceAttachments={this.state.voices}
                                            player={this.state.player}
                                            setNewPlayer={(player) => {
                                                this.setState({player: player})
                                            }}
                                            allowDeleting={false}
                                />
                            </View>
                            {
                                this.state.speak.respeaked ? (
                                    <TouchableOpacity onPressOut={this.toRespeak} style={{width: '100%', backgroundColor: 'white', marginBottom: 5, maxHeight: 100, borderColor: '#979797', borderRadius: 8, borderWidth: 0.3, padding: 7}}>
                                        <View style={{width: '100%', flexDirection: 'row'}}>
                                            <TouchableOpacity onPressOut={() => this.jumpToUser('@' + this.state.speak.respeaked.tag)}>
                                                <Image source={{uri: Constants.SERVER_ADDRESS + this.state.speak.respeaked.image}} style={{height: 40, width: 40, borderRadius: 30, backgroundColor: '#f6f7f8'}}/>
                                            </TouchableOpacity>
                                            <View style={{flex: 1, marginLeft: 10}}>
                                                <Text style={{fontSize: 16}}>{this.state.speak.respeaked.name}</Text>
                                                <Text style={{color: '#979797', fontSize: 14}}>@{this.state.speak.respeaked.tag}</Text>
                                            </View>
                                        </View>
                                        <Text style={{marginTop: 5}}>{this.state.speak.respeaked.original_text}</Text>
                                    </TouchableOpacity>
                                ) : null
                            }
                        </View>
                        <View style={{flexDirection: 'row', marginTop: 10, justifyContent: 'space-around', borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#EDEDED', padding: 10}}>
                            <View style={{flexDirection: 'row'}}>
                                <TouchableOpacity disabled={true}>
                                    <Image source={require('../Images/comment.png')} style={{height: 20, width: 20, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <Text style={{color: '#979797', fontSize: 15, marginLeft: 5, marginRight: 15}}>{this.state.speak?.comments}</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <TouchableOpacity onPressOut={() => this.props.navigation.navigate('CreateNewSpeak', {
                                    id: this.state.speak.id,
                                    name: this.state.speak.user.name,
                                    parentUpdate: this.props.navigation.state.params.parentUpdate ? this.props.navigation.state.params.parentUpdate : null,
                                    speak: this.state.speak
                                })}>
                                    <Image source={require('../Images/reply.png')} style={{height: 20, width: 20, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <Text style={{color: '#979797', fontSize: 15, marginLeft: 5, marginRight: 15}}>{this.state.speak.respeaks}</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <TouchableOpacity onPressOut={this.like}>
                                    <Image source={this.state.speak?.liked ? require('../Images/liked.png') : require('../Images/like.png')} style={{height: 20, width: 20, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <Text style={{color: '#979797', fontSize: 15, marginLeft: 5, marginRight: 15}}>{this.state.speak?.likes}</Text>
                            </View>
                        </View>
                        {
                            this.state.speak.commentsArray?.length ? (
                                this.state.speak.commentsArray.map((value, i) => {
                                    return <CommentComponent onLayout={(event) => {
                                        if(this.props.navigation.state.params.commentId == value.commentid && !this.state.alreadyScrolled){
                                            let bruh = event.nativeEvent.layout
                                            setTimeout(() => {
                                                this.scroll.scrollTo({
                                                    animated: true,
                                                    x: bruh.x,
                                                    y: bruh.y
                                                })
                                            }, 1)
                                            this.setState({alreadyScrolled: true})
                                        }
                                    }} navigation={this.props.navigation} key={value.commentid} comment={value}/>
                                })
                            ) : (
                                <Text style={{textAlign: 'center', fontSize: 18, marginBottom: 20, color: '#0AB4BE', fontWeight: 'bold', marginTop: 20}}>{strings('SpeakScreen.noCommentsTitle')}{'\n'}<Text style={{fontWeight: 'normal', fontSize: 14}}>{strings('SpeakScreen.noCommentsBody')}</Text></Text>
                            )
                        }
                    </ScrollView>
                    <View style={{flexDirection: 'row'}} onLayout={event => this.setState({commentBlockHeight: event.nativeEvent.layout.height})}>
                        <CustomInput 
                            onRef={ref => this.input = ref}
                            placeholder={strings('SpeakScreen.comment')} 
                            type="none" 
                            value={this.state.text}
                            onChangeText={(text) => this.setState({comment: text})} 
                            multiline={true}
                            viewStyle={{marginBottom: 0, maxWidth: Dimensions.get('window').width - 80, borderBottomWidth: 0}}
                            style={{marginBottom: 0, borderTopWidth: 1, borderTopColor: '#EDEDED', paddingLeft: 10, textAlignVertical: 'center', width: 'auto', flexGrow: 1, maxWidth: Dimensions.get('window').width - 80, borderBottomWidth: 0}}/>
                        <View style={{borderTopWidth: 1, borderTopColor: '#EDEDED'}}>
                            <View style={{flex: 1}}/>
                            <TouchableOpacity onPressOut={this.sendComment}>
                                <Image source={require('../Images/send.png')} style={{height: 35, resizeMode: 'contain', marginBottom: 4, marginRight: -20}}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

export default connect(state => ({id: state.id}))(SpeakScreen)