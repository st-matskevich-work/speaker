import React from 'react'
import {View, Text, Image, TouchableOpacity, RefreshControl, FlatList, TextInput, Keyboard} from 'react-native'
import SafeAreaView from 'react-native-safe-area-view'
import { connect } from 'react-redux'
import MessageTile from '../Components/MessageTile'
import * as UsersAPI from '../API/Users'
import { NavigationEvents } from 'react-navigation'
import { strings } from '../../translation/i18n'

class AddNewChatScreen extends React.Component{
    state = {
        loading: false,
        searchUsers: [],
        text: "",
    }

    onTextChange = (text) => {
        if(!text.length){
            this.setState({searchUsers: []})
            return;
        }

        this.setState({text: text});

        UsersAPI.getUsersByQuery(text)
        .then(res => {
            console.log(res, '\n\n\n')
            if(res.statusCode === 200){
                this.setState({searchUsers: res.users})
            }else{
                this.setState({searchUsers: []})
            }
        })
        .catch(err => {

        })
    }

    render(){
        return(
            <SafeAreaView style={{backgroundColor: 'white', flex: 1}} ref={ref => this.view = ref}>
                <NavigationEvents onWillFocus={() => {
                    //this.setState({text: ""})
                    Keyboard.dismiss()
                }} onWillBlur={() => Keyboard.dismiss()}/>
                <View style={{height: 60, backgroundColor: 'white', flexDirection: 'row', alignItems: 'center', borderBottomWidth: 0.3, borderBottomColor: 'rgba(151,151,151,0.2)'}}>
                    <TouchableOpacity style={{marginLeft: 20, marginRight: 20}} onPressOut={() => this.props.navigation.goBack()}>
                        <Image source={require('../Images/back.png')} style={{height: '100%', width: 16, resizeMode: 'contain'}}/>
                    </TouchableOpacity>
                    <Text style={{fontSize: 18}}>{strings('AddNewChatScreen.createChat')}</Text> 
                    <View style={{flex: 1}}/>
                    {
                        this.state.loading ? <DotIndicator count={3} color="#0AB4BE" size={8}/> : null
                    }   
                </View>
                <TouchableOpacity style={{height: 50, backgroundColor: 'white', flexDirection: 'row', marginTop: 1, alignItems: 'center'}}>
                    <TextInput ref={ref => this.input = ref} onChangeText={this.onTextChange} selectTextOnFocus={true} style={{ backgroundColor: '#EDF7F8', borderRadius: 20, height: 40, paddingLeft: 15, paddingRight: 15, fontSize: 14, flex: 1, marginLeft: 15, marginRight: 15}} placeholder={strings('AddNewChatScreen.placeholder')}/>
                </TouchableOpacity>
                <FlatList style={{flex: 1}}
                            keyboardShouldPersistTaps='always'
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.loading}
                                    onRefresh={this.onRefresh}
                                    colors={['#0AB4BE']}
                                 />
                              }
                            data={this.state.searchUsers}
                            keyExtractor={item => {
                                //console.log(item)
                                if(item) return item.id
                            }}
                            extraData={this.props}
                            renderItem={({item}) => {
                                console.log(item)
                                if(item) return <MessageTile navigation={this.props.navigation} user={item} lastMessage={this.props.messages["" + item.id]?.messages.length ? this.props.messages["" + item.id].messages[0].text.length ? this.props.messages["" + item.id].messages[0].text : "Изображение" : "Пока пусто..."}/>
                            }}/>
            </SafeAreaView>
        )
    }
}

export default connect(state => ({messages: state.messages}))(AddNewChatScreen)
