import React from 'react'
import {FlatList, StyleSheet, View, Text} from 'react-native'
import SpeakBlock from '../Components/SpeakBlock'

export default class SpeaksAndAnswers extends React.Component{
    data = [
        {
            id: 1,
            text: 'First Bruh'
        },
        {
            id: 2,
            text: 'SecondBruh'
        }
    ]

    render(){
        return(
            <View onLayout={(event) => this.props.heightCallback(event.nativeEvent.layout.height)} style={{minHeight: this.props.minimalHeight - 52, backgroundColor: 'white'}}>
                <View style={{flex: 1, alignItems: 'center', marginTop: 10}}>
                    <Text style={{color: '#0AB4BE', fontWeight: 'bold', fontSize: 18}}>Пока нет спиков</Text>
                    <Text style={{textAlign: 'center', marginTop: 5, color: '#0AB4BE'}}>Когда будут опубликованы спики или{'\n'}ответы, они будут отображаться здесь</Text>
                </View>
            </View>
        )
    }
}