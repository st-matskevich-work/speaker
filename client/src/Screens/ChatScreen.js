import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import { View, Text, StyleSheet, Button, Image, TouchableOpacity, Platform, KeyboardAvoidingView, Animated, Modal, ActivityIndicator, Dimensions, Keyboard, MessageList,BackHandler } from 'react-native'
import { NavigationEvents } from 'react-navigation'
import { connect } from 'react-redux'
import { GiftedChat, Send, Bubble, Message, MessageImage } from 'react-native-gifted-chat'
import { stat } from 'react-native-fs'
import VoiceRecorder from '../Components/VoiceRecorderButton'
import CustomInput from '../Components/CustomInput'
import moment from 'moment'
import 'moment/locale/ru'
import ImageViewer from 'react-native-image-zoom-viewer';
import VoiceTile from '../Components/VoiceTile'
import * as Actions from '../Actions/Actions'
import io from 'socket.io-client';
import * as Constants from '../Constants/Constants'
import { cos } from 'react-native-reanimated'
import * as SpeaksAPI from '../API/Speaks'
import { DotIndicator } from 'react-native-indicators';
import ImagePicker from 'react-native-image-crop-picker';
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';
import { Player } from '@react-native-community/audio-toolkit';
import * as UsersAPI from '../API/Users'
import PushNotification from 'react-native-push-notification'
import { strings } from '../../translation/i18n'

class Chat extends React.Component { //Компонент чата
    state = { //Состояние компонента
        user: {}, //Пользователь, с которым чат
        messages: [], //Сообщения
        toolbarHeight: 44, //Высота панели инструментов
        attachButtonsPosition: new Animated.Value(0), //Величина, отвечающая за скрытие кнопок
        showImageViewer: false, //показать просмотрщик картинок
        image: {}, //картинка, которая прикрепляется к сообщению
        loading: true, //Идет ли загрузка
        text: "", //сообщение, введенное пользователем
        player: null, //обьект проигрывателя голосовых сообщений
        voiceRefs: [], //массив ссылок на голосовые сообщения для синхронизации (включил одно - выключилось другое)
        audioSeek: 0, //Момент, на котором остановилось прослушивание
        currentAudioIndex: 0 //Текущее сообщение в массиве ссылок на голосовые (this.state.voiceRefs)
    }


    componentDidMount() { //Монтирование компонента
        this.props.screenProps.addUpdateRef(this) //коллбэк на добавление ссылки на текущий обьект для обновления извне
    }

    openImageViewer = (images) => { //показать фото на весь экран
        this.setState({ images: images, showImageViewer: true })
    }

    onSend = (messages = [], voice = false) => { //отправить сообщение
        //console.log(messages[0].user)

        if (voice) { //если голосовое
            messages.push({ //формируем обьект сообщения
                _id: this.props.messages["" + this.props.navigation.state.params.user.id]?.messages + 1,
                text: 'Голосовое сообщение',
                voice: voice.path,
                createdAt: new Date(),
                user: {
                    _id: +this.props.id,
                },
            })

            SpeaksAPI.addAttachments([{ //Загружаем вложения
                data: voice.base64,
                type: 'wav'
            }], this.props.id, this.props.token)
                .then(res => {
                    


                    if (res.statusCode === 200) { //Если загружено успешно
                        this.sendToServer(messages[0], res.id) //загрузка сообщения
                    }
                })
                .catch(err => {

                })
        }
        else if (this.state.image.url) { //если сообщение с картинкой
            messages[0].image = this.state.image.url // ссылку на картинку

            SpeaksAPI.addAttachments([{ //загружаем картинки
                data: this.state.image.props.base64,
                type: this.state.image.props.extension
            }], this.props.id, this.props.token)
                .then(res => {
                    
                    if (res.statusCode === 200) { //если успешно
                        this.sendToServer(messages[0], res.id) //Отправить сообщение
                    }
                })
                .catch(err => {

                })

            this.setState({ image: {} })

            Animated.timing(this.state.attachButtonsPosition).stop() //показать кнопки
            Animated.timing(
                this.state.attachButtonsPosition,
                {
                    duration: 150,
                    toValue: 0
                }
            ).start()
        } else {//Если текст
            this.sendToServer(messages[0]) //Отправка 
        }

        this.props.dispatch(Actions.addMessage(messages[0], this.props.navigation.state.params.user)) //добавить сообщение в Redux store

        //обновить компонент
        this.setState(previousState => ({ 
            messages: GiftedChat.append(previousState.messages, messages)
        }))

        this.props.screenProps.updateRefs({
            "user": this.props.navigation.state.params.user,
            "messages": messages
        })
    }

    //Отправить обьект сообщения
    sendToServer = (value, attachment_id = []) => {
        try { //если сокет инициализирован
            this.props.socket.emit('send_message', {
                sender: this.props.id,
                receiver: "" + this.props.navigation.state.params.user.id,
                message: value.text,
                createdAt: value.createdAt,
                attachment_id: attachment_id
            })
        } catch{
            this.props.screenProps.socketInit() //Если нет - инициализировать

            this.props.socket.emit('send_message', {
                sender: this.props.id,
                receiver: "" + this.props.navigation.state.params.user.id,
                message: value.text,
                createdAt: value.createdAt,
                attachment_id: attachment_id
            })
        }
    }

    messageReceived = (params) => { //Сообщение доставлено по сокету - апдейт компонента чата 
        this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, params.messages)
        }))

        console.log(this.props.messages["" + this.props.navigation.state.params.user.id]?.messages)
        this.forceUpdate()
    }

    uploadFromGallery = () => { //Загрузить из галереи
        ImagePicker.openPicker({
            cropping: true,
            includeBase64: true
        }).catch(error => console.log(error))
            .then(photo => {
                //console.log(photo.mime)
                this.setState({
                    image: {
                        url: photo.path,
                        props: {
                            base64: photo.data,
                            extension: photo.mime.split('/')[1]
                        }
                    }
                })

                Animated.timing(this.state.attachButtonsPosition).stop()
                Animated.timing(
                    this.state.attachButtonsPosition,
                    {
                        duration: 150,
                        toValue: -100
                    }
                ).start()
            })
            .catch(error => console.log(error))
    }

    uploadFromCamera = () => { //Загрузить с камеры
        check(PERMISSIONS.ANDROID.CAMERA)//Получение доступа на камеру
            .then(result => {
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        console.log(
                            'This feature is not available (on this device / in this context)',
                        );
                        break;
                    case RESULTS.DENIED:
                        {
                            request(PERMISSIONS.ANDROID.CAMERA)
                                .then(result => {
                                    if (result == 'granted') {
                                        ImagePicker.openCamera({
                                            cropping: true,
                                            includeBase64: true
                                        }).catch(error => console.log(error))
                                            .then(photo => {
                                                //console.log(photo.mime)
                                                this.setState({
                                                    image: {
                                                        url: photo.path,
                                                        props: {
                                                            base64: photo.data,
                                                            extension: photo.mime.split('/')[1]
                                                        }
                                                    }
                                                })

                                                Animated.timing(this.state.attachButtonsPosition).stop()
                                                Animated.timing(
                                                    this.state.attachButtonsPosition,
                                                    {
                                                        duration: 150,
                                                        toValue: -100
                                                    }
                                                ).start()
                                            })
                                            .catch(error => console.log(error))
                                    }
                                })
                            break;
                        }
                    case RESULTS.GRANTED:
                        {
                            ImagePicker.openCamera({
                                cropping: true,
                                includeBase64: true
                            }).catch(error => console.log(error))
                                .then(photo => {
                                    //console.log(photo.mime)
                                    /*this.state.image = {
                                        url: photo.path,
                                        props: {
                                            base64: photo.data,
                                            extension: photo.mime.split('/')[1]
                                        }
                                    }
                                    this.forceUpdate()*/
                                    this.setState({
                                        image: {
                                            url: photo.path,
                                            props: {
                                                base64: photo.data,
                                                extension: photo.mime.split('/')[1]
                                            }
                                        }
                                    })

                                    Animated.timing(this.state.attachButtonsPosition).stop()
                                    Animated.timing(
                                        this.state.attachButtonsPosition,
                                        {
                                            duration: 150,
                                            toValue: -100
                                        }
                                    ).start()
                                })
                                .catch(error => console.log(error))
                        }
                    case RESULTS.BLOCKED:
                        console.log('The permission is denied and not requestable anymore');
                        break;
                }
            })
    }

    onBackPress = () => { //Нажатие клавиши назад
        this.props.navigation.navigate('MessageList')
    }

    jumpToUser = (props) => { //перейти на пользователя
        if(this.state.loading) return

        props = props.split('@')[1]
        let key = Math.round( Math.random() * 10000000 );

        this.setState({loading: true})
        UsersAPI.getUserInfoByTag(this.props.id, props)
        .then(result => {
            console.log(result)
            if(result.statusCode === 200){
                this.props.navigation.push('Profile', {
                    user: result
                })
            }
        })
        .catch(err=> console.log(err))
        .finally(() => {
            this.setState({loading: false})
        })
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <NavigationEvents onWillFocus={() => {
                    Keyboard.dismiss()
                    PushNotification.clearLocalNotification(this.props.navigation.state.params.user.id)
                    this.props.screenProps.addUpdateRef(this)
                    this.setState({ user: this.props.navigation.state.params.user })
                    SpeaksAPI.getChatById(this.props.id, this.props.navigation.state.params.user.id, this.props.token)
                        .then(res => {
                            if (res.statusCode === 200) {
                                this.props.dispatch(Actions.setChat(res.messages, this.props.navigation.state.params.user))
                                this.setState({ loading: false })
                                console.log('Here after dispatching', this.props.messages["" + this.props.navigation.state.params.user.id])
                            }
                        })
                        .catch(err => {
                            console.log(err)
                            this.setState({ loading: false })
                        })
                }} onWillBlur={() => {
                    let msgId;

                    this.props.messages["" + this.props.navigation.state.params.user.id]?.messages.map((value, i) => {
                        if(value.user._id == this.props.navigation.state.params.user.id && !msgId) msgId = value._id
                    })

                    if(msgId){
                        UsersAPI.setLastReadMessage(this.props.id, 
                            this.props.navigation.state.params.user.id, 
                            this.props.token, 
                            msgId)
                        .then(res => {})
                        .catch(err =>{})

                        this.props.dispatch(Actions.setReadState(this.props.navigation.state.params.user.id))
                        if(this.props.navigation.state.params.disableStatusUpdate) this.props.navigation.state.params.disableStatusUpdate(this.props.navigation.state.params.user.id)
                    }

                    Keyboard.dismiss()
                }}/>
                <View style={{ height: 60, backgroundColor: 'white', borderBottomWidth: 0.3, borderBottomColor: 'rgba(151,151,151,0.5)', flexDirection: 'row', alignItems: 'center' }}>
                    <TouchableOpacity style={{ marginLeft: 20, marginRight: 20 }} onPressOut={() => {
                        if(this.props.navigation.state.params.disableStatusUpdate){
                            this.props.navigation.navigate('MessageList', {
                                dontUpdateChatStatus: this.props.navigation.state.params.user.id
                            })
                        }else{
                            console.log('GO_BACK')
                            this.props.navigation.goBack()
                        }
                    }}>
                        <Image source={require('../Images/back.png')} style={{ height: '100%', width: 16, resizeMode: 'contain' }} />
                    </TouchableOpacity>
                    <View>
                        <Text style={{ fontSize: 18 }}>{this.props.navigation.state.params.user.name}</Text>
                        <Text style={{ color: '#979797' }}>@{this.props.navigation.state.params.user.tag}</Text>
                    </View>
                    <View style={{ flex: 1 }} />
                    {
                        this.state.loading ? <DotIndicator count={3} color="#0AB4BE" size={8} /> : null
                    }
                </View>
                <GiftedChat
                    messages={this.props.messages["" + this.props.navigation.state.params.user.id]?.messages}
                    extraData={this.state.messages}
                    alwaysShowSend={true}
                    isLoadingEarlier={true}
                    text={this.state.text}
                    loadEarlier={false}
                    renderLoadEarlier={() => {
                        return (
                            <View>
                                <ActivityIndicator color="#0AB4BE" size="small" />
                            </View>
                        )
                    }}
                    placeholder={strings('ChatScreen.placeholder')}
                    onSend={this.onSend}
                    user={{
                        _id: +this.props.id,
                    }}
                    renderLoadEarlier={() => (
                        <View>
                            <ActivityIndicator size={Platform.OS == "android" ? 30 : "small"} color="#182c4c" />
                        </View>
                    )}
                    imageProps={{ openImageViewer: this.openImageViewer }}
                    onInputTextChanged={text => {
                        this.setState({ text: text })
                        if (text.length) {
                            Animated.timing(
                                this.state.attachButtonsPosition,
                                {
                                    duration: 150,
                                    toValue: this.state.image.url ? -100 : -30
                                }
                            ).start()
                        }
                        else {
                            Animated.timing(this.state.attachButtonsPosition).stop()
                            Animated.timing(
                                this.state.attachButtonsPosition,
                                {
                                    duration: 150,
                                    toValue: this.state.image.url ? -100 : 0
                                }
                            ).start()
                        }
                    }}
                    minInputToolbarHeight={this.state.toolbarHeight}
                    renderSend={(props) => {
                        return (
                            <Send {...props}>
                                <TouchableOpacity onPressOut={() => {
                                    if (!this.state.text.trim().length && !this.state.image.url) return

                                    this.onSend([{
                                        _id: this.props.messages["" + this.props.navigation.state.params.user.id]?.messages + 1,
                                        text: this.state.text.trim(),
                                        createdAt: new Date(),
                                        user: {
                                            _id: +this.props.id,
                                        },
                                    }])
                                    this.setState({ text: "" })
                                    Animated.timing(this.state.attachButtonsPosition).stop()
                                    Animated.timing(
                                        this.state.attachButtonsPosition,
                                        {
                                            duration: 150,
                                            toValue: 0
                                        }
                                    ).start()
                                }}>
                                    <Text style={{ fontSize: 16, color: '#0AB4BE', fontWeight: 'bold', marginRight: 10, marginBottom: 15 }}>{strings('ChatScreen.send')}</Text>
                                </TouchableOpacity>
                            </Send>
                        )
                    }}
                    renderMessage={props => {
                        //console.log('RENDERING.............')
                        return (
                            <View>
                                <Message {...props} containerStyle={{ color: 'blue' }} />
                            </View>
                        )
                    }}
                    onPressAvatar={() => this.jumpToUser('@' + this.props.navigation.state.params.user.tag)}
                    renderMessageText={(props) => {
                        console.log(props.currentMessage, '\n\n\n\n')
                        return props.currentMessage.voice ? (
                            <VoiceTile style={{ width: 267, height: 60, margin: 6 }}
                                onRef={ref => this.state.voiceRefs[props.currentMessage._id] = ref}
                                play={callback => {
                                    //console.log(props.currentMessage.voice)
                                    if (this.state.player) {
                                        this.state.voiceRefs[this.state.currentAudioIndex].resetProgress()
                                        this.state.player.destroy()
                                    }

                                    let audioPlayer = new Player(props.currentMessage.voice.includes('com.speaker') ? props.currentMessage.voice : Constants.SERVER_ADDRESS + props.currentMessage.voice)
                                    audioPlayer.on('ended', () => {
                                        this.state.voiceRefs[props.currentMessage._id].resetProgress()
                                    })

                                    audioPlayer.play(() => {
                                        callback(audioPlayer.duration)
                                    })
                                    //callback(audioPlayer.duration)
                                    this.setState({ currentAudioIndex: props.currentMessage._id, player: audioPlayer })
                                }}
                                continue={(callback) => {
                                    this.state.player.seek(this.state.audioSeek)
                                    this.state.player.play()
                                    callback(this.state.player.duration - this.state.audioSeek)
                                }}
                                pause={(callback) => {
                                    this.state.player.pause()
                                    this.state.audioSeek = this.state.player.currentTime
                                    this.forceUpdate()
                                    callback()
                                }} />
                        ) : <Text style={{ padding: 5, paddingLeft: 10, paddingBottom: 0, paddingRight: 10 }}>{props.currentMessage.text}</Text>
                    }}
                    renderMessageImage={(props) => {

                        //console.log(props.currentMessage.image)
                        let images = [{
                            url: props.currentMessage.image.includes('file://') ? props.currentMessage.image : Constants.SERVER_ADDRESS + props.currentMessage.image
                        }]

                        return (
                            <TouchableOpacity style={{ width: 267, height: 150, marginTop: 6, overflow: 'hidden', marginLeft: 6, marginRight: 6 }} onPressOut={() => props.imageProps.openImageViewer(images)}>
                                <Image source={{ uri: props.currentMessage.image.includes('file://') ? props.currentMessage.image : Constants.SERVER_ADDRESS + props.currentMessage.image }} style={{ width: '100%', resizeMode: 'cover', height: '100%', borderRadius: 10,backgroundColor: '#f6f7f8' }} />
                            </TouchableOpacity>
                        )
                    }}
                    renderTime={(props) => {
                        return <Text style={{ padding: 5, paddingLeft: 10, paddingRight: 10 }}>{moment(props.currentMessage.createdAt).locale('ru').format("HH:mm")}</Text>
                    }}
                    renderChatFooter={(props) => {
                        return this.state.image.url ? (
                            <View style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').width * 9 / 16, backgroundColor: 'white' }}>
                                <Image style={{ flex: 1, resizeMode: 'cover', margin: 6, marginBottom: 10, borderRadius: 20 }} source={{ uri: this.state.image.url }} />
                                <TouchableOpacity style={{ position: 'absolute', top: 16, right: 16, backgroundColor: 'rgba(255,255,255,0.5)', height: 20, width: 20, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }} onPressOut={() => {
                                    this.setState({ image: {} })

                                    Animated.timing(this.state.attachButtonsPosition).stop()
                                    Animated.timing(
                                        this.state.attachButtonsPosition,
                                        {
                                            duration: 150,
                                            toValue: this.state.text.length ? -30 : 0
                                        }
                                    ).start()
                                }}>
                                    <Image source={require('../Images/close.png')} style={{ height: 14, width: 14, resizeMode: 'contain' }} />
                                </TouchableOpacity>
                            </View>
                        ) : null
                    }}
                    renderBubble={props => {
                        return (
                            <View>
                                <Bubble {...props} wrapperStyle={{
                                    left: {
                                        backgroundColor: '#DBEFF0',
                                    },
                                    right: {
                                        backgroundColor: '#DBEFF0'
                                    }
                                }} />
                            </View>
                        )
                    }}
                    renderActions={(props) => {
                        return (
                            <Animated.View style={{ width: 100, height: 48, flexDirection: 'row', marginLeft: this.state.attachButtonsPosition }}>
                                <VoiceRecorder onRecorded={voice => {
                                    this.onSend([], voice)
                                }} style={{ marginLeft: 0, width: 40, marginRight: -10 }} />
                                <View style={[{ marginRight: 5, marginLeft: 10, width: 20, alignItems: 'center', justifyContent: 'center', overflow: 'visible' }, this.props.style]}>
                                    <TouchableOpacity style={{ marginLeft: 10, marginRight: 5 }} onPressOut={this.uploadFromGallery}>
                                        <Image source={require('../Images/gallery.png')} style={{ height: '100%', width: 28, resizeMode: 'contain' }} />
                                    </TouchableOpacity>
                                </View>
                                <View style={[{ marginRight: 5, marginLeft: 10, width: 20, alignItems: 'center', justifyContent: 'center', overflow: 'visible' }, this.props.style]}>
                                    <TouchableOpacity style={{ marginLeft: 10, marginRight: 5 }} onPressOut={this.uploadFromCamera}>
                                        <Image source={require('../Images/photo.png')} style={{ height: '100%', width: 28, resizeMode: 'contain' }} />
                                    </TouchableOpacity>
                                </View>
                            </Animated.View>
                        )
                    }}

                />
                <Modal onRequestClose={() => this.setState({ showImageViewer: false })} visible={this.state.showImageViewer} transparent={false}>
                    <ImageViewer
                        loadingRender={() => <Text>Loading...</Text>}
                        imageUrls={this.state.images}
                        enableSwipeDown={true}
                        onSwipeDown={() => this.setState({ showImageViewer: false })} />
                </Modal>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
})

export default connect(state => ({ login: state.login, id: state.id, messages: state.messages, socket: state.socket, token: state.token }))(Chat)