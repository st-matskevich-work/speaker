import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import SafeArea, { SafeAreaInsets } from 'react-native-safe-area-view'
import {View, Text, StyleSheet, Button, ScrollView, Dimensions, TouchableOpacity, Image, StatusBar, RefreshControl, DeviceEventEmitter, Modal as ModalNative, TouchableOpacityBase} from 'react-native'
import { connect } from 'react-redux'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import LoginScreen from './Auth/LoginScreen'
import { NavigationNativeContainer } from '@react-navigation/native'
import SpeaksList from './ProfileFeed/SpeaksList'
import { cos } from 'react-native-reanimated'
import { TabView, SceneMap } from 'react-native-tab-view';
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets';
import CustomTab from '../Components/CustomProfileTab'
import * as Constants from '../Constants/Constants'
import * as Actions from '../Actions/Actions'
import * as SpeaksAPI from '../API/Speaks'
import * as UsersAPI from '../API/Users'
import {NavigationEvents} from 'react-navigation'
import moment from 'moment'
import 'moment/locale/ru'
import { Player } from '@react-native-community/audio-toolkit'
import ImageViewer from 'react-native-image-zoom-viewer'
import Modal, { FadeAnimation, ModalContent, ModalTitle, ModalButton } from 'react-native-modals';
import ImagePicker from 'react-native-image-crop-picker';
import {check, PERMISSIONS, RESULTS, request} from 'react-native-permissions';
import { strings } from '../../translation/i18n'

let News = createMaterialTopTabNavigator()
const initialLayout = { width: Dimensions.get('window').width};

class Profile extends React.Component{
    state = {
        index: 0,
        routes: [
            {
                key: 'speaks',
                title: strings('ProfileScreen.speaks')
            },
            {
                key: 'answers',
                title: strings('ProfileScreen.answers')
            },
            {
                key: 'media',
                title: strings('ProfileScreen.media')
            },
            {
                key: 'liked',
                title: strings('ProfileScreen.liked')
            }
        ],
        scrollHeight: '100%',
        speaksHeight: 0,
        speaksAndAnswersHeight: 0,
        mediaHeight: 0,
        likedHeight: 0,
        profileHeight: 0,
        user: {},
        availableScreenSize: 0,
        refreshing: false,
        player: "",
        speaks: [],
        respeaks: [],
        media: [],
        liked: [],
        showModal: false,
        images: [{url: ""}],
        index: 0,
        showChangeAvatar: false
    }

    onRefresh = async (showindicator = true) => {
        //this.speaksList?.recalculateHeight()
        //console.log(this.state.user)
        //console.log(this.speaksList)
        console.log('REFRESHIG...')
        this.setState({refreshing: showindicator})
        // console.log(this.props.id, this.props.navigation.state.params.user.id, 'ON REFRESH')
        //console.log(this.props.navigation.state.params.user.id) 
        console.log('ID', this.props.id, this.props.navigation.state.params.user.id)
        SpeaksAPI.getSpeaksById(this.props.id, this.props.navigation.state.params.user.id)
        .then(res => {
            console.log(res, 'HERE')
            if(res.statusCode === 200){
                //this.setState({speaks: res.speaks.reverse()})
                this.props.dispatch(Actions.setSpeaks(res, this.props.navigation.state.params.user.id))
                this.forceUpdate()
                //this.setState({liked: res.liked, media: res.media, speaks: res.speaks, respeaks: res.respeaks})
            }
        })
        .catch(err => console.log(err))
        .finally(() => {
            this.setState({refreshing: false})
        })


        UsersAPI.getUserInfoByTag(this.props.id, this.props.navigation.state.params.user.tag)
        .then(result => {
            if(result.statusCode === 200){
                this.setState({user: result})
                console.log(this.state.images[0].url, result.image, '\n\n\n\n\n')
                this.state.images[0] = {url: Constants.SERVER_ADDRESS + result.image}

                if(this.props.id == this.state.user.id){
                    this.props.dispatch(Actions.setUser(result))
                }
            }
        })
        .catch(err=> console.log(err))
    }

    async componentDidMount(){
        this.state.user = this.props.navigation.state.params.user
        this.state.images[0] = {url: Constants.SERVER_ADDRESS + this.props.navigation.state.params.user.image}
                        //console.log(this.props.navigation.state.params.user.id == this.props.id, 'MEEEEEEEEEEEh')
        this.forceUpdate()
        console.log('DID MOUNT')
        this.onRefresh(false)
    }

    processDate = () => {
        return moment(this.state.user.date).format("MMMM YYYY")
    }

    subscription = () => {
        //Add sub api
        UsersAPI.subscribe(this.props.id, this.state.user.id)
        .then(res => {
            if(res.statusCode === 200){
                UsersAPI.getUserInfoByTag(this.props.id, this.state.user.tag)
                .then(result => {
                    if(result.statusCode === 200){
                        this.setState({user: result})
                    }
                })
                .catch(err=> console.log(err))
            }
        })
        .catch(err=> console.log(err))
    }

    showChangeAvatarModal = () =>  {
        if(this.props.id == this.state.user.id){
            this.setState({showChangeAvatar: true})
        }else{
            this.setState({showModal: true, images: [{url: Constants.SERVER_ADDRESS + this.state.user.image}]})
        }
    }

    uploadFromGallery = () => {
        this.setState({showChangeAvatar: false})
        ImagePicker.openPicker({
            cropping: true,
            includeBase64: true
        }).catch(error => console.log(error))
        .then(photo => {
            this.setState({showChangeAvatar: false})
            UsersAPI.updateAvatar(this.props.id, this.props.token, photo.data, photo.mime.split('/')[1])
            .then(res => {
                if(res.statusCode === 200) {
                    this.onRefresh(false)
                }
            })
            .catch(err => console.log(err))
        })
        .catch(error => console.log(error))
    }

    uploadFromCamera = () => {
        check(PERMISSIONS.ANDROID.CAMERA)
        .then(result => {
          switch(result){
            case RESULTS.UNAVAILABLE:
            console.log(
              'This feature is not available (on this device / in this context)',
            );
            break;
          case RESULTS.DENIED:
          {
            request(PERMISSIONS.ANDROID.RECORD_AUDIO)
            .then(result => {
                if(result == 'granted'){
                    this.setState({showChangeAvatar: false})
                    ImagePicker.openCamera({
                        cropping: true,
                        includeBase64: true
                    }).catch(error => console.log(error))
                    .then(photo => {
                        //console.log(photo.mime)
                        this.setState({showChangeAvatar: false})
                        UsersAPI.updateAvatar(this.props.id, this.props.token, photo.data, photo.mime.split('/')[1])
                        .then(res => {
                            if(res.statusCode === 200) {
                                this.onRefresh(false)
                            }
                        })
                        .catch(err => console.log(err))
                        this.forceUpdate()
                    })
                    .catch(error => console.log(error))
                }
            })
            break;
          }
          case RESULTS.GRANTED:
          {
            this.setState({showChangeAvatar: false})
            ImagePicker.openCamera({
                cropping: true,
                includeBase64: true
            }).catch(error => console.log(error))
            .then(photo => {
                //console.log(photo.mime)
                this.setState({showChangeAvatar: false})
                UsersAPI.updateAvatar(this.props.id, this.props.token, photo.data, photo.mime.split('/')[1])
                .then(res => {
                    if(res.statusCode === 200) {
                        this.onRefresh(false)
                    }
                })
                .catch(err => console.log(err))
                this.forceUpdate()
            })
            .catch(error => console.log(error)) 
          }
          case RESULTS.BLOCKED:
            console.log('The permission is denied and not requestable anymore');
            break;
          }
        })
    }

    render(){
        /*const renderScene = SceneMap({
            speaks: SpeaksList,
            answers: SpeaksAndAnswersList,
            media: SpeaksList,
            liked: SpeaksList
        })*/
        const renderScene = ({ route }) => {
            switch (route.key) {
              case 'speaks':
                return <SpeaksList 
                                nullText = {strings('ProfileScreen.speaksNullText')}
                                player={this.state.player} 
                                setNewPlayer={(player) => {
                                    this.setState({player: player})
                                }}
                                updateList={this.onRefresh}
                                navigation={this.props.navigation} 
                                onRef={ref => this.speaksList = ref}
                                minimalHeight={this.state.availableScreenSize} 
                                speaks={this.props.speaks[this.props.navigation.state.params.user.id]?.speaks} 
                                userId={this.state.user.id}
                                heightCallback={(height) => this.setState({speaksHeight: height, scrollHeight: height + 52})} />;
              case 'answers':
                return <SpeaksList 
                                nullText = {strings('ProfileScreen.answersNullText')}
                                player={this.state.player} 
                                setNewPlayer={(player) => {
                                    this.setState({player: player})
                                }}
                                navigation={this.props.navigation} 
                                updateList={this.onRefresh}
                                onRef={ref => this.speaksList = ref}
                                minimalHeight={this.state.availableScreenSize} 
                                userId={this.state.user.id}
                                speaks={this.props.speaks[this.props.navigation.state.params.user.id]?.respeaks} 
                                heightCallback={(height) => {
                                    this.setState({speaksAndAnswersHeight: height})
                                }} />
              case 'media':
                return <SpeaksList 
                                nullText = {strings('ProfileScreen.mediaNullText')}
                                player={this.state.player} 
                                setNewPlayer={(player) => {
                                    this.setState({player: player})
                                }}
                                navigation={this.props.navigation} 
                                updateList={this.onRefresh}
                                onRef={ref => this.speaksList = ref}
                                minimalHeight={this.state.availableScreenSize} 
                                speaks={this.props.speaks[this.props.navigation.state.params.user.id]?.media} 
                                userId={this.state.user.id}
                                heightCallback={(height) => {
                                    this.setState({mediaHeight: height})
                                }} />
              case 'liked':
                return <SpeaksList 
                                nullText = {strings('ProfileScreen.likedText')}
                                player={this.state.player} 
                                setNewPlayer={(player) => {
                                    this.setState({player: player})
                                }}
                                userId={this.state.user.id}
                                navigation={this.props.navigation} 
                                onRef={ref => this.speaksList = ref}
                                updateList={this.onRefresh}
                                minimalHeight={this.state.availableScreenSize} 
                                speaks={this.props.speaks[this.props.navigation.state.params.user.id]?.liked} 
                                heightCallback={(height) => {
                                    this.setState({likedHeight: height})
                                }} />
              default:
                return null;
            }
        }
        StatusBar.setBarStyle('dark-content')
        //console.log(this.state.user, this.props.id, 'IDDDDD')
        return(
                <View style={{flex: 1}} onLayout={(event) => this.setState({availableScreenSize: event.nativeEvent.layout.height})}>
                    <NavigationEvents onDidFocus={() => console.log('DID')} onWillFocus={() => {
                        console.log('EVENT TRIGGERED, \n\n\n\n')
                        //this.setState({user:    this.props.navigation.state.params.user})
                       // this.setState({speaksHeight: 0, speaksAndAnswersHeight: 0, mediaHeight:0, likedHeight: 0})
                        //console.log(this.props.navigation.state.params.user.id == this.props.id, 'MEEEEEEEEEEEh')
                        //this.forceUpdate()
                        //this.onRefresh()
                    }} onDidBlur={() => {
                        this.scroll.scrollTo({
                            x: 0,
                            y: 0,
                            animated: false
                        })
                        //this.props.dispatch(Actions.setSpeaks({speaks: [], respeaks: []}))
                        this.tabs.jumpToIndex(0)
                    }}/>
                    <ScrollView ref={ref => this.scroll = ref} scrollEnabled={true} refreshControl={
                        <RefreshControl colors={['#0AB4BE']} refreshing={false} onRefresh={this.onRefresh}/>
                    }>
                        <View style={{backgroundColor: 'white'}} onLayout={event => this.setState({profileHeight: event.nativeEvent.layout.height})}>
                            <TouchableOpacity style={{height: 120, width: '100%'}} onPressOut={() => this.setState({images: [{url: Constants.SERVER_ADDRESS + this.state.user.headerImage}], showModal: true})}>
                                <Image source={{uri: Constants.SERVER_ADDRESS + this.state.user.headerImage}} style={{width: '100%', resizeMode: 'cover', height: '100%', backgroundColor: '#f6f7f8'}}/>
                            </TouchableOpacity>
                            <TouchableOpacity style={{borderWidth: 3, borderColor: 'white', top: 75, left: 20, position: 'absolute', height: 90, width: 90, borderRadius: 60, backgroundColor: '#979797'}} onPressOut={this.showChangeAvatarModal}>
                                <Image source={{uri: Constants.SERVER_ADDRESS + this.state.user.image}} style={{height: 84, width: 84, borderRadius: 60, backgroundColor: '#f6f7f8'}}/>
                            </TouchableOpacity>
                            <View style={{width: '100%', marginTop: 20, height: 35, flexDirection: 'row'}}>
                                <View style={{flex: 1}}/>
                                {
                                    this.props.id == this.props.navigation.state.params.user.id ? null : (
                                        <>
                                            <TouchableOpacity style={{width: 120, height: 35, borderWidth: 2, borderColor: '#0AB4BE', borderRadius: 20, justifyContent: 'center', alignItems: 'center', marginRight: 10}} onPressOut={() => {
                                                this.props.navigation.navigate('ChatScreen', {
                                                    user: this.props.navigation.state.params.user,
                                                    disableStatusUpdate: null
                                                })
                                            }}>
                                                <Text style={{fontSize: 15, color: '#0AB4BE', fontWeight: 'bold'}}>{strings('ProfileScreen.message')}</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{width: 120, height: 35, borderWidth: 2, borderColor: '#0AB4BE', borderRadius: 20, justifyContent: 'center', alignItems: 'center', marginRight: 10}} onPressOut={this.subscription}>
                                                <Text style={{fontSize: 15, color: '#0AB4BE', fontWeight: 'bold'}}>{this.state.user.following ? strings('ProfileScreen.unsub') : strings('ProfileScreen.sub')}</Text>
                                            </TouchableOpacity>
                                        </>
                                    )
                                }
                            </View>
                            <Text style={{fontSize: 26, marginTop: 5, marginLeft: 20}}>{this.state.user.name}</Text>
                            <Text style={{fontSize: 15, marginLeft: 20, marginTop: 5, color: '#979797'}}>@{this.state.user.tag}</Text>
                            {
                                this.state.user.description ? <Text style={{fontSize: 15, marginLeft: 20, marginTop: 5, marginRight: 40}}>{this.state.user.description}</Text> : null
                            }
                            <View style={{marginTop: 15, marginLeft: 20, flexDirection: 'row', alignItems: 'center'}}>
                                {
                                    this.state.user.city ? (
                                        <View style={{marginRight: 10, flexDirection: 'row'}}>
                                            <Image source={require('../Images/Point.png')} style={{height: 20, width: 20, resizeMode: 'contain'}}/>
                                            <Text style={{fontSize: 12, color: '#979797', marginLeft: 10}}>{this.state.user.city}</Text>
                                        </View>
                                    ) : null
                                }
                                <Image source={require('../Images/Group12.png')} style={{height: 20, width: 20, resizeMode: 'contain'}}/>
                                <Text style={{fontSize: 12, color: '#979797', marginLeft: 10, color: '#0AB4BE'}}>{this.state.user.email}</Text>
                            </View>
                            <View style={{marginTop: 15, marginLeft: 20, flexDirection: 'row', alignItems: 'center'}}>
                                <Image source={require('../Images/Data.png')} style={{height: 20, width: 20, resizeMode: 'contain'}}/>
                                <Text style={{fontSize: 12, color: '#979797', marginLeft: 10}}>{strings('ProfileScreen.registration')} {this.processDate()}</Text>
                            </View>
                            <Text style={{color: '#979797', fontSize: 12, marginLeft: 20, marginTop: 15, marginBottom: 15}}>
                                <Text style={{fontWeight: 'bold'}}>{this.state.user.subscribesNumber}</Text> {strings('ProfileScreen.subscribtionsCount')}
                                <Text style={{fontWeight: 'bold'}}>      {this.state.user.subscribersNumber}</Text> {strings('ProfileScreen.subsCount')}
                            </Text>
                            {
                                this.state.user?.listeners?.length ? (
                                    <View style={{marginLeft: 20, marginBottom: 15, flexDirection: 'row', alignItems: 'center'}}>
                                        <View style={{flexDirection: 'row', marginLeft: 10}}>
                                            {
                                                this.state.user.listeners.map((value, i) => {
                                                    //console.log(Constants.SERVER_ADDRESS + value.image)
                                                    return <Image source={{uri: Constants.SERVER_ADDRESS + value.image}} style={{height: 20, width: 20, resizeMode: 'cover', borderRadius: 20, position: 'absolute', top: -9, left: -10*(1-i), zIndex: 4-i}}/>
                                                })
                                            }
                                        </View>
                                        <Text style={{color: '#979797', fontSize: 12, marginLeft: 10+7*this.state.user.listeners.length}}>{strings('ProfileScreen.subscribtionsCount')} {
                                            this.state.user.listeners.map((value, i) => {
                                                if(i === this.state.user.listeners.length - 1)
                                                {
                                                    return value.name
                                                }
                                                else{
                                                    return value.name + ", "
                                                }
                                            })    
                                        }</Text>
                                    </View>
                                ) : null 
                            }
                        </View>
                        <View style={{width: '100%', height: this.state.scrollHeight}}>
                            <TabView
                                ref={ref => this.tabs = ref}
                                renderTabBar={(props) => <CustomTab half={false} {...props}/>}
                                navigationState={this.state}
                                renderScene={renderScene}
                                onIndexChange={ async (_index) => {
                                    this.setState({index: _index})
                                    //console.log(this.state.availableScreenSize)
                                    //console.log('WOW')
                                    switch(_index){
                                        case 0:{
                                            this.setState({scrollHeight: this.state.speaksHeight+52})
                                            break
                                        }
                                        case 1:{
                                            this.setState({scrollHeight: this.state.speaksAndAnswersHeight+52})
                                            break
                                        }
                                        case 2:{
                                            this.setState({scrollHeight: this.state.mediaHeight+52})
                                            break
                                        }
                                        case 3:{
                                            this.setState({scrollHeight: this.state.likedHeight+52})
                                            break
                                        }
                                        default:{
                                            break;
                                        }
                                    }
                                    //this.onRefresh(false)
                                    /*setTimeout(() => {
                                        this.scroll.scrollTo({
                                            x: 0,
                                            y: this.state.profileHeight,
                                            animated: false
                                        })
                                    }, 4)*/
                                }}
                                initialLayout={initialLayout}
                                //style={{height: '100%'}}
                                swipeEnabled={false}/>
                        </View>
                        <TouchableOpacity style={{position: 'absolute', backgroundColor:'white',top: 10, left: 10, width: 40, height: 40, borderColor: '#0AB4BE', borderWidth: 2, borderRadius: 20,justifyContent:'center', alignItems: 'center'}} onPressOut={() => {
                            //console.log(this.props.navigation.goBack)
                            //console.log(this.props.navigation.state.params.previousRoute)
                            //if(this.props.navigation.state.params.previousRoute) this.props.navigation.navigate(this.props.navigation.state.params.previousRoute, this.props.navigation.state.params.previousParams)
                            this.props.navigation.goBack(this.props.navigation.state.params.key ? this.props.navigation.state.params.key : null)
                        }}>
                            <Image source={require('../Images/back_colorized.png')}style={{height: 20, width: 20}}/>
                        </TouchableOpacity>
                    </ScrollView>
                    {
                        this.state.user.id == this.props.id ? 
                        <TouchableOpacity style={{height: 60, width: 60, backgroundColor: '#0AB4BE', position: 'absolute', bottom: 10, borderRadius: 35, right: 10, justifyContent: 'center', alignItems: 'center'}}
                        onPressOut={() => {
                                        this.props.navigation.navigate('CreateNewSpeak', {
                                            parentUpdate: () => this.onRefresh(false)
                                        })
                                    }}>
                        <Image source={require('../Images/button_speak.png')} style={{height: 60, width: 60, alignSelf: 'center'}}/>
                        </TouchableOpacity> : null
                    }
                    <ModalNative onRequestClose={() => this.setState({showModal: false})} visible={this.state.showModal} transparent={false}>
                        <ImageViewer
                                loadingRender={() => <Text>Loading...</Text>}
                                imageUrls={this.state.images}
                                enableSwipeDown={true}
                                index={this.state.index}
                                onSwipeDown={() => this.setState({showModal: false})}/>
                    </ModalNative>
                    <Modal
                      visible={this.state.showChangeAvatar}
                      modalAnimation={new FadeAnimation({
                        initialValue: 0, // optional
                        animationDuration: 150, // optional
                        useNativeDriver: true, // optional
                      })}
                      onTouchOutside={() => this.setState({showChangeAvatar: false})}
                      onHardwareBackPress={() => {
                        this.setState({showChangeAvatar: false})
                        return true
                      }}
                      modalTitle={<ModalTitle title={strings('ProfileScreen.modalTitle')} textStyle={{color: '#0AB4BE', fontWeight: 'bold'}} hasTitleBar={false}/>}
                    >
                      <ModalContent style={{height: 120}}>
                        <TouchableOpacity onPressOut={() => {
                            //alert('Open avatar pressed!');
                            this.setState({showModal: true, showChangeAvatar: false, images: [{url: Constants.SERVER_ADDRESS + this.state.user.image}]})
                        }} style={{height: 40, width: 250}}>
                            <Text style={{color: 'black', fontSize: 18, fontWeight: 'normal', textAlign: 'center'}}>{strings('ProfileScreen.modalOpenPhoto')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPressOut={() => {
                            //alert('Upload from gallery pressed!');
                            this.uploadFromGallery()
                        }} style={{height: 40, width: 250}}>
                            <Text style={{color: 'black', fontSize: 18, fontWeight: 'normal', textAlign: 'center'}}>{strings('ProfileScreen.modalUploadFromGallery')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPressOut={() => {
                            //alert('Upload from camera pressed!');
                            this.uploadFromCamera()
                        }} style={{height: 40, width: 250}}>
                            <Text style={{color: 'black', fontSize: 18, fontWeight: 'normal', textAlign: 'center'}}>{strings('ProfileScreen.modalUploadFromCamera')}</Text>
                        </TouchableOpacity>
                      </ModalContent>
                    </Modal>
                </View>
            
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default connect(state => ({user: state.user, id: state.id, token: state.token, speaks: state.userSpeaks}))(Profile)

/*
<ModalButton onPress={() => this.setState({showModal: true, showChangeAvatar: false, images: [{url: Constants.SERVER_ADDRESS + this.state.user.image}]})} text={strings('ProfileScreen.modalOpenPhoto')} style={{height: 50, width: 250}} textStyle={{color: 'black', fontSize: 18, fontWeight: 'normal'}}/>
                        <ModalButton onPress={this.uploadFromGallery} text={strings('ProfileScreen.modalUploadFromGallery')} style={{height: 50, width: 250}} textStyle={{color: 'black', fontSize: 18, fontWeight: 'normal'}} align="left"/>
                        <ModalButton onPress={this.uploadFromCamera} text={strings('ProfileScreen.modalUploadFromCamera')} style={{height: 50, width: 250}} textStyle={{color: 'black', fontSize: 18, fontWeight: 'normal'}} align="left"/>*/
