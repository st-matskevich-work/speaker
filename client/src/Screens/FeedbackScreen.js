import React from 'react'
import {View, Text, StatusBar, TouchableOpacity, KeyboardAvoidingView, Animated, Image, ScrollView} from 'react-native'
import SafeAreaView from 'react-native-safe-area-view'
import CustomInput from '../Components/CustomInput'
import CustomHeader from '../Components/CustomHeader'
import VoiceRecorder from '../Components/VoiceRecorderButton'
import AudioSyncBlock from '../Components/AudioSyncBlock'
import ImageViewer from '../Components/ImageViewerComponent'
import ImagePicker from 'react-native-image-crop-picker';
import {check, PERMISSIONS, RESULTS, request} from 'react-native-permissions';
import { connect } from 'react-redux'
import {DotIndicator} from 'react-native-indicators';
import * as Actions from '../Actions/Actions'
import * as SpeaksAPI from '../API/Speaks'
import { strings } from '../../translation/i18n'

class FeedbackScreen extends React.Component{
    state = {
        text: "",
        uploading: false,
        voiceAttachments: [],
        images: [],
        attachButtonsPosition: new Animated.Value(0),
        player: ""
    }

    addVoice = (audio) => {
        this.state.text = ""
        this.state.voiceAttachments.push(audio)

        Animated.timing(
            this.state.attachButtonsPosition,
            {
                duration: 500,
                toValue: -200
            }
        ).start()

        this.forceUpdate()
    }

    removeVoice = (i) => {
        this.state.voiceAttachments.splice(i, 1)

        Animated.timing(
            this.state.attachButtonsPosition,
            {
                duration: 500,
                toValue: 0
            }
        ).start()

        this.forceUpdate()
    }

    uploadFromGallery = () => {
        ImagePicker.openPicker({
            cropping: true,
            includeBase64: true
        }).catch(error => console.log(error))
        .then(photo => {
            //console.log(photo.mime)
            this.state.images.push({
                url: photo.path,
                props: {
                    base64: photo.data,
                    extension: photo.mime.split('/')[1]
                }
            })

            Animated.timing(
                this.state.attachButtonsPosition,
                {
                    duration: 500,
                    toValue: -200
                }
            ).start()

            this.forceUpdate()
        })
        .catch(error => console.log(error))
    }

    uploadFromCamera = () => {
        check(PERMISSIONS.ANDROID.CAMERA)
        .then(result => {
          switch(result){
            case RESULTS.UNAVAILABLE:
            console.log(
              'This feature is not available (on this device / in this context)',
            );
            break;
          case RESULTS.DENIED:
          {
            request(PERMISSIONS.ANDROID.CAMERA)
            .then(result => {
                if(result == 'granted'){
                    ImagePicker.openCamera({
                        cropping: true,
                        includeBase64: true
                    }).catch(error => console.log(error))
                    .then(photo => {
                        //console.log(photo.mime)
                        this.state.images.push({
                            url: photo.path,
                            props: {
                                base64: photo.data,
                                extension: photo.mime.split('/')[1]
                            }
                        })

                        Animated.timing(
                            this.state.attachButtonsPosition,
                            {
                                duration: 500,
                                toValue: -200
                            }
                        ).start()

                        this.forceUpdate()
                    })
                    .catch(error => console.log(error))
                }
            })
            break;
          }
          case RESULTS.GRANTED:
          {
            ImagePicker.openCamera({
                cropping: true,
                includeBase64: true
            }).catch(error => console.log(error))
            .then(photo => {
                //console.log(photo.mime)
                this.state.images.push({
                    url: photo.path,
                    props: {
                        base64: photo.data,
                        extension: photo.mime.split('/')[1]
                    }
                })
                this.forceUpdate()

                Animated.timing(
                    this.state.attachButtonsPosition,
                    {
                        duration: 500,
                        toValue: -200
                    }
                ).start()
            })
            .catch(error => console.log(error)) 
          }
          case RESULTS.BLOCKED:
            console.log('The permission is denied and not requestable anymore');
            break;
          }
        })
    }

    sendToAdmin = () => {
        console.log(this.state)
        if(!this.state.text.trim().length && !this.state.voiceAttachments.length && !this.state.images.length) return
        
        let base64Attach = this.state.voiceAttachments.map((value, i) => {
            return {
                data: value.base64,
                type: 'wav'
            }
        })

        this.state.images.map((value, i) => {
            base64Attach.push({
                data: value.props.base64,
                type: value.props.extension
            })
        })

        //console.log(base64Attach)
        
        this.setState({uploading: true})

        SpeaksAPI.addAttachments(base64Attach, this.props.id, this.props.token)
        .then(res => {
            console.log(res)
            if(res.statusCode === 200){


                try {
                    this.props.socket.emit('send_message', {
                        sender: this.props.id,
                        admin: true,
                        message: this.state.text ? this.state.text : this.state.voiceAttachments.length ? 'Голосовое сообщение' : this.state.text,
                        createdAt: new Date(),
                        attachment_id: res.id
                    })
                } catch{
                    this.props.screenProps.socketInit()
        
                    this.props.socket.emit('send_message', {
                        sender: this.props.id,
                        admin: true,
                        message: this.state.text ? this.state.text : this.state.voiceAttachments.length ? 'Голосовое сообщение' : this.state.text,
                        createdAt: new Date(),
                        attachment_id: res.id
                    })
                }

                this.input.clearInput()
            }
        })
        .catch(error => console.log(error))
        .finally(() => {
            this.setState({uploading: false})
            this.setState({
                text: "",
                uploading: false,
                voiceAttachments: [],
                images: [],
                attachButtonsPosition: new Animated.Value(0),
                player: ""
            })
        })
    }

    render(){
        StatusBar.setBarStyle('dark-content')
        return(
            <SafeAreaView style={{flex: 1}}>
                <CustomHeader title={strings('FeedbackScreen.header')} navigation={this.props.navigation}/>
                <KeyboardAvoidingView style={{flex: 1}}>
                    <ScrollView contentContainerStyle={{flexGrow: 1, padding: 20}}>
                        <Text style={{color: '#0AB4BE', fontSize: 20}}>{strings('FeedbackScreen.screenTitle')}</Text>
                        <Text style={{fontSize: 15, marginTop: 5, marginBottom: 20}}>{strings('FeedbackScreen.screenDescription')}</Text>
                        {
                            this.state.voiceAttachments.length ? (
                                null
                            ) : (
                                <View>
                                    <CustomInput 
                                                onRef={ref => this.input = ref}
                                                style={{fontSize: 18}} 
                                                placeholder={strings('FeedbackScreen.placeholder')} 
                                                type="none" 
                                                onChangeText={(text) => this.setState({text: text})} 
                                                multiline={true}
                                                maxLength={1024}/>
                                    <ImageViewer 
                                                images={this.state.images}
                                                deleteImage={(index) => {
                                                    this.state.images.splice(index, 1)

                                                    if(!this.state.images.length){
                                                        Animated.timing(
                                                            this.state.attachButtonsPosition,
                                                            {
                                                                duration: 200,
                                                                toValue: 0
                                                            }
                                                        ).start()
                                                    }

                                                    this.forceUpdate()
                                                }}/>
                                </View>
                            )
                        }
                        <AudioSyncBlock
                                        voiceAttachments={this.state.voiceAttachments}
                                        player={this.state.player}
                                        setNewPlayer={(player) => {
                                            this.setState({player: player})
                                        }}
                                        removeVoice={this.removeVoice}
                                        allowDeleting={true}
                        />
                        <View style={{flex: 1}}/>
                    </ScrollView>
                    <Animated.View style={{height: 60, flexDirection: 'row', alignItems: 'center', borderTopWidth: 1, borderTopColor: '#EDEDED', overflow: 'visible', marginLeft: this.state.attachButtonsPosition}}>
                        <VoiceRecorder onRecorded={this.addVoice}/>
                        <TouchableOpacity style={{marginLeft: 30, marginRight: 10, zIndex: 1}} onPressOut={this.uploadFromGallery}>
                            <Image source={require('../Images/gallery.png')} style={{height: '100%', width: 28, resizeMode: 'contain'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={{marginLeft: 20, marginRight: 10}} onPressOut={this.uploadFromCamera}>
                            <Image source={require('../Images/photo.png')} style={{height: '100%', width: 30, resizeMode: 'contain'}}/>
                        </TouchableOpacity>
                        <View style={{flex: 1}}/>
                        <TouchableOpacity onPressOut={this.sendToAdmin} style={{marginRight: 15}}>
                        {
                            this.state.uploading ? <DotIndicator count={3} color="#0AB4BE" size={8}/> : <Text style={{color: '#0AB4BE', fontSize: 16, fontWeight: 'bold'}}>{strings('FeedbackScreen.send')}</Text>
                        }
                        </TouchableOpacity>
                    </Animated.View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

export default connect(state => ({socket: state.socket, id: state.id, token: state.token}))(FeedbackScreen)