import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import {View, Text, StyleSheet, Image, TouchableOpacity, Dimensions, RefreshControl} from 'react-native'
import { NavigationEvents } from 'react-navigation'
import * as Constants from '../Constants/Constants'
import { TabView, SceneMap } from 'react-native-tab-view';
import CustomTab from '../Components/CustomProfileTab'
import NotificationList from './NotificationsList' 
import * as UsersAPI from '../API/Users'
import * as Actions from '../Actions/Actions'
import { connect } from 'react-redux'
import PushNotification from 'react-native-push-notification'
const initialLayout = { width: Dimensions.get('window').width };
import { strings } from '../../translation/i18n'

class Notifications extends React.Component{
    state = {
        routes: [
            {
                key: 'all',
                title: strings('NotificationsScreen.allTitle')
            },
            {
                key: 'mentions',
                title: strings('NotificationsScreen.mentionsTitle')
            }
        ],
        index: 0,
        mentions: [],
        refreshing: false
    }

    fetchNotifications = (callback) => {
        
        UsersAPI.getMensions(this.props.id, this.props.token)
        .then(async res => {
            console.log(res, '\n\n\n')
            if(res.statusCode === 200){
                this.props.dispatch(await Actions.setMentions(res.mentions))

                UsersAPI.getNotifications(this.props.id, this.props.token)
                .then(async res => {
            //console.log(res)
                    if(res.statusCode === 200){
                        //console.log(res.notifications, 'ALL')
                        this.props.dispatch(await Actions.setNotifications(res.notifications))
                        this.forceUpdate()
                    }
                })
                .catch(err => {
                    
                })
            }
        })
        .catch(err => {
            
        })
        .finally(() => {
            this.setState({refreshing: false})
            if(callback) callback()
        })
    }

    onRefresh = () => {
        this.setState({refreshing: true})
        this.fetchNotifications()
    }

    render(){
        this.state.routes[0].title = strings('NotificationsScreen.allTitle')
        this.state.routes[1].title = strings('NotificationsScreen.mentionsTitle')
        const renderScene = ({route}) => {
            switch(route.key){
                case 'all':
                    return <NotificationList title={strings('NotificationsList.emptyListTitle')} body={strings('NotificationsList.emptyListHeader')} type="all" onRefresh={this.fetchNotifications} navigation={this.props.navigation} data={this.props.notifications.all}/>;
                case 'mentions':
                    return <NotificationList title={strings('NotificationsList.emptyListTitle')} body={strings('NotificationsList.emptyListHeader')} type="mentions" onRefresh={this.fetchNotifications} navigation={this.props.navigation} data={this.props.notifications.mentions}/>;
            }
        }

        return(
            <SafeAreaView style={styles.container}>
                <NavigationEvents onWillFocus={() => {
                    console.log('FOCUSED')
                    this.props.screenProps.popupNotifications?.map((value, i) => {
                        console.log(value, 'CANCEL')
                        //PushNotification.cancelAllLocalNotifications()
                        //PushNotification.cancelLocalNotifications({id: ""+value})
                        PushNotification.clearLocalNotification(value)
                        console.log('Done')
                    })
                    this.fetchNotifications()
                }}/>
                <View style={{height: 60, backgroundColor: 'white', elevation: 1, flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={{uri: Constants.SERVER_ADDRESS + this.props.user.image}} style={{height: 36, width: 36, borderRadius: 25, marginLeft: 15, marginRight: 15, backgroundColor: '#f6f7f8'}}/>
                    <Text style={{fontSize: 18}}>{strings('NotificationsScreen.header')}</Text>                 
                </View>
                <TabView
                        renderTabBar={(props) => <CustomTab {...props} half={true}/>}
                        navigationState={this.state}
                        onIndexChange={(index) => {
                            this.setState({index: index})
                        }}
                        initialLayout={initialLayout}
                        renderScene={renderScene}/>
                <TouchableOpacity style={{height: 60, width: 60, backgroundColor: '#0AB4BE', position: 'absolute', bottom: 10, borderRadius: 35, right: 10, justifyContent: 'center', alignItems: 'center'}}
                                    onPressOut={() => {
                                        this.props.navigation.navigate('CreateNewSpeak')
                                    }}>
                        <Image source={require('../Images/button_speak.png')} style={{height: 60, width: 60, alignSelf: 'center'}}/>
                </TouchableOpacity>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
    }
})

export default connect(state => ({user: state.user, id: state.id, token: state.token, notifications: state.notifications}))(Notifications)
