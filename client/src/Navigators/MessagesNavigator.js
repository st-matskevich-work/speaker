import {createStackNavigator, TransitionPresets} from 'react-navigation-stack'
import MessageList from '../Screens/MessageListScreen'
import AddNewChatScreen from '../Screens/AddNewChatScreen'

const MessagesNavigator = createStackNavigator({
    MessageList: {
        screen: MessageList
    },
    AddNewChat: {
        screen: AddNewChatScreen
    }
}, {
    headerMode: 'none',
    keyboardHandlingEnabled: false,
    defaultNavigationOptions: {
        ...TransitionPresets.ScaleFromCenterAndroid
    },
})

export default MessagesNavigator
