import {createBottomTabNavigator} from 'react-navigation-tabs'
import ProfileScreen from '../Screens/ProfileScreen'
import SearchScreen from '../Screens/SearchScreen'
import NotificationsScreen from '../Screens/NotificationsScreen'
import MessagesNavigation from '../Navigators/MessagesNavigator'
import TabBar from '../Components/TabBar'
import React from 'react'
import {createStackNavigator, TransitionPresets} from 'react-navigation-stack'
import CreateNewSpeak from '../Screens/CreateNewSpeak'

const ProfileNavigator = createStackNavigator({
    Profile:{
        screen: ProfileScreen,
        navigationOptions: {
            headerShown: false
        }
    },
    CreacteNewSpeak: {
        screen: CreateNewSpeak,
        navigationOptions: {
            headerShown: false
        }
    }
}, {
    defaultNavigationOptions: {
        ...TransitionPresets.ScaleFromCenterAndroid
    },
})

export default ProfileNavigator