import {createStackNavigator, TransitionPresets} from 'react-navigation-stack'
import AddPhotoScreen from '../Screens/AccountSetup/AddPhotoScreen'
import ChooseHeaderScreen from '../Screens/AccountSetup/ChooseHeaderScreen'
import SetUsernameScreen from '../Screens/AccountSetup/SetUsernameScreen'
import DescribeYourselfScreen from '../Screens/AccountSetup/DescribeYourselfScreen'
import YourLocationScreen from '../Screens/AccountSetup/YourLocationScreen'
import FinalScreenSetup from '../Screens/AccountSetup/FinalSetupScreen'

const SetupAccount = createStackNavigator({
    AddPhoto: {
        screen: AddPhotoScreen
    },
    SetUsername:{
        screen: SetUsernameScreen
    },
    ChooseHeader: {
        screen: ChooseHeaderScreen
    },
    DescribeYourself:{
        screen: DescribeYourselfScreen
    },
    YourLocation:{
        screen: YourLocationScreen
    },
    FinalScreen: {
        screen: FinalScreenSetup
    }
}, {
    defaultNavigationOptions: {
        ...TransitionPresets.ScaleFromCenterAndroid
    },
})

export default SetupAccount