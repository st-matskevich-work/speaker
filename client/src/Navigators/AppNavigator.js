import {createSwitchNavigator, createAppContainer} from 'react-navigation'
import AuthNavigator from './AuthNavigator'
import MainNavigation from './MainNavigator'

const SwitchNavigationConfig = createSwitchNavigator({
    Auth: {
        screen: AuthNavigator
    },
    Main: {
        screen: MainNavigation
    }
}, {

})

export default createAppContainer(SwitchNavigationConfig)