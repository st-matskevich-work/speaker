import {createDrawerNavigator} from 'react-navigation-drawer'
import {createStackNavigator, TransitionPresets} from 'react-navigation-stack'
import SideMenu from '../Components/SideBar'
import React from 'react'
import RulesScreen from '../Screens/RulesScreen'
import FeedbackScreen from '../Screens/FeedbackScreen'
import ProfileScreen from '../Screens/ProfileScreen'
import BottomTabs from './BottomTabNavigator'
import CreateNewSpeak from '../Screens/CreateNewSpeak'
import SpeakScreen from '../Screens/SpeakScreen'
import ChangeLanguageScreen from '../Screens/ChangeLanguageScreen'
import ChatScreen from '../Screens/ChatScreen'

const DrawerNavigation = createDrawerNavigator({
    BottomNavigation: {
        screen: BottomTabs
    },
    Rules: {
        screen: RulesScreen,
        navigationOptions: {
            drawerLockMode: 'locked-closed'
        }
    },
    Feedback:{
        screen: FeedbackScreen,
        navigationOptions: {
            drawerLockMode: 'locked-closed'
        }
    },
    ChangeLanguage: {
        screen: ChangeLanguageScreen,
        navigationOptions: {
            drawerLockMode: 'locked-closed'
        }
    }
}, {
    contentComponent: props => <SideMenu {...props}/>,
    edgeWidth: 100,
})

const MainNavigator = createStackNavigator({
    Drawer: {
        screen: DrawerNavigation,
        navigationOptions: {
            headerShown: false
        }
    },
    Profile: {
        screen: ProfileScreen,
        navigationOptions: {
            headerShown: false
        }
    },
    CreateNewSpeak: {
        screen: CreateNewSpeak,
        navigationOptions:{
            headerShown: false
        }
    },
    SpeakScreen: {
        screen: SpeakScreen,
        navigationOptions:{
            headerShown: false,
        }
    },
    ChatScreen: {
        screen: ChatScreen,
        navigationOptions:{
            headerShown: false,
        }
    },
}, {
    defaultNavigationOptions: {
        ...TransitionPresets.ScaleFromCenterAndroid
    },
    keyboardHandlingEnabled: false,
})

export default MainNavigator