import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { TransitionPresets } from 'react-navigation-stack'
import MessagesScreen from '../Navigators/MessagesNavigator'
import NotificationsScreen from '../Screens/NotificationsScreen'
import Feed from '../Screens/Feed'
import SearchScreen from '../Screens/SearchScreen'
import {Image} from 'react-native' 
import React from 'react'

const BottomTab = createBottomTabNavigator({
    Feed: {
        screen: Feed,
        navigationOptions:{
            tabBarColor: 'white',
            tabBarIcon: (props) => {
                if(props.focused){
                    return <Image source={require('../Images/TabIcons/feed_focused.png')} style={{height: 25, resizeMode: 'contain', alignSelf: 'center'}}/>
                }else{
                    return <Image source={require('../Images/TabIcons/feed_unfocused.png')} style={{height: 25, resizeMode: 'contain'}}/>
                }
            }
        }
    },
    Search: {
        screen: SearchScreen,
        navigationOptions:{
            tabBarColor: 'white',
            tabBarIcon: (props) => {
                if(props.focused){
                    return <Image source={require('../Images/TabIcons/search_focused.png')} style={{height: 25, resizeMode: 'contain', alignSelf: 'center'}}/>
                }else{
                    return <Image source={require('../Images/TabIcons/search_unfocused.png')} style={{height: 25, resizeMode: 'contain'}}/>
                }
            }
        },
        tabBarBadge: 'btuh'
    },
    Notifications:{
        screen: NotificationsScreen,
        navigationOptions:{
            tabBarColor: 'white',
            tabBarIcon: (props) => {
                if(props.focused){
                    return <Image source={require('../Images/TabIcons/notifications_focused.png')} style={{height: 25, resizeMode: 'contain', alignSelf: 'center'}}/>
                }else{
                    return <Image source={require('../Images/TabIcons/notifications_unfocused.png')} style={{height: 25, resizeMode: 'contain'}}/>
                }
            }
        }
    },
    Messages: {
        screen:MessagesScreen,
        navigationOptions:{
            tabBarColor: 'white',
            tabBarIcon: (props) => {
                if(props.focused){
                    return <Image source={require('../Images/TabIcons/messages_focused.png')} style={{height: 25, resizeMode: 'contain', alignSelf: 'center'}}/>
                }else{
                    return <Image source={require('../Images/TabIcons/messages_unfocused.png')} style={{height: 25, resizeMode: 'contain'}}/>
                }
            }
        }
    }
}, {
    labeled: false,
    style: {
        justifyContent: 'center',
        borderTopWidth: 0
    },
    defaultNavigationOptions: {
        ...TransitionPresets.SlideFromRightIOS
    },
    tabBarOptions:{
        keyboardHidesTabBar: false,
        showLabel: false,
        style: {
            borderTopWidth: 1, 
            borderTopColor: '#EDEDED',
            height: 60
        }
    }
})

export default BottomTab