import {createSwitchNavigator, createAppContainer} from 'react-navigation'
import SetupNewAccount from './SetupNewAccount'
import LoginNavigator from './LoginNavigator'
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
import { Transition } from 'react-native-reanimated';
import React from 'react'

const AuthNavigator = createSwitchNavigator({
    Login: {
        screen: LoginNavigator,
    },
    SetupNewAccount: {
        screen: SetupNewAccount
    }
}, {
    /*transition: (
        <Transition.Together>
          <Transition.Out
            type="slide-left"
            durationMs={300}
            interpolation="easeIn"
          />
          <Transition.In type="fade" durationMs={500} />
        </Transition.Together>
      ),*/
})

export default AuthNavigator