import {createStackNavigator, TransitionPresets} from 'react-navigation-stack'
import LoginScreen from '../Screens/Auth/LoginScreen'
import RegisterScreen from '../Screens/Auth/RegisterScreen'
import ConfirmRegistrationScreen from '../Screens/Auth/ConfirmRegistrationScreen'
import SetupNewAccount from '../Navigators/SetupNewAccount'
import InputEmailScreen from '../Screens/RestorePassword/ImputEmailScreen'
import ConfirmRestoreScreen from '../Screens/RestorePassword/ConfirmRestoreScreen'
import EnterNewPasswordScreen from '../Screens/RestorePassword/EnterNewPassword'
import FinishRestoreScreen from '../Screens/RestorePassword/FinishRestoreScreen'
import ChangeLanguageScreen from '../Screens/ChangeLanguageScreen'

const LoginNavigator = createStackNavigator({
    LoginScreen: {
        screen: LoginScreen
    },
    ChangeLanguageAuth: {
        screen: ChangeLanguageScreen,
        navigationOptions: {
            headerShown: false
        }
    },
    RegisterScreen: {
        screen: RegisterScreen
    },
    ConfirmRegistrationScreen: {
        screen: ConfirmRegistrationScreen
    },
    InputEmail: {
        screen: InputEmailScreen
    },
    ConfirmRestore: {
        screen: ConfirmRestoreScreen
    },
    EnterNewPassword: {
        screen: EnterNewPasswordScreen
    },
    FinishRestore: {
        screen: FinishRestoreScreen
    }
}, {
    defaultNavigationOptions: {
        ...TransitionPresets.ScaleFromCenterAndroid
      }
})

export default LoginNavigator