import React from 'react'
import {View} from 'react-native'
import VoiceTile from '../Components/VoiceTile'
import { Player } from '@react-native-community/audio-toolkit'

export default class AudioSyncBlock extends React.Component{
    state = {
        voiceRefs: [],
        audioSeek: 0,
        currentAudioIndex: 0,
    }

    removeVoice = (i) => {
        this.state.voiceRefs.splice(i, 1)[0].deleteProcess(() => {
            this.props.removeVoice(i);
        });
    }

    render(){
        return(
            <View>
            {
                this.props.voiceAttachments.map((value, i) => {
                    return <VoiceTile onRef={ref => {
                        this.state.voiceRefs.push(ref)
                    }} play={(callback) => {
                        //console.log(this.props.player,'player', '\n\n\n\n\n\n')
                        if(this.props.player)
                        {
                            this.state.voiceRefs[this.state.currentAudioIndex].resetProgress()
                            this.props.player.destroy()
                        }

                        let audioPlayer =  new Player(value.path) 
                        audioPlayer.on('ended', () => {
                            this.state.voiceRefs[i].resetProgress()
                        })
                        
                        
                        if(this.props.syncronize) this.props.syncronize(this.state.voiceRefs[i])
                        audioPlayer.play(() => {
                            callback(audioPlayer.duration)
                        })
                        //callback(audioPlayer.duration)
                        this.setState({currentAudioIndex: i})
                        this.props.setNewPlayer(audioPlayer)
                        //console.log(this.props.player.currentTime, '\n\n\n\n\n')

                    }} pause={(callback) => {
                        this.props.player.pause()
                        this.state.audioSeek = this.props.player.currentTime
                        this.forceUpdate()
                        callback()

                    }} continue={(callback) => {
                        this.props.player.seek(this.state.audioSeek)
                        this.props.player.play()
                        callback(this.props.player.duration - this.state.audioSeek)
                    }} allowDeleting={this.props.allowDeleting} key={value?.base64 ? value.base64 : i} player={this.props.player} removeVoice={() => this.removeVoice(i)}/>
                })
            }
            </View>
        )
    }
}

/*
{
    this.state.voiceAttachments.map((value, i) => {
        
}
=======
import React from 'react'
import {View} from 'react-native'
import VoiceTile from '../Components/VoiceTile'
import { Player } from '@react-native-community/audio-toolkit'

export default class AudioSyncBlock extends React.Component{
    state = {
        voiceRefs: [],
        audioSeek: 0,
        currentAudioIndex: 0,
    }

    removeVoice = (i) => {
        this.state.voiceRefs.splice(i, 1)[0].deleteProcess(() => {
            this.props.removeVoice(i);
        });
    }

    render(){
        return(
            <View>
            {
                this.props.voiceAttachments.map((value, i) => {
                    return <VoiceTile onRef={ref => {
                        this.state.voiceRefs.push(ref)
                    }} play={(callback) => {
                        //console.log(this.props.player,'player', '\n\n\n\n\n\n')
                        if(this.props.player)
                        {
                            this.state.voiceRefs[this.state.currentAudioIndex].resetProgress()
                            //this.props.player.stop()
                            this.props.player.destroy()
                        }

                        let audioPlayer =  new Player(value.path) //Check
                        //console.log(new Player(value.path).speed)
                        audioPlayer.on('ended', () => {
                            this.state.voiceRefs[i].resetProgress()
                        })
                        
                        
                        if(this.props.syncronize) this.props.syncronize(this.state.voiceRefs[i])
                        audioPlayer.play(() => {
                            callback(audioPlayer.duration)
                        })
                        //callback(audioPlayer.duration)
                        this.setState({currentAudioIndex: i})
                        this.props.setNewPlayer(audioPlayer)
                        //console.log(this.props.player.currentTime, '\n\n\n\n\n')

                    }} pause={(callback) => {
                        this.props.player.pause()
                        this.state.audioSeek = this.props.player.currentTime
                        this.forceUpdate()
                        callback()

                    }} continue={(callback) => {
                        this.props.player.seek(this.state.audioSeek)
                        this.props.player.play()
                        callback(this.props.player.duration - this.state.audioSeek)
                    }} allowDeleting={this.props.allowDeleting} key={value?.base64 ? value.base64 : i} player={this.props.player} removeVoice={() => this.removeVoice(i)}/>
                })
            }
            </View>
        )
    }
}

/*
{
    this.state.voiceAttachments.map((value, i) => {
        
}
>>>>>>> 63f034d080f31b1c0bb0ef0be3bed266eaa0bc28
*/