import React from 'react'
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native'
import SafeAreaView from 'react-native-safe-area-view'
import {connect} from 'react-redux'
import * as Constants from '../Constants/Constants'
import * as AuthAPI from '../API/Auth'
import * as Actions from '../Actions/Actions'
import { strings } from '../../translation/i18n'

class SideMenu extends React.Component{
    logout = () => {
        let token = this.props.token
        let id = this.props.id
        this.props.dispatch(Actions.setID(""))
        this.props.dispatch(Actions.setToken(""))
        this.props.navigation.navigate('LoginScreen')
        AuthAPI.logout(token, id)
        .then(res => {
            if(res.statusCode === 200){
                Actions.writeToken("")
                Actions.writeID("")
            }else{
                this.props.dispatch(Actions.setToken(token))
                this.props.dispatch(Actions.setID(id))
            }
        })
        .catch(err => {
            this.props.dispatch(Actions.setToken(token))
            this.props.dispatch(Actions.setID(id))
        })
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                <View style={styles.imageZone}>
                    <TouchableOpacity onPressOut={() => {
                        this.props.navigation.closeDrawer()
                        this.props.navigation.navigate('Profile', {
                            user: this.props.user
                        })
                    }}>
                        <Image source={{uri: Constants.SERVER_ADDRESS + this.props.user.image}} style={{height: 54, width: 54, borderRadius: 60, marginTop: 8, marginLeft: 20, backgroundColor: '#f6f7f8'}}/>
                    </TouchableOpacity>
                    <View style={{flex: 1}}/>
                    <Image source={{uri: Constants.SERVER_ADDRESS + this.props.user.image}} style={{height: 38, width: 38, borderRadius: 60, marginRight: 20, backgroundColor: '#f6f7f8'}}/>
                </View>
                <Text style={{marginLeft: 20, fontSize: 18}}>{this.props.user.name}</Text>
                <Text style={{marginLeft: 20, fontSize: 14, marginTop: 1}}>@{this.props.user.tag}</Text>
                <Text style={{marginLeft: 20, marginTop: 10, marginBottom: 10}}>{this.props.user.subscribesNumber} {strings('SideBar.subscribtionsCount')}     {this.props.user.subscribersNumber} {strings('SideBar.subsCount')} </Text>
                <View style={{borderTopWidth: 1, borderBottomWidth: 1, borderBottomColor: '#E9E5E5', borderTopColor: '#E9E5E5'}}>
                    <TouchableOpacity style={[styles.button, {marginTop: 10}]} onPressOut={() => {
                        this.props.navigation.closeDrawer()
                        this.props.navigation.navigate('Profile', {
                            user: this.props.user
                        })
                    }}>
                        <Image style={styles.buttonImage} source={require('../Images/user.png')}/>
                        <Text style={styles.buttonText}>{strings('SideBar.profile')} </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPressOut={() => this.props.navigation.navigate('Rules')}>
                        <Image style={styles.buttonImage} source={require('../Images/rules.png')}/>
                        <Text style={styles.buttonText}>{strings('SideBar.rules')} </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.button]} onPressOut={() => this.props.navigation.navigate('Feedback')}>
                        <Image style={styles.buttonImage} source={require('../Images/reverse.png')}/>
                        <Text style={styles.buttonText}>{strings('SideBar.callback')} </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.button, {marginBottom: 10}]} onPressOut={() => this.props.navigation.navigate('ChangeLanguage')}>
                        <Image style={styles.buttonImage} source={require('../Images/language.png')}/>
                        <Text style={styles.buttonText}>{strings('SideBar.language')} </Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={[styles.button, {marginBottom: 10}]} onPressOut={this.logout}>
                    <Image style={styles.buttonImage} source={require('../Images/exit.png')}/>
                    <Text style={styles.buttonText}>{strings('SideBar.exit')} </Text>
                </TouchableOpacity>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        width: '100%',
        height: '100%'
    },
    button:{
        width: '100%',
        height: 50,
        alignItems: 'center',
        flexDirection: 'row'
    },
    buttonText:{
        fontSize: 18
    },
    buttonImage:{
        width: 20,
        height: 20,
        backgroundColor: 'white',
        marginLeft: 20,
        marginRight: 20,
        alignSelf: 'center'
    },
    imageZone: {
        width: '100%',
        marginTop: 5,
        height: 70,
        flexDirection: 'row',
        marginBottom: 10
    }
})


export default connect(state => ({user: state.user, token: state.token, id: state.id}))(SideMenu)