import React from 'react'
import { Modal, View, TouchableOpacity, Image, Text } from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import { PlaceholderContainer, Placeholder } from 'react-native-loading-placeholder';
import LinearGradient from 'react-native-linear-gradient';
import RNFetchBlob from 'rn-fetch-blob'

const Gradient = () => {
  return (
    <LinearGradient
      colors={['#eeeeee', '#dddddd', '#eeeeee']}
      start={{ x: 1.0, y: 0.0 }}
      end={{ x: 0.0, y: 0.0 }}
      style={{
        flex: 1,
        width: 120
      }}
    />
  );
};

export default class ImageViewerBundle extends React.Component {
    state = {
        showModal: false,
        index: 0,
        height: 0,
        imageLoaded: []
    }

    

    render(){
        return (
            <View>
                {
                    this.props.images.map((value, i) => {
                        //if(!value) return 
                        //console.log(value)
                            //AsyncImage.props.children[0].props.onLoadEnd()
                        let promise = new Promise(async resolve => {
                            //!value.url.includes('file://')
                            let path = RNFetchBlob.fs.dirs.DocumentDir + value.url.split('/')[value.url.split('/').length - 1];
                            //console.log( await RNFetchBlob.fs.exists(RNFetchBlob.fs.dirs.DocumentDir + value.url.split('/')[value.url.split('/').length - 1]), '\n\n\n\n')
                            //if(!value.url.includes('file://') || await RNFetchBlob.fs.exists(RNFetchBlob.fs.dirs.DocumentDir + value.url.split('/')[value.url.split('/').length - 1])){
                            if(0){
                                RNFetchBlob
                                .config({
                                  // add this option that makes response data to be stored as a file,
                                  // this is much more performant.
                                  fileCache : false,
                                  path: RNFetchBlob.fs.dirs.DocumentDir + value.url.split('/')[value.url.split('/').length - 1]
                                })
                                .fetch('GET', value.url, {
                                    Accept: 'application/json',
                                    'Content-Type': 'application/json'
                                })
                                .then((res) => {
                                  // the temp file path
                                  console.log(res.path())
                                  resolve(
                                    <View>
                                        <Image ref={ref => this.imageRef = ref} source={{uri: Platform.OS === 'android' ? 'file://' + res.path() : '' + res.path()}} style={{resizeMode: 'cover', height: '100%', width: '100%'}}/>
                                        {
                                            this.props.deleteImage ? (
                                                <TouchableOpacity style={{position: 'absolute', top: 10, right: 10, backgroundColor: 'rgba(255,255,255,0.5)', height: 20, width: 20, borderRadius: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: '#0AB4BE'}} onPressOut={() => this.props.deleteImage(i)}>
                                                    <Image source={require('../Images/close.png')} style={{height: 14, width: 14, resizeMode: 'contain'}}/>
                                                </TouchableOpacity>
                                            ) : null
                                        }
                                    </View>
                                  )
                                })
                            }
                            else{
                                resolve(
                                    <View>
                                        <Image ref={ref => this.imageRef = ref} source={value.url ? {uri: value.url} : {uri: path}} style={{resizeMode: 'cover', height: '100%', width: '100%'}}/>
                                        {
                                            this.props.deleteImage ? (
                                                <TouchableOpacity style={{position: 'absolute', top: 10, right: 10, backgroundColor: 'rgba(255,255,255,0.5)', height: 20, width: 20, borderRadius: 10, justifyContent: 'center', alignItems: 'center'}} onPressOut={() => this.props.deleteImage(i)}>
                                                    <Image source={require('../Images/close.png')} style={{height: 14, width: 14, resizeMode: 'contain'}}/>
                                                </TouchableOpacity>
                                            ) : null
                                        }
                                    </View>
                                )
                            }
                        })


                        return (
                            <TouchableOpacity style={{width: '100%', height: this.state.height, borderRadius: 20, overflow: 'hidden', marginBottom: 10}} activeOpacity={1} key={value.url} onPress={() => this.setState({index: i, showModal: true})} onLayout={(event) => this.setState({height: event.nativeEvent.layout.width * 9/16})}>
                                <Image ref={ref => this.imageRef = ref} source={value.url ? {uri: value.url} : value?.params?.source} style={{resizeMode: 'cover', height: '100%', width: '100%', backgroundColor: '#f6f7f8'}}/>
                                {
                                    this.props.deleteImage ? (
                                        <TouchableOpacity style={{position: 'absolute', top: 10, right: 10, backgroundColor: 'rgba(255,255,255,0.5)', height: 20, width: 20, borderRadius: 10, justifyContent: 'center', alignItems: 'center'}} onPressOut={() => this.props.deleteImage(i)}>
                                            <Image source={require('../Images/close.png')} style={{height: 14, width: 14, resizeMode: 'contain'}}/>
                                        </TouchableOpacity>
                                    ) : null
                                }
                            </TouchableOpacity>
                        )
                    })
                }
                <Modal onRequestClose={() => this.setState({showModal: false})} visible={this.state.showModal} transparent={false}>
                    <ImageViewer
                            loadingRender={() => <Text>Loading...</Text>}
                            imageUrls={this.props.images}
                            enableSwipeDown={true}
                            index={this.state.index}
                            onSwipeDown={() => this.setState({showModal: false})}/>
                </Modal>
            </View>
        )
    }
}