import React from 'react'
import { TabBar } from 'react-native-tab-view';
import {Text, View, Dimensions, TouchableOpacity} from 'react-native'
import Animated, { color } from 'react-native-reanimated'

export default class CustomTab extends React.Component{
    state={}

    render(){
        const inputRange = this.props.navigationState.routes.map((x, i) => i);

        return(
            <View style={{width: '100%', height: 52, flexDirection: 'row', backgroundColor: 'white'}}>
                {
                    this.props.navigationState.routes.map((route, i) => {
                        const opacity = Animated.interpolate(this.props.position, {
                            inputRange,
                            outputRange: inputRange.map(inputIndex =>
                              inputIndex === i ? 1 : 0
                            ),
                          })

                        return this.props.half ? (
                            <View style={{height: 52, width:'50%', justifyContent: 'center', alignItems: 'center'}} key={route.title}>
                                <TouchableOpacity style={{height: 50, flexGrow:1, justifyContent: 'center', alignItems: 'center'}}
                                                    onPressOut={() => this.props.jumpTo(route.key)}>
                                    <Text style={{fontSize: 16, color: '#0AB4BE', fontWeight: 'bold'}}>{route.title}</Text>
                                </TouchableOpacity>
                                <Animated.View style={{height: 2, backgroundColor: '#0AB4BE', width: '100%', opacity: opacity}}/>
                            </View>
                        ) : (
                            <View style={{height: 52, flexGrow:1, justifyContent: 'center', alignItems: 'center'}} key={route.title}>
                                <TouchableOpacity style={{height: 50, flexGrow:1, justifyContent: 'center', alignItems: 'center'}}
                                                    onPressOut={() => this.props.jumpTo(route.key)}>
                                    <Text style={{fontSize: 16, color: '#0AB4BE', fontWeight: 'bold'}}>{route.title}</Text>
                                </TouchableOpacity>
                                <Animated.View style={{height: 2, backgroundColor: '#0AB4BE', width: '100%', opacity: opacity}}/>
                            </View>
                        )
                    })
                }
            </View>
        )
    }
}

/*
<View style={{height: 52, flexGrow:1, justifyContent: 'center', alignItems: 'center'}}>
                                <TouchableOpacity style={{height: 50, flexGrow:1, justifyContent: 'center', alignItems: 'center'}}
                                                    onPress={() => this.props.jumpTo(route.key)}>
                                    <Text style={{fontSize: 16, color: '#0AB4BE', fontWeight: 'bold'}}>{route.title}</Text>
                                </TouchableOpacity>
                                <View style={{height: 2, backgroundColor: bc, width: '100%'}}/>
                            </View>
*/

/*<View style={{height: 52, flexGrow:1, justifyContent: 'center', alignItems: 'center'}}>
                    <TouchableOpacity style={{height: 50, flexGrow:1, justifyContent: 'center', alignItems: 'center'}}
                                onPress={() => this.props.jumpTo('speaks')}>
                        <Text style={{fontSize: 16, color: '#0AB4BE', fontWeight: 'bold'}}>Спики</Text>
                    </TouchableOpacity>
                    <View style={{height: 2, backgroundColor: '#0AB4BE', width: '100%', opacity: this.props.navigationState.index === 0 ? 1 : 0}}/>
                </View>
                <View style={{height: 52, flexGrow:1, justifyContent: 'center', alignItems: 'center'}}>
                    <TouchableOpacity style={{height: 50, flexGrow:1, justifyContent: 'center', alignItems: 'center'}}
                                onPress={() => this.props.jumpTo('answers')}>
                        <Text style={{fontSize: 16, color: '#0AB4BE', fontWeight: 'bold'}}>Спики и ответы</Text>
                    </TouchableOpacity>
                    <View style={{height: 2, backgroundColor: '#0AB4BE', width: '100%', opacity: this.props.navigationState.index === 1 ? 1 : 0}}/>
                </View>
                <View style={{height: 52, flexGrow:1, justifyContent: 'center', alignItems: 'center'}}>
                    <TouchableOpacity style={{height: 50, flexGrow:1, justifyContent: 'center', alignItems: 'center'}}
                                onPress={() => this.props.jumpTo('media')}>
                        <Text style={{fontSize: 16, color: '#0AB4BE', fontWeight: 'bold'}}>Медиа</Text>
                    </TouchableOpacity>
                    <View style={{height: 2, backgroundColor: '#0AB4BE', width: '100%', opacity: this.props.navigationState.index === 2 ? 1 : 0}}/>
                </View>
                <View style={{height: 52, flexGrow:1, justifyContent: 'center', alignItems: 'center'}}>
                    <TouchableOpacity style={{height: 50, flexGrow:1, justifyContent: 'center', alignItems: 'center'}}
                                onPress={() => this.props.jumpTo('liked')}>
                        <Text style={{fontSize: 16, color: '#0AB4BE', fontWeight: 'bold'}}>Нравится</Text>
                    </TouchableOpacity>
                    <View style={{height: 2, backgroundColor: '#0AB4BE', width: '100%', opacity: this.props.navigationState.index === 3 ? 1 : 0}}/>
                </View>*/