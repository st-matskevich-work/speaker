import React from 'react'
import {TouchableOpacity, Text, Image, View} from 'react-native'
import { connect } from 'react-redux'
import * as Constants from '../Constants/Constants'
import { strings } from '../../translation/i18n'

class MessageTile extends React.Component{

    render(){
        console.log(this.props.id, this.props.user.id, '\n\n\n')
        return(
            <TouchableOpacity style={{width: '100%', height: 100, flexDirection: 'row', overflow: 'hidden', backgroundColor: this.props.isRead ? '#EDF7F8' : 'white'}} onPressOut={() => this.props.navigation.navigate('ChatScreen', {
                user: this.props.user,
                disableStatusUpdate: this.props.disableStatusUpdate
            })}>
                <Image source={{uri: Constants.SERVER_ADDRESS + this.props.user.image}} style={{height: 60, width: 60, borderRadius: 30, marginLeft: 20, marginRight: 20, marginTop: 10, backgroundColor: '#f6f7f8'}}/>
                <View style={{flex: 1, marginTop: 10, marginRight: 20, marginBottom: 10}}>
                    <View style={{flexDirection: 'row-reverse', maxWidth: '100%'}}>
                        <Text style={{color: '#979797'}}>{this.props.time}</Text>
                        <View style={{flex: 1}}/>
                        <Text style={{fontSize: 18, fontWeight: 'bold'}}>{this.props.user.name}{'\n'}<Text style={{fontSize: 15, fontWeight: 'normal', color: '#979797'}}>@{this.props.user.tag}</Text></Text>
                    </View>
                    <View style={{marginTop: 10}}>
                        {
                            /*this.props.speak.voice === true ? (
                                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                    <Image source={require('../Images/play.png')} style={{height: 30, width: 30, marginRight: 10}}/>
                                    <Image source={require('../Images/waveform.png')} style={{height: 40, resizeMode: 'center', flex: 1}}/>
                                    <Text style={{color: '#979797', fontSize: 15, marginLeft: 10}}>1:20</Text>
                                </View>
                            ) : (
                                <Text>{this.props.speak.speak}</Text>
                            )*/
                            //this.props.speak.speak?.length ? <Text style={{marginBottom: 10}}>{this.props.speak.speak}</Text> : null
                            <Text style={{marginBottom: 10}}><Text style={{color: '#979797'}}>{this.props.id == this.props.lastSpeaker ? strings('MessageTile.self') : null}</Text>{this.props.lastMessage}</Text>
                        }
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

export default connect(state => ({id: state.id}))(MessageTile)
