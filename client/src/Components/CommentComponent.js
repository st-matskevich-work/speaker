import React from 'react'
import {View, Text, Image, TouchableOpacity} from 'react-native'
import moment from 'moment'
import 'moment/locale/ru'
import * as Constants from '../Constants/Constants'
import ParsedText from 'react-native-parsed-text';
import * as UsersAPI from '../API/Users'
import { comment, connect } from 'react-redux'
import * as SpeaksAPI from '../API/Speaks' 

class CommentComponent extends React.Component{
    state = {
        likes: 0,
        liked: false
    }

    componentDidMount(){
        this.setState({likes: this.props.comment.likes, liked: this.props.comment.liked})
    }
    
    processDate = () => {
        return moment(new Date(+this.props.comment.date)).startOf('minute').fromNow()
    }

    like = () => {
        this.setState({likes: this.state.liked ? this.state.likes - 1 : this.state.likes + 1, liked: !this.state.liked})

        SpeaksAPI.sendLikeRequest(this.props.comment.commentid, this.props.id, this.props.token, "comment")
        .then(res => {
            if(res.statusCode != 200) this.setState({likes: this.state.liked ? this.state.likes - 1 : this.state.likes + 1, liked: !this.state.liked})
        })
        .catch(err => this.setState({likes: this.state.liked ? this.state.likes - 1 : this.state.likes + 1, liked: !this.state.liked}))
    }

    jumpToUser = (props) => {
        props = props.split('@')[1]
        let key = Math.round( Math.random() * 10000000 );

        UsersAPI.getUserInfoByTag(this.props.id, props)
        .then(result => {
            console.log(result)
            if(result.statusCode === 200){
                this.props.navigation.push('Profile', {
                    user: result
                })
            }
        })
        .catch(err=> console.log(err))
    }

    render(){
        console.log(this.props.comment)

        return(
            <View style={{flex: 1, flexDirection: 'row'}} onLayout={this.props.onLayout}>
                <TouchableOpacity onPressOut={() => this.jumpToUser('@' + this.props.comment.tag)}>
                    <Image source={{uri: Constants.SERVER_ADDRESS + this.props.comment.image}} style={{height: 46, width: 46, borderRadius: 30, marginLeft: 20, marginRight: 20, marginTop: 10, backgroundColor: '#f6f7f8'}}/>
                </TouchableOpacity>
                <View style={{flex: 1, marginTop: 10, marginRight: 20, marginBottom: 10}}>
                    <View style={{flexDirection: 'row-reverse', maxWidth: '100%'}}>
                        <Text style={{color: '#979797'}}>{this.processDate()}</Text>
                        <View style={{flex: 1}}/>
                        <Text style={{fontSize: 18, fontWeight: 'bold'}}>{this.props.comment.name}{'\n'}<Text style={{fontSize: 15, fontWeight: 'normal', color: '#979797'}}>@{this.props.comment.tag}</Text></Text>
                    </View>
                    <View style={{marginTop: 10, flexDirection: 'row', marginBottom: 10}}>
                        <View style={{flex: 1}}>
                        {
                            this.props.comment.voice === true ? (
                                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                    <Image source={require('../Images/play.png')} style={{height: 30, width: 30, marginRight: 10}}/>
                                    <Image source={require('../Images/waveform.png')} style={{height: 40, resizeMode: 'center', flex: 1}}/>
                                    <Text style={{color: '#979797', fontSize: 15, marginLeft: 10}}>1:20</Text>
                                </View>
                            ) : (
                                <ParsedText style={{}}
                                            parse={[
                                                {
                                                    pattern: /@[a-zA-Z0-9_-]{3,32}/,
                                                    style: {color: '#0AB4BE'},
                                                    onPress: this.jumpToUser
                                                }
                                            ]}>{this.props.comment.text}
                                </ParsedText>
                            )
                        }
                        </View>
                        <View>
                            <View style={{flex: 1}}/>
                            <View style={{flexDirection: 'row'}}>
                                <View style={{flex: 1}}/>
                                <TouchableOpacity onPressOut={this.like}>
                                    <Image source={this.state.liked ? require('../Images/liked.png') : require('../Images/like.png')} style={{height: 20, width: 20, resizeMode: 'stretch'}}/>
                                </TouchableOpacity>
                                <Text style={{color: '#979797', fontSize: 15, marginLeft: 5}}>{this.state.likes}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

export default connect(state => ({id: state.id, token: state.token}))(CommentComponent)