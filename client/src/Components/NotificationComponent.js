import React from 'react'
import {View, TouchableOpacity, Text, Image} from 'react-native'
import { connect } from 'react-redux'
import * as UsersAPI from '../API/Users'
import * as Constants from '../Constants/Constants'
import * as SpeaksAPI from '../API/Speaks'
import * as Actions from '../Actions/Actions'
import { strings } from '../../translation/i18n'

class NotificationsComponent extends React.Component{
    jumpToUser = (props) => {
        if(this.state?.loading) return

        props = props.split('@')[1]
        let key = Math.round( Math.random() * 10000000 );

        this.setState({loading: true})
        UsersAPI.getUserInfoByTag(this.props.id, props)
        .then(result => {
            console.log(result)
            if(result.statusCode === 200){
                this.props.navigation.push('Profile', {
                    user: result
                })
            }
        })
        .catch(err=> console.log(err))
        .finally(() => {
            this.setState({loading: false})
        })
    }

    like = () => {
        let id;

        if(this.props.type === "mentions"){
            id = this.props.payload?.speak?.id ? this.props.payload?.speak?.id : this.props.payload?.comment?.speak_id
        }else{
            if(this.props.payload.type == "like"){
                id = this.props.payload.data.content.id
            }
            else if(this.props.payload.type == "respeak"){
                id = this.props.payload.data.respeak.id
            }
            else if(this.props.payload.type == "comment"){
                id = this.props.payload.data.speak.id
            }
        }

        this.props.dispatch(Actions.setProfileLikemark( id, this.props.payload?.user?.id ? this.props.payload?.user?.id : this.props.payload.data.user.id, this.props.id))

        

        SpeaksAPI.sendLikeRequest(id, this.props.id, this.props.token)
        .then(res => {
            if(res.statusCode != 200){
                this.props.dispatch(Actions.setProfileLikemark( id, this.props.payload?.user?.id ? this.props.payload?.user?.id : this.props.payload.data.user.id, this.props.id))
            }
        })
        .catch(err => this.props.setLikeMark(id))
    }

    toMention = () => {
        //console.log(this.props.payload.type, this.props.payload.data) 
        SpeaksAPI.getCurrentSpeak(this.props.id, !this.props.payload.type ? this.props.payload.speak ? this.props.payload.speak.id : this.props.payload.comment.speak_id : this.props.payload.type === "like" ? this.props.payload.data.content.id : this.props.payload.type === "respeak" ? this.props.payload.data.respeak.id : this.props.payload.type === "comment" ? this.props.payload.data.speak.id : null)
        .then(res => {
            console.log(res)
            if(res.statusCode === 200){
                let key = Math.round( Math.random() * 10000000 );
                
                this.props.navigation.navigate({
                    routeName:'SpeakScreen',
                    params: {
                        speak: res.speaks,
                        like: this.like,
                        key: key,
                        commentId: this.props.payload.comment ? this.props.payload.comment.id : this.props.payload.content?.comment_id ? this.props.payload.content.comment_id : null 
                    },
                    key: key
                })
            }
        })
        .catch(res => console.log(res))
    }

    render(){
        let tag, avatar, text;

        if(this.props.type == "mentions"){
            tag = this.props.payload.user.tag;
            avatar = this.props.payload.user.avatar;
        }else{
            tag = this.props.payload.data.user.tag;
            avatar = this.props.payload.data.user.avatar;

            switch(this.props.payload.type){
                case 'like': {
                    text = this.props.payload.data.content.content_type === "speak" ? strings('NotificationComponent.likeSpeak') : strings('NotificationComponent.likeComment')
                    break
                }
                case 'respeak':{
                    text = strings('NotificationComponent.respeak')
                    break
                }
                case 'subscription': {
                    text = strings('NotificationComponent.subscription')
                    break;
                }
                case 'comment': {
                    text = strings('NotificationComponent.comment') + '"' + this.props.payload.data.comment.text + '"'
                }
            }

            console.log(text, this.props.payload.data?.respeak)
        }

        return (
            <TouchableOpacity style={{width: '100%', flexDirection: 'row', backgroundColor: 'white', marginBottom: 5}} onPressOut={this.toMention} disabled={this.props.payload.type === "subscription"}>
                <View>
                    <TouchableOpacity onPressOut={() => this.jumpToUser('@' + tag)}>
                        <Image source={{uri: Constants.SERVER_ADDRESS + avatar}} style={{height: 46, width: 46, borderRadius: 30, marginLeft: 20, marginRight: 20, marginTop: 10, backgroundColor: '#f6f7f8'}}/>
                    </TouchableOpacity>
                    <View style={{flex: 1}}/>
                </View>
                {
                    this.props.type === "mentions" ? (
                        <View style={{flex: 1, marginRight: 20}}>
                            <Text style={{marginTop: 10, fontSize: 14}}>{this.props.payload.user.name} {this.props.payload.speak ? strings('NotificationComponent.mentionSpeak') : strings('NotificationComponent.mentionComment')}</Text>
                            <View style={{maxHeight: 70, width: '100%', borderColor: '#979797', borderRadius: 8, borderWidth: 0.3, marginTop: 10}}>
                                <Text style={{padding: 7}}>{this.props.payload.speak ? this.props.payload.speak.text : this.props.payload.comment.text}</Text>
                            </View>
                        </View>
                    ) : (
                        <View style={{flex: 1, marginRight: 20}}>   
                            <Text style={{marginTop: 10, fontSize: 14}}>{this.props.payload.data.user.name} {text}</Text>
                            {
                                this.props.payload.type === "like" || this.props.payload.type === "comment" ? (
                                    <View style={{maxHeight: 70, width: '100%', borderColor: '#979797', borderRadius: 8, borderWidth: 0.3, marginTop: 10}}>
                                        <Text style={{padding: 7}}>{this.props.payload.data.speak? this.props.payload.data.speak.text : this.props.payload.data.content.text}</Text>
                                    </View>
                                ) : this.props.payload.type === "respeak" ? (
                                    <View style={{maxHeight: 70, width: '100%', borderColor: '#979797', borderRadius: 8, borderWidth: 0.3, marginTop: 10}}>
                                        <Text style={{padding: 7}}>{this.props.payload.data.respeak.text}</Text>
                                    </View>
                                ) : null
                            }
                        </View>
                    )
                }
            </TouchableOpacity>
        )
    }
}

export default connect(state => ({id: state.id, token: state.token}))(NotificationsComponent)