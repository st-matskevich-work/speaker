import React from 'react'
import {TouchableOpacity, Image, Vibration, View, Animated, Easing} from 'react-native'
import RNSoundLevel from 'react-native-sound-level'
import AudioRecord from 'react-native-audio-record';
import {
    Player,
    Recorder,
    MediaStates
} from '@react-native-community/audio-toolkit';
import { WaveFile } from 'wavefile'
import RNFS from 'react-native-fs'
import { Buffer } from 'buffer'
let base64 = require('base-64');

export default class VoiceRecorder extends React.Component{
    state = {
        voiceBase64: "",
        circleSize: new Animated.Value(0),
        voiceID: 0,
        recording: false
    }

    onLongPress = () => {
        Vibration.vibrate(100)
        const options = {
            sampleRate: 16000,  // default 44100
            channels: 1,        // 1 or 2, default 1
            bitsPerSample: 16,  // 8 or 16, default 16
            audioSource: 6,     // android only (see below)
            wavFile: this.state.voiceID + '.wav', // default 'audio.wav'
        };
          
        AudioRecord.init(options);

        AudioRecord.start();

        Animated.timing(
            this.state.circleSize,
            {
                toValue: 120,
                duration: 250,
                delay: 0 ,
                //easing: Easing.
            }
        ).start()
    }

    onPressOut = async () => {
        Animated.timing(
            this.state.circleSize,
            {
                toValue: 0,
                duration: 250,
                delay: 0
            }
        ).start()

        let audioFile = await AudioRecord.stop()

        /*alert(audioFile);
        alert(await RNFS.exists(audioFile))
        RNFS.readFile(audioFile, 'base64')
            .then((data) => {
                let decodedData = base64.decode(data);
                let bytes=decodedData.length;

                if(bytes < 1024) alert(bytes + " Bytes");
                else if(bytes < 1048576) alert("KB:"+(bytes / 1024).toFixed(3) + " KB");
                else if(bytes < 1073741824) alert("MB:"+(bytes / 1048576).toFixed(2) + " MB");
                else alert((bytes / 1073741824).toFixed(3) + " GB");
            })*/
    

        this.props.onRecorded({
            path: audioFile,
            base64: await RNFS.readFile(audioFile, 'base64'),
            voiceID: this.state.voiceID
        })

        this.setState({voiceID: this.state.voiceID+1, voiceBase64: ""})
    }

    render(){
        return(
            <View style={[{marginRight: -20, width: 60, alignItems: 'center', justifyContent: 'center',overflow: 'visible'}, this.props.style]}>
                <Animated.View style={{position: 'absolute', backgroundColor: '#0AB4BE', width: this.state.circleSize, height: this.state.circleSize, borderRadius: this.state.circleSize, opacity: 0.3, zIndex: 1}}/>
                <TouchableOpacity style={{width: '100%', justifyContent: 'center', alignItems: 'center', zIndex: 2}} onLongPress={this.onLongPress} onPressOut={this.onPressOut}>
                    <Image source={require('../Images/Logo1.png')} style={{height: '100%', width: 15, resizeMode: 'contain'}}/>
                </TouchableOpacity>
            </View>
        )
    }
}