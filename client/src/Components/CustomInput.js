import React from 'react'
import {TextInput, StyleSheet, TouchableWithoutFeedbackBase, View, Image} from 'react-native'
import * as AuthAPI from '../API/Auth'

export default class CustomInput extends React.Component{
    state={
        value: "",
        width: '99%',
        showChecked: false,
        fetchAllowed: true,
        isPhone: false
    }

    componentDidMount(){
        setTimeout(() => {
            this.setState({ width: '100%' })
          }, 100)

          this.ref = React.createRef()
          if(this.props.onRef) this.props.onRef(this)
    }

    clearInput = () => {
        this.setState({value: ""})
    }

    fromRegex = (value) => {
        if(this.props.phoneRegex){
            if(value.match(this.props.phoneRegex)?.length){
                this.setState({showChecked: true, isPhone: true})
                return
            }else{
                this.setState({showChecked: false, isPhone: false})
            }
        }

        if(value.match(this.props.regex)?.length){
            //console.log
            if(this.props.checkAvailability){
                this.checkAvailability(value)
            }else{
                this.setState({showChecked: true, isPhone: false})
            }
        }else{
            this.setState({showChecked: false, isPhone: false})
        }
    }

    checkAvailability = (value) => {
        if(this.state.fetchAllowed){
            this.setState({fetchAllowed: false})

            if(this.props.type === "emailAddress"){
                AuthAPI.checkAvailability({email: value})
                .then(res => {
                    if(res.statusCode === 200){
                        if(this.state.value === value) this.setState({showChecked: true})
                    }else{
                        this.setState({showChecked: false})
                    }
                })
                .catch(err => {})
            }else{
                AuthAPI.checkAvailability({tag: value})
                .then(res => {
                    if(res.statusCode === 200){
                        if(this.state.value === value) this.setState({showChecked: true})
                    }else{
                        this.setState({showChecked: false})
                    }
                })
                .catch(err => {})
            }
            
            setTimeout(() => this.setState({fetchAllowed: true}), 200)
        }
    }

    getFieldState = () => {
        return this.state.showChecked
    }


    render(){

        return(
            <View style={[{flexDirection: 'row'}, {width: this.state.width, borderBottomWidth: this.props.disableBorder ? 0 : 1, borderBottomColor: '#E9E5E5', marginBottom: 20, alignItems: 'center'}, this.props.viewStyle]}>
                <TextInput style={[styles.input, {flex: 1}, this.props.style]}
                            placeholder={this.props.placeholder}
                            textContentType={this.props.type}
                            secureTextEntry={this.props.type === "password" ? true:false}
                            onChangeText={text => {
                                console.log(this.props.disableSpacing, text.length)
                                if(!this.props.blurOnSubmit) text = text.split('\n')[0]
                                if(this.props.disableSpacing){
                                    //text.replace('B', 'ff')
                                    text = text.split(' ')[0]
                                }

                                console.log(this.props.regex)

                                if(this.props.regex) this.fromRegex(text)
                                //if(this.props.checkAvailability) this.checkAvailability(text)
                                
                                this.props.onChangeText(text)
                                this.setState({value : text})
                            }}
                            underlineColorAndroid="transparent"
                            textAlignVertical="center"
                            selectTextOnFocus={true}
                            value={this.state.value}
                            onContentSizeChange={this.props.onContentSizeChange ? this.props.onContentSizeChange : null}
                            blurOnSubmit={this.props.blurOnSubmit ? this.props.blurOnSubmit : true}
                            maxLength={this.props.maxLength ? this.props.maxLength : 64}
                            multiline={this.props.multiline ? this.props.multiline: false}
                            keyboardType={this.props.keyboardType ? this.props.keyboardType : "default"}
                        />
                    {
                        this.state.showChecked? <Image source={require('../Images/verified.png')} style={{height: 30, width: 30}}/> : null
                    }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    input:{
        fontSize: 18,
        paddingBottom: 10,
        color: 'black',
    }
})