import React from 'react'
import {View, Image, Text, TouchableOpacity} from 'react-native'
import * as UsersAPI from '../API/Users'
import {DotIndicator} from 'react-native-indicators';
import * as Constants from '../Constants/Constants'
import { connect } from 'react-redux'

export default class UserTile extends React.Component{
    state = {
        loading: false
    }


    onPress = () => {
        this.setState({loading: true})
        UsersAPI.getUserInfoByTag(this.props.id, this.props.user.tag)
        .then(res => {
            //console.log(res)
            if(res.statusCode === 200){
                this.props.navigation.navigate('Profile', {
                    user: res
                })
            }
            this.setState({loading: false})
        })
        .catch(err => this.setState({loading: false}))
    }

    render(){
        return(
            <TouchableOpacity style={{flex: 1, flexDirection: 'row'}} onPressOut={this.onPress} disabled={this.state.loading}>
                <Image source={{uri: Constants.SERVER_ADDRESS + this.props.user.image}} style={{marginLeft: 15, marginTop: 10, marginBottom: 10, width: 50, height: 50, borderRadius: 25, backgroundColor: '#f6f7f8'}}/>
                <View style={{flex: 1, marginRight: 15, marginLeft: 15, justifyContent: 'center'}}>
                    <Text style={{fontSize: 18}}>{this.props.user.name}</Text>
                    <Text style={{color: '#979797'}}>@{this.props.user.tag}</Text>
                </View>
                {
                    this.state.loading ? <DotIndicator color="#0AB4BE" size={9} count={3}/> : null
                }
            </TouchableOpacity>
        )
    }
}