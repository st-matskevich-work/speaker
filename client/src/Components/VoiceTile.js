import React from 'react'
import { View, Text, Animated, TouchableOpacity, Dimensions, Easing, Image } from 'react-native'
import {
    Player,
    Recorder,
    MediaStates
} from '@react-native-community/audio-toolkit';
import * as Redash from 'react-native-redash'

export default class VoiceTile extends React.Component{
    state = { 
        currentPosition: new Animated.Value(0),
        playing: false,
        wasEnabled: false,
        width: new Animated.Value(0),
        imageSize: 0,
    }

    componentDidMount(){
    }

    audioControl = () => {
        //if(this.wasEnabled) this.
        // /console.log(this.state)
        if(this.state.playing){
            this.props.pause(() => {
                Animated.timing(this.state.width).stop()
            })
            this.setState({playing: false, wasEnabled: true})
        }else{
            //new Player(this.props.data.path).play()
            if(this.state.wasEnabled){
                this.setState({playing: true})
                this.props.continue((duration) => {
                    Animated.timing(
                        this.state.width,
                        {
                            duration: duration,
                            toValue: this.state.imageSize,
                        }
                    ).start()
                })
            }
            else{
                this.setState({playing: true, wasEnabled: true})
                this.props.play((duration) => {
                    Animated.timing(
                        this.state.width,
                        {
                            duration: duration,
                            toValue: this.state.imageSize,
                        }
                    ).start()
                })
                //console.log(this.props.player.duration)
                //setTimeout(() => Animated.timing(this.state.width).stop(), 700)
            }

            this.setState({playing: true})
        }
    }

    resetProgress = () => {
        this.setState({playing: false, wasEnabled: false})
        //console.log(this.state)
        this.forceUpdate()
        Animated.timing(
            this.state.width,
            {
                duration: 0,
                toValue: 0
            }
        ).start()
    }

    componentDidMount(){
       if(this.props.onRef) this.props.onRef(this)

        /*Animated.timing(
            this.state.currentPosition,
            {
                toValue: 0,
                duration: 250,
                delay: 0
            }
        ).start()*/
    }

    deleteProcess = async (callback) => {
        /*console.log('DELETENG')
        await Animated.timing(
            this.state.currentPosition,
            {
                toValue: Dimensions.get('window').width*(-1),
                duration: 250,
                delay: 0
            }
        ).start(callback)*/
        callback()
    }

    componentWillUnmount(){
        
    }
    
    render(){
        return(
            <Animated.View style={[{left: this.state.currentPosition, height: 60, flexDirection: 'row', alignItems: 'center'}, this.props.style]}>
                <TouchableOpacity onPressOut={this.audioControl} style={{height: 46,marginRight: 10, width: 46, justifyContent: 'center', alignItems: 'center', overflow: 'hidden', backgroundColor: 'white', borderRadius: 23}}>
                    <Image source={this.state.playing ? require('../Images/pause.png') : require('../Images/play.png')} style={{height: 40, width: 40, borderRadius: 20}}/>
                </TouchableOpacity>
                <View style={{flex: 1, height: 60, justifyContent: 'flex-start', overflow: 'hidden', justifyContent: 'center'}}>
                    <Image source={require('../Images/waveform.png')} style={{height: 40, width: '100%' , resizeMode: 'contain'}} onLayout={event => {
                        this.setState({imageSize: event.nativeEvent.layout.width})
                    }}/>
                    <Animated.View style={{position: 'absolute', height: 40, width: this.state.width , overflow: 'hidden'}}>
                        <Animated.Image source={require('../Images/waveformProgress.png')} style={{height: 40, width: this.state.imageSize, resizeMode: 'contain'}}/>
                    </Animated.View>
                </View>
                {
                    this.props.allowDeleting ? (
                        <TouchableOpacity onPressOut={this.props.removeVoice} style={{height: 40, width: 40, justifyContent: 'center', alignItems: 'center'}}> 
                            <Image source={require('../Images/close.png')} style={{height: 15, width: 15}}/>
                        </TouchableOpacity>
                    ) : null
                }
            </Animated.View>
        )
    }
}