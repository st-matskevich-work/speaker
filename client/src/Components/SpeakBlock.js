import React from 'react'
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native'
import { connect } from 'react-redux'
import * as Constants from '../Constants/Constants'
import * as SpeaksAPI from '../API/Speaks' 
import * as UsersAPI from '../API/Users'
import * as Actions from '../Actions/Actions'
import moment from 'moment'
import 'moment/locale/ru'
import AudioSyncBlock from './AudioSyncBlock'
import ImageViewer from './ImageViewerComponent'
import ParsedText from 'react-native-parsed-text';

class SpeakBlock extends React.Component{
    state = {
        likes: 0,
        liked: false,
        respeakes: 0,
        respeaked: false,
        comments: 0,
        images: [],
        voices: [],
        loading: false
    }
    
    componentDidMount(){
        //console.log(this.props.speak)
        this.props.speak.attachments.map((value, i) => {
            //console.log(value.type, 'TYPE AUDIO')
            if(value.type == 'voice'){
                //console.log('BEGIN RENDERING...')
                this.state.voices.push({
                    type: value.type,
                    path: Constants.SERVER_ADDRESS + value.path
                })
                this.forceUpdate()
            }
            if(value.type == 'image'){
                this.state.images.push({
                    url: Constants.SERVER_ADDRESS + value.path
                })
                this.forceUpdate()
            }
        })
    }

    processDate = () => {
        return moment(new Date(+this.props.speak.date)).startOf('minute').fromNow()
    }

    like = () => {
        this.props.setLikeMark(this.props.speak.id, this.props.speak.user.id, this.props.id)

        SpeaksAPI.sendLikeRequest(this.props.speak.id, this.props.id, this.props.token)
        .then(res => {
            console.log(res)
            if(res.statusCode != 200){
                this.props.setLikeMark(this.props.speak.id, this.props.speak.user.id, this.props.id) 
                
            }
        })
        .catch(err => this.props.setLikeMark(this.props.speak.id))
    }   

    toRespeak = () => {
        if(this.state.loading) return

        this.setState({loading: true})
        SpeaksAPI.getCurrentSpeak(this.props.id, this.props.speak.respeaked.id)
        .then(res => {
            if(res.statusCode === 200){
                this.props.navigation.navigate({
                    routeName: 'SpeakScreen',
                    params: {
                        speak: res.speaks,
                        like: this.like
                    }
                })
            }
        })
        .catch(err => console.log(err))
        .finally(() => {
            this.setState({loading: false})
        })
    }

    jumpToUser = (props) => {
        if(this.state.loading) return

        props = props.split('@')[1]
        let key = Math.round( Math.random() * 10000000 );

        this.setState({loading: true})
        UsersAPI.getUserInfoByTag(this.props.id, props)
        .then(result => {
            console.log(result)
            if(result.statusCode === 200){
                this.props.navigation.push('Profile', {
                    user: result
                })
            }
        })
        .catch(err=> console.log(err))
        .finally(() => {
            this.setState({loading: false})
        })
    }

    render(){
        return(
            <TouchableOpacity style={{width: '100%', flexDirection: 'row'}} activeOpacity={1} onPress={() => {
                if(this.state.loading) return
                this.setState({loading: true})
                let key = Math.round( Math.random() * 10000000 );
                
                this.props.navigation.navigate({
                    routeName:'SpeakScreen',
                    params: {
                        speak: this.props.speak,
                        like: this.like,
                        currentUserId: this.props.currentUserId,
                        key: key,
                        parentUpdate: this.props.parentUpdate
                    },
                    key: key
                })
                this.setState({loading: false})
            }}>
                <View>
                    <TouchableOpacity onPressOut={() => this.jumpToUser('@' + this.props.speak.user.tag)}>
                        <Image source={{uri: Constants.SERVER_ADDRESS + this.props.speak.user.avatar}} style={{height: 46, width: 46, borderRadius: 30, marginLeft: 20, marginRight: 20, marginTop: 10, backgroundColor: '#f6f7f8'}}/>
                    </TouchableOpacity>
                    <View style={{flex: 1}}/>
                </View>
                <View style={{flex: 1, marginTop: 10, marginRight: 20, marginBottom: 10}}>
                    {
                        this.props.speak.uniqueId ? (
                            <TouchableOpacity onPressOut={this.toRespeak} disabled={true}>
                                <Text style={{marginBottom: 5, color: '#979797'}}>Респик от {this.props.speak.user.name}</Text>
                            </TouchableOpacity>
                        ) : null
                    }
                    <View style={{flexDirection: 'row-reverse', maxWidth: '100%'}}>
                        <Text style={{color: '#979797'}}>{this.processDate()}</Text>
                        <View style={{flex: 1}}/>
                        <Text style={{fontSize: 18, fontWeight: 'bold'}}>{this.props.speak.user.name}{'\n'}<Text style={{fontSize: 15, fontWeight: 'normal', color: '#979797'}}>@{this.props.speak.user.tag}</Text></Text>
                    </View>
                    <View style={{marginTop: 10}}>
                        {
                            /*this.props.speak.voice === true ? (
                                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                    <Image source={require('../Images/play.png')} style={{height: 30, width: 30, marginRight: 10}}/>
                                    <Image source={require('../Images/waveform.png')} style={{height: 40, resizeMode: 'center', flex: 1}}/>
                                    <Text style={{color: '#979797', fontSize: 15, marginLeft: 10}}>1:20</Text>
                                </View>
                            ) : (
                                <Text>{this.props.speak.speak}</Text>
                            )*/
                            this.props.speak.speak?.length ? 
                                    <ParsedText style={{marginBottom: 10}}
                                                parse={[
                                                    {
                                                        pattern: /@[a-zA-Z0-9_-]{3,32}/,
                                                        style: {color: '#0AB4BE'},
                                                        onPress: this.jumpToUser
                                                    }
                                                ]}>{this.props.speak.speak}
                                                </ParsedText> : null
                        }
                        <ImageViewer 
                                    images={this.state.images}
                        />
                        <AudioSyncBlock 
                                        syncronize={this.props.syncronize}
                                        voiceAttachments={this.state.voices}
                                        allowDeleting={false}
                                        player={this.props.player}
                                        setNewPlayer={this.props.setNewPlayer}/>

                        {
                            this.props.speak.respeaked ? (
                                <TouchableOpacity onPressOut={this.toRespeak} style={{width: '100%', backgroundColor: 'white', marginBottom: 5, maxHeight: 100, borderColor: '#979797', borderRadius: 8, borderWidth: 0.3, padding: 7}}>
                                    <View style={{width: '100%', flexDirection: 'row'}}>
                                        <TouchableOpacity onPressOut={() => this.jumpToUser('@' + this.props.speak.respeaked.tag)}>
                                            <Image source={{uri: Constants.SERVER_ADDRESS + this.props.speak.respeaked.image}} style={{height: 40, width: 40, borderRadius: 30, backgroundColor: '#f6f7f8'}}/>
                                        </TouchableOpacity>
                                        <View style={{flex: 1, marginLeft: 10}}>
                                            <Text style={{fontSize: 16}}>{this.props.speak.respeaked.name}</Text>
                                            <Text style={{color: '#979797', fontSize: 14}}>@{this.props.speak.respeaked.tag}</Text>
                                        </View>
                                    </View>
                                    <Text style={{marginTop: 5}}>{this.props.speak.respeaked.original_text}</Text>
                                </TouchableOpacity>
                            ) : null
                        }
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 10}}>
                        <TouchableOpacity disabled={true}>
                            <Image source={require('../Images/comment.png')} style={{height: 20, width: 20, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <Text style={{color: '#979797', fontSize: 15, marginLeft: 5, marginRight: 15}}>{this.props.speak.comments}</Text>
                        <TouchableOpacity onPressOut={() => this.props.navigation.navigate('CreateNewSpeak', {
                            id: this.props.speak.id,
                            name: this.props.speak.user.name,
                            parentUpdate: this.props.parentUpdate,
                            speak: this.props.speak
                        })}>
                            <Image source={require('../Images/reply.png')} style={{height: 20, width: 20, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <Text style={{color: '#979797', fontSize: 15, marginLeft: 5, marginRight: 15}}>{this.props.speak.respeaks}</Text>
                        <TouchableOpacity onPressOut={this.like}>
                            <Image source={this.props.speak.liked ? require('../Images/liked.png') : require('../Images/like.png')} style={{height: 20, width: 20, resizeMode: 'stretch'}}/>
                        </TouchableOpacity>
                        <Text style={{color: '#979797', fontSize: 15, marginLeft: 5, marginRight: 15}}>{this.props.speak.likes}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

export default connect(state => ({id: state.id, token: state.token}))(SpeakBlock)