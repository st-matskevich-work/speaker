import React from 'react'
import {View, Text, StatusBar, ScrollView, Image, TouchableOpacity} from 'react-native'

export default class CustomHeader extends React.Component{
    render(){
        return(
            <View style={{width: '100%', flexDirection: 'row', height: 50,alignItems: 'center'}}>
                <TouchableOpacity style={{height: '100%', width: 50, justifyContent: 'center', alignItems: 'center'}} onPressOut={() => this.props.navigation.goBack()}>
                    <Image source={require('../Images/back.png')} style={{height: 14, width: 14, resizeMode: 'contain'}}/>
                </TouchableOpacity>
                <Text style={{fontSize: 18, margin: 10}}>{this.props.title}</Text>
                <View style={{flex: 1}}/>
            </View>
        )
    }
}